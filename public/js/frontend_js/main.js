
$(document).ready(function(){
    $("#current_pwd").keyup(function(){
		var current_pwd = $(this).val();
		$.ajax({
			headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
			type:'post',
			url:'/check-user-pwd',
			data:{current_pwd:current_pwd},
			success:function(resp){
				/*alert(resp);*/
				if(resp=="false"){
					$("#chkPwd").html("<font color='red'>Current Password is incorrect</font>");
				}else if(resp=="true"){
					$("#chkPwd").html("<font color='green'>Current Password is correct</font>");
				}
			},error:function(){
				alert("Error");
			}
		});
    });
    
    $("#password_validate").validate({
        errorElement: 'div',

		rules:{
	
			current_pwd:{
				required: true,
				minlength:6,
				maxlength:20
			},

			new_pwd:{
				required: true,
				minlength:6,
				maxlength:20
			},
			confirm_pwd:{
				required:true,
				minlength:6,
				maxlength:20,
				equalTo:"#new_pwd"
			}
		},

	
		messages:{
			current_pwd:{ 
				required:"Please enter your current password",
				minlength: "Your Password must be atleast 6 characters long",
			}, 
			new_pwd:{
				required:"Please provide your new Password",
				minlength: "Your Password must be atleast 6 characters long"
            },
            confirm_pwd:{
				required:"Please provide your Confirm Password",
				minlength: "Your Confirm Password must be equal Password "
			}
        }
    });
    
});


$(document).ready(function(){

    //Change Price & Stock with Size 
    $("#selectSize").change(function(){
        var idSize = $(this).val();

        if(idSize == ""){
            return false;
        }

        $.ajax({
            type:'get',
            url:'/get-product-price',
            data:{idSize:idSize},
            success:function(resp){
                //alert(resp); return false;
                var arr = resp.split('#');
                var arr1 = arr[0].split('-');
                var button = $("#cartButton");
                $("#getPrice").html("<h4> $  " + arr1[0] + "</h4>"+"<h7><small>ILS</small>"+arr1[1]+"<br><small>JOD</small>"+arr1[2]+"<br></h7><br>");
                //we can send updated price of product attribute whenever user tries to add product in cart.
                $("#price").val(arr[0]);
                if(arr[1] == 0 ){
                    $("#available").text("Out of Stock");
					$(button).hide();

                }else {
                    $("#available").text("In Stock");
					$(button).show();

                }
            },error:function(){
                alert("Error");
            }

        });
    });
});



$(document).ready(function(){
       //Replace Main Image with Alternative
       $(".changeImage").click(function(){

        var image = $(this).attr('src');
        $(".mainImage").attr("src",image);

    });

});

$(document).ready(function(){
    var $easyzoom = $('.easyzoom').easyZoom();

    // Setup thumbnails example
    var api1 = $easyzoom.filter('.easyzoom--with-thumbnails').data('easyZoom');

    $('.thumbnails').on('click', 'a', function(e) {
        var $this = $(this);

        e.preventDefault();

        // Use EasyZoom's `swap` method
        api1.swap($this.data('standard'), $this.attr('href'));
    });

    // Setup toggles example
    var api2 = $easyzoom.filter('.easyzoom--with-toggle').data('easyZoom');

    $('.toggle').on('click', function() {
        var $this = $(this);

        if ($this.data("active") === true) {
            $this.text("Switch on").data("active", false);
            api2.teardown();
        } else {
            $this.text("Switch off").data("active", true);
            api2._init();
        }
    });

 });

 $().ready(function(){
	// Validate Register form on keyup and submit
	$("#registerForm").validate({
        errorElement: 'div',
		rules:{
			name:{
				required:true,
				minlength:2,
                accept: "[a-zA-Z]+"
			},
			myPassword:{
				required:true,
				minlength:6
            },
            re_pass:{
				required:true,
				minlength:6,
				maxlength:20,
				equalTo:"#myPassword"
			},
			email:{
				required:true,
                email:true,
                remote:"/check-email"
            }
		},
		messages:{
			name:{ 
				required:"Please enter your Name",
				minlength: "Your Name must be atleast 2 characters long",
				accept: "Your Name must contain letters only"		
			}, 
			pass:{
				required:"Please provide your Password",
				minlength: "Your Password must be atleast 6 characters long"
            },
            re_pass:{
				required:"Please provide your Confirm Password",
				minlength: "Your Confirm Password must be equal Password "
			},
			email:{
				required: "Please enter your Email",
                email: "Please enter valid Email",
                remote:"Email alreay exist !"

			}
        }
        
    });

    $("#login-form").validate({
        errorElement: 'div',
		rules:{
            email:{
				required:true,
                email:true,
            },
			your_pass:{
				required:true,
				minlength:6
            }
		},
		messages:{
			email:{ 
                required: "Please enter your Email",
                email: "Please enter valid Email",
			}, 
			your_pass:{
				required:"Please provide your Password",
            }
        }
        
    });

    $("#UpdateForm").validate({
        errorElement: 'div',
		rules:{
			name:{
				required:true,
				minlength:2,
                accept: "[a-zA-Z]+"
			},
			address:{
                required:true,
                minlength:2

            },
            city:{
                required:true,
                minlength:2
            },
            state:{
                required:true,
                minlength:2
            },
            country:{
                required:true,
            },
            pincode:{
                required:true,
                minlength:2
            },
            mobile:{
                required:true,
                minlength:10
			},
		},
		messages:{
			name:{ 
				required:"Please enter your Name",
				minlength: "Your Name must be atleast 2 characters long",
				accept: "Your Name must contain letters only"		
			}, 
			address:{
				required:"Please provide your Address",
            },
            city:{
				required:"Please provide your City",
            },
            country:{
				required:"Please provide your Country",
            },
            pincode:{
				required:"Please provide your Pincode",
            },
            mobile:{
				required:"Please provide your Mobile",
            }
        }
        
    });

 });
 


 $(document).ready(function($) {

    $('#myPassword').passtrength({
        tooltip:true,
        textWeak:"Weak", 
        textMedium:"Medium", 
        textStrong:"Strong",  
        textVeryStrong:"Very Strong",
      
      });
    $('#myPassword').passtrength({
        minChars: 6,
        passwordToggle: true,
        tooltip: true,
        eyeImg : "/images/frontend_images/eye.svg"
    });

    //copy billing address to shipping address when shipping address is same as billing address
    $('#Bill2Ship').on('click',function(){
        if(this.checked){
            $("#shiping_name").val($("#billing_name").val());
            $("#shiping_address").val($("#billing_address").val());
            $("#shiping_city").val($("#billing_city").val());
            $("#shiping_state").val($("#billing_state").val());
            $("#shiping_pincode").val($("#billing_pincode").val());
            $("#shiping_phone").val($("#billing_phone").val());
            $("#shiping_country").val($("#billing_country").val());
         }else{
            $("#shiping_name").val('');
            $("#shiping_address").val('');
            $("#shiping_city").val('');
            $("#shiping_state").val('');
            $("#shiping_pincode").val('');
            $("#shiping_phone").val('');
            $("#shiping_country").val('');
        }

    });

  });
  
  function selectPaymentMethod(){
      if($('#PayPal').is(':checked') || $('#COD').is(':checked')){
        /*  alert("checked"); */
      }else{
        alert("Plese Select Payment Method");
        return false;
      }
  }

  $(document).ready(function() {
    $('#example').DataTable({
        "pageLength": 10,
        "pagingType": "numbers"
    });
} );



