<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Coupon;

class CouponsController extends Controller
{
    public function addCoupon(Request $request){

        if ($request->isMethod('post')){
         $data= $request->all();

         //echo "<pre>" ; print_r($data) ; die;
         

         $coupon = new Coupon;
         $coupon->coupon_code = $data['coupon_code'];
         $coupon->amount = $data['amount'];
         $coupon->amount_type = $data['amount_type'];
         $coupon->expiry_date = $data['expiry_date'];
         $coupon->description = $data['description'];

         if(empty($data['status'])){
            $status='0';
            }else{
            $status='1';
        }

            $coupon->status = $status;


            $coupon->save();

            return redirect('/admin/view-coupons')->with('flash_message_success','Coupon Added Successfully!');
        }
        return view('admin.coupons.add_coupon');

    } 
    
    public function viewCoupons(){
        $coupons = Coupon::get();
        return view('admin.coupons.view_coupon')->with(compact('coupons'));
    }

    public function editCoupon(Request $request , $id = null ){
        if($request->isMethod('post')){
            $data= $request->all();

            if(empty($data['status'])){
                $status='0';
            }else{
                $status='1';
            }
            Coupon::where(['id'=>$id])->update(['coupon_code'=>$data['coupon_code'] ,//name in form 
                                                 'amount'=>$data['amount'] ,
                                                  'amount_type'=>$data['amount_type'],
                                                  'expiry_date'=>$data['expiry_date'],
                                                  'description' => $data['description'],
                                                  'status'=>$status]);
            return redirect('/admin/view-coupons')->with('flash_message_success','Coupon updated Successfully!');

        }
        $CouponDetaiels = Coupon::where(['id'=>$id])->first();
        return view('admin.coupons.edit_coupon')->with(compact('CouponDetaiels'));

    }

    
  
        //other function to edit coupon
    	/*public function editCoupon(Request $request,$id=null){
		if($request->isMethod('post')){
			$data = $request->all();
			//echo "<pre>"; print_r($data); die;
			$coupon = Coupon::find($id);
			$coupon->coupon_code = $data['coupon_code'];	
			$coupon->amount_type = $data['amount_type'];	
			$coupon->amount = $data['amount'];
			$coupon->expiry_date = $data['expiry_date'];
			if(empty($data['status'])){
				$data['status'] = 0;
			}
			$coupon->status = $data['status'];
			$coupon->save();	
			return redirect()->action('CouponsController@viewCoupons')->with('flash_message_success', 'Coupon has been updated successfully');
		}
		$couponDetails = Coupon::find($id);
		//$couponDetails = json_decode(json_encode($couponDetails));
		//echo "<pre>"; print_r($couponDetails); die;/
		return view('admin.coupons.edit_coupon')->with(compact('couponDetails'));
	} */

    
    

    public function deleteCoupon(Request $request , $id = null){

        if(!empty($id)){
            Coupon::where(['id'=>$id])->delete();
            return redirect()->back()->with('flash_message_success','Coupon deleted Successfully!');
        }

    }  
    
   


}
