<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NewsletterSubscriber;

class NewsletterController extends Controller
{
    public function checkSubscriber(Request $request){
        if($request->ajax()){
            $data = $request->all();
           // echo "<pre>" ; print_r($data) ; die;

           $newsletter =  NewsletterSubscriber::where('email' , $data['email'])->count();
           if($newsletter>0 ){
               echo true;
           }else{
               echo false;
           }
        }
    }

    public function addSubscriber(Request $request){
        if($request->ajax()){
            $data = $request->all();
        // echo "<pre>" ; print_r($data) ; die;

        $newsletter =  NewsletterSubscriber::where('email' , $data['email'])->count();
            if($newsletter>0 ){
                echo true;
            }else{
                //Add Newsletter email in 
                $newsletter = new NewsletterSubscriber;
                $newsletter->email = $data['email'];
                $newsletter->status = 1; 
                $newsletter->save();

                echo false; 
            }
        }
    }

    public function viewNewsletterSubscribers(){
        $newsletterDetails = NewsletterSubscriber::get()->all();
        $newsletterDetails = json_decode(json_encode($newsletterDetails));

        return view('admin.newsletter.view_newsletters')->with(compact('newsletterDetails'));
    }

    public function updateNewsletterStatus($id , $status){
        NewsletterSubscriber::where('id',$id)->update(['status' =>  $status]);
        return redirect()->back()->with('flash_message_success','Newsletter status has been updated ');
    }

    public function deleteNewsletterEmail($id){
        NewsletterSubscriber::where('id',$id)->delete();
        return redirect()->back()->with('flash_message_success','Newsletter  has been deleted ');

    }

    public function exportNewsletterEmails(){

        $newsletterData= NewsletterSubscriber::select('id','email','created_at')->orderBy('id','DESC')->where('status',1)->get();
        $newsletterData = json_decode(json_encode($newsletterData));
        echo "<pre>" ; print_r($newsletterData) ; die;

    }
}
