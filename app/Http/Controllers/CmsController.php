<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\CmsPage;


class CmsController extends Controller
{
    public function addCmsPage(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            //echo "<pre>" ; print_r($data) ; die;
            $CMS = new CmsPage ;
            $CMS->title = $data['cms_title'];
            $CMS->description = $data['cms_description'];
            $CMS->url = $data['cms_url'];
            $CMS->meta_title = $data['meta_title'];
            $CMS->meta_description = $data['meta_description'];
            $CMS->meta_keywords = $data['meta_keywords'];

            if(empty($data['status'])){
                $status = 0;
            }else{
                $status =1;
            }
            $CMS->status = $status;
            $CMS->save();
            return redirect()->back()->with('flash_message_success','CMS Page added Successfully');
        }
        return view('admin.cms.add_cms_page');
    }

    public function ViewCmsPage(){
        $cms_pages = CmsPage::get();
        //$cms_pages = json_decode(json_encode($cms_pages));
        //echo "<pre>" ; print_r($cms_pages); die;
        return view('admin.cms.view_cms_pages')->with(compact('cms_pages'));
    }

    public function editCmsPage(Request $request , $id=null){
        if($request->isMethod('post')){
            $data = $request->all();

            if(empty( $data['description'])){

                $data['description']='';
            }

            if(empty($data['status'])){
                $status='0';
            }else{
                $status='1';
            }
            CmsPage::where(['id'=>$id])->update([
                'title'=>$data['cms_title'],
                'description'=>$data['cms_description'],
                'url'=>$data['cms_url'],
                'status'=>$status,
                'meta_title'=>$data['meta_title'],
                'meta_description'=>$data['meta_description'],
                'meta_keywords'=>$data['meta_keywords']
            ]);
            return redirect('/admin/view-cms-pages')->with('flash_message_success','Page attribute has been Updated Successfully!');
        }
        $pageDetaiels = CmsPage::where(['id'=>$id])->first();
        /*$pageDetaiels = json_decode(json_encode($pageDetaiels));
        echo "<pre>";print_r($pageDetaiels); die;*/

        return view('admin.cms.sdit_cms_page')->with(compact('pageDetaiels'));
    }

    public function deleteCmsPage(Request $request , $id=null){
        if(!empty($id)){
            CmsPage::where(['id'=>$id])->delete();
            return redirect()->back()->with('flash_message_success','Page deleted Successfully!');

        }
    }

    public function cmsPage($url){
        //Redirect 404 page if cms page disabeld or not exist 
        $pageCount = cmsPage::where(['url'=>$url , 'status'=>1])->count();
        //echo "<pre>" ;print_r($pageCount);die;
        if($pageCount > 0){
            $cmsPageDetails = CmsPage::where('url',$url)->first();
            $meta_title = $cmsPageDetails->meta_title;
            $meta_description = $cmsPageDetails->meta_description;
            $meta_keywords = $cmsPageDetails->meta_keywords;
 
        }else{
            abort(404);
        }
        
        return view('pages.cms_page')->with(compact('cmsPageDetails','meta_title','meta_description','meta_keywords'));
    }

    public function contact(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();


            $validator = Validator::make($request->all(), [
                'name' => 'required|alpha|max:255',
                'email' => 'required|email',
                'subject'=>'required'
                ]);
    
                if ($validator->fails()) {
                    return redirect()->back()
                                ->withErrors($validator)
                                ->withInput();
                }
            $email = "ghadeershalayel@gmail.com";
            //echo "<pre>";print_r($data); die;
 
            //Send Contact Email
            $MassegeData = [
             'email'=>$data['email'],
             'name'=>$data['name'],
             'subject'=>$data['subject'],
             'Massage'=>$data['Massage']
            ];
            Mail::send('emails.enquiry',$MassegeData,function($message) use ($email){
                $message->to($email)->subject('Enquiry from karl Website');
             });
         return redirect()->back()->with('flash_message_success','Thanks for your enquiry. We will get back to you soon');

        }

        $meta_title = "Contact Us - Karl Fashion Website  ";
        $meta_description = "Contact us for any queries related to our products.";
        $meta_keywords = " contact us , اتصل بنا , queries , استفسار ";
   
       return view ('pages.contact')->with(compact('meta_title','meta_description','meta_keywords'));
    }
    
}
