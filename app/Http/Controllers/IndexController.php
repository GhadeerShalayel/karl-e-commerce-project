<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Banner;
use App\ProductAttributes;
class IndexController extends Controller
{

    public function index( ){
        //SHOW IN ascending order by default.
        //$ProductAll = Product::get(); 

        //Show Products in Descending Order :-
        $ProductAll = Product::orderBy('id','DESC')->with('arrtibute')->where('status',1)->where('feature_item',1)->paginate(6);
        //$ProductAll =  json_decode(json_encode($ProductAll));
        //echo"<pre>" ; print_r($ProductAll) ;die;
       

        //Show Products in Random Order :-
        //$ProductAll = Product::inRandomOrder()->where('status',1)->where('feature_item',1)->paginate(6); //number of items you would like to display "per page"
        //$ProductAll =  json_decode(json_encode($ProductAll));
        //echo"<pre>" ; print_r($ProductAll) ;die;

       //dump($ProductAll);
       
        $category = Category::with('category')->where(['parent_id' => '0'])->get();
        $categoryDetails = Category::with('category')->where(['status'=>'1'])->get();
        $categoryDetails = json_decode(json_encode($categoryDetails));
        //echo"<pre>" ; print_r($categoryDetails) ;die;
        //$category =  json_decode(json_encode($category));

        $banners = Banner::where('status','1')->get();
        //$total_stock = ProductAttributes::where('product_id',)->sum('stock');

        //Meta Tag 
        $meta_title = " Karl Fashion Website  ";
        $meta_description = "Online Shopping Site For Men , Women And Kids Clothing ";
        $meta_keywords = "eshop website  , online shopping   , shop , online , men clothing , women , ملابس , تسوق اونلاين ";

        return view('index')->with(compact('ProductAll','category','banners','categoryDetails','meta_title','meta_description','meta_keywords'));
    }



}
