<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ShippingCharges;

class ShippingController extends Controller
{
    public function viewShipping(){
        $ShippingSharges = ShippingCharges::get();
      /*  $ShippingSharges = json_decode(json_encode($ShippingSharges));
        echo "<pre>" ; print_r($ShippingSharges) ; die;*/
        return view('admin.shipping.shipping_charges')->with(compact('ShippingSharges'));
    }

    public function editShipping ($id , Request $request){
        if($request -> isMethod('post')){
            $data = $request->all();
           //echo "<pre>" ; print_r($data) ; die;
           ShippingCharges::where('id',$id)->update(['shipping_charges0_500g'=>$data['shipping_charges0_500g'], 'shipping_charges501_1000g'=>$data['shipping_charges501_1000g'],
           'shipping_charges1001_2000g'=>$data['shipping_charges1001_2000g'],'shipping_charges2001g_5000g'=>$data['shipping_charges2001g_5000g']]);
           
            return redirect('/admin/view-shipping')->with('flash_message_success' , 'Shipping sharges updates successfully ');
        }
        $ShippingDetails = ShippingCharges::where('id',$id)->first();
        return view('admin.shipping.edit_shipping')->with(compact('ShippingDetails'));

    }
}
