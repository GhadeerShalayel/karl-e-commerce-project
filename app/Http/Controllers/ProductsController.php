<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

use Illuminate\Http\Request;
use Auth;
use Session;
use Image;
use App\Category;
use App\Product;
use App\ProductAttributes;
use App\ProductsImage;
use App\Coupon;
use App\countries;
use App\User;
use App\ShipingAddress;
use App\Order;
use App\OrdersProduct;
use DB;
use App\ProductColor;
use Carbon\Carbon;
class ProductsController extends Controller
{


    //Add product function start

    public function add_Product(Request $request){

        if ($request->isMethod('post')){

            $data = $request->all();
           

           // echo "<pre>" ; print_r($data); die;
            if(empty($data['category_id'])){
                return redirect()->back()->with('flash_message_error','Under Category missing ');
            }
            $product = new Product;
            $product->category_id = $data['category_id'];
             /* name in database */ $product->product_name = $data['product_name']; /*name in form*/
            $product->product_code = $data['product_code'];
            $product->product_color = $data['product_color'];
            $product->meta_title = $data['meta_title'];
            $product->meta_description = $data['meta_description'];
            $product->meta_keywords = $data['meta_keywords'];

            if(!empty($data['description'])){
                $product->description = $data['description'];
            }else{
                $product->description ='';
            }

            if(!empty($data['care'])){
                $product->care = $data['care'];
            }else{
                $product->care ='';
            }

            if(!empty($data['Weight'])){
                $product->weight = $data['Weight'];
            }else{
                $product->weight =0;
            }

            if(empty( $data['meta_title'])){

                $data['meta_title']='';
            }
            if(empty( $data['meta_description'])){

                $data['meta_description']='';
            }
            if(empty( $data['meta_keywords'])){

                $data['meta_keywords']='';
            }



            $product->price = $data['price'];


            //Upload Image

            if($request->hasFile('image')){
                $image_tmp = input::file('image');
               if($image_tmp->isValid()){
                   $extinsion = $image_tmp->getClientOriginalExtension();
                   $file_name = rand(111,999).'.'.$extinsion;
                   $large_image_path = 'images/backend_images/products/large/'.$file_name;
                   $home_image_path = 'images/backend_images/products/home/'.$file_name;
                   $shop_image_path = 'images/backend_images/products/shop/'.$file_name;
                   $medium_image_path = 'images/backend_images/products/medium/'.$file_name;
                   $small_image_path = 'images/backend_images/products/small/'.$file_name;

                   //Resize Image

                Image::make($image_tmp)->save($large_image_path);
                Image::make($image_tmp)->resize(380,505)->save($home_image_path);
                Image::make($image_tmp)->resize(254.9,368.1)->save($shop_image_path);
                Image::make($image_tmp)->resize(700,700)->save($medium_image_path);
                Image::make($image_tmp)->resize(100,100)->save($small_image_path);

                //Store image name in products table

                $product->image = $file_name ;
               }
            }


            if(empty($data['status'])){
                $status='0';
                }else{
                $status='1';
            }

            if(empty($data['feature_item'])){
                $feature_item='0';
                }else{
                $feature_item='1';
            }
            $product->status = $status;
            $product->feature_item = $feature_item ; 

            $product->save();
            //return redirect()->back()->with('flash_message_success','Product added Successfully');
            return redirect('/admin/view-product')->with('flash_message_success','Product added Successfully');



        }

        //Category DropDown Start

        $categories = Category::where(['parent_id'=>'0'])->get();
        $category_dropdown = "<option selected value=''  disabled>Select</option>";
        foreach($categories as $cat){

            $category_dropdown .="<option value  = '" .$cat->id."'> ".$cat->name."</option>";
            $sub_categories = Category::where(['parent_id' => $cat->id])->get();
            foreach($sub_categories as $sub_cat){
                $category_dropdown .= "<option value = '".$sub_cat->id."'>&nbsp; --&nbsp;".$sub_cat->name." </option>";

            }

        }        //Category DropDown end


        return view('admin.products.add_product')->with(compact('category_dropdown'));


    }
    //Add product function end



    //View product function start
    public function view_Product(Request $request){
        $products = Product::orderBy('id', 'desc')->get();
        $products = json_decode(json_encode($products));
        foreach($products as $key => $val){

            $category_name =  Category::where(['id' => $val->category_id])->first();
            $products[$key]->category_name = $category_name->name;
        }
     //   echo "<pre>" ; print_r($products); die;
        return view('admin.products.view_product')->with(compact('products','category_name'));
        
    }
    //View product function end



    //Edit product function end
    public function edit_Product(Request $request , $id = null){

        if($request->isMethod('post')){

            $data = $request->all();

            /*
            If we upload new image from edit product form then if part will work and new image will 
            get uploaded otherwise we will pick current_image name again from form. In both cases,
            we will update $filename that can have current image or new image name.*/

            if($request->hasFile('image')){

                $image_tmp = input::file('image');
               if($image_tmp->isValid()){
                   $extinsion = $image_tmp->getClientOriginalExtension();
                   $file_name = rand(111,999).'.'.$extinsion;
                   $large_image_path = 'images/backend_images/products/large/'.$file_name;
                   $home_image_path = 'images/backend_images/products/home/'.$file_name;
                   $shop_image_path = 'images/backend_images/products/shop/'.$file_name;
                   $medium_image_path = 'images/backend_images/products/medium/'.$file_name;
                   $small_image_path = 'images/backend_images/products/small/'.$file_name;

                   //Resize Image

                Image::make($image_tmp)->save($large_image_path);
                Image::make($image_tmp)->resize(380,505)->save($home_image_path);
                Image::make($image_tmp)->resize(254.9,368.1)->save($shop_image_path);
                Image::make($image_tmp)->resize(700,850)->save($medium_image_path);
                Image::make($image_tmp)->resize(100,100)->save($small_image_path);

               }
            }else{

                $file_name = $data['image'];
            }
         

            if(empty( $data['description'])){

                $data['description']='';
            }

            if(empty( $data['care'])){

                $data['care']='';
            }

            if(empty( $data['Weight'])){

                $data['Weight']=0;
            }  
              

            if(empty( $data['meta_title'])){

                $data['meta_title']='';
            }
            if(empty( $data['meta_description'])){

                $data['meta_description']='';
            }
            if(empty( $data['meta_keywords'])){

                $data['meta_keywords']='';
            }


            if(empty($data['status'])){
                $status='0';
            }else{
                $status='1';
            }

            if(empty($data['feature_item'])){
                $feature_item='0';
            }else{
                $feature_item='1';
            }


            Product::where(['id'=>$id])->update(['category_id'=>$data['category_id'] ,//name in form 
            'product_name'=>$data['product_name'] ,
             'product_code'=>$data['product_code'],
             'product_color'=>$data['product_color'],
             'description'=>$data['description'] ,
             'care'=>$data['care'],
             'price'=>$data['price'],
             'weight'=>$data['Weight'],
             'image'=>$file_name ,
             'status'=>$status,
             'meta_title'=>$data['meta_title'],
             'meta_description'=>$data['meta_description'],
             'meta_keywords'=>$data['meta_keywords'],
             'feature_item'=>$feature_item]);
             return redirect('/admin/view-product')->with('flash_message_success','Product updated Successfully!');

        }

                //Get product details

        $productDetaiels = Product::where(['id'=>$id])->first();

                //Category DropDown Start

        $categories = Category::where(['parent_id'=>'0'])->get();
        $category_dropdown = "<option selected value=''  disabled>Select</option>";
        foreach($categories as $cat){

            if($cat->id == $productDetaiels->category_id){

                $selected = "Selected";
            }else{
                $selected ="";
            }

            $category_dropdown .="<option value  = '".$cat->id."' ".$selected."> ".$cat->name."</option>";
            $sub_categories = Category::where(['parent_id' => $cat->id])->get();
            foreach($sub_categories as $sub_cat){
                if($sub_cat->id == $productDetaiels->category_id){

                    $selected = "Selected";
                }else{
                    $selected ="";
                }
                $category_dropdown .= "<option value = '".$sub_cat->id."' ".$selected.">&nbsp; --&nbsp;".$sub_cat->name." </option>";

            }
        }        //Category DropDown end

        return view('admin.products.edit_product')->with(compact('productDetaiels' ,'category_dropdown'));

    }
    //Edit product function end



    //Delete product image function Start

    public function deleteProductImage($id = null){
        //Get Produc Image name
        $productImage = Product::where(['id'=>$id])->first();

        ////Get Produc Image path
        $large_image_path = 'images/backend_images/products/large/';
        $medium_image_path = 'images/backend_images/products/medium/';
        $small_image_path = 'images/backend_images/products/small/';

        /*We will first check if image exists in those folders with "file_exists" built-in function 
        of PHP and then delete the image from the folder with "unlink" built-in function of PHP.*/

        //Delete lareg image if it not existing in folder
        if(file_exists($large_image_path.$productImage->image)){//file_exists — Checks whether a file or directory exists
            unlink($large_image_path.$productImage->image);//unlink — Deletes a file
        }

        //Delete meduim image if it not existing in folder
        if(file_exists($medium_image_path.$productImage->image)){//file_exists — Checks whether a file or directory exists
            unlink($medium_image_path.$productImage->image);//unlink — Deletes a file
        }

        //Delete small image if it not existing in folder
        if(file_exists($small_image_path.$productImage->image)){//file_exists — Checks whether a file or directory exists
            unlink($small_image_path.$productImage->image);//unlink — Deletes a file
        }

        //Delete Image from product table

        Product::where(['id'=>$id])->update(['image'=>'']);
        return redirect()->back()->with('flash_message_success','Product image delete Successfully!');

            
     }   

   //Delete product image function End



    //Delete product function Start

    public function deleteProduct(Request $request , $id = null){

        if(!empty($id)){
            Product::where(['id'=>$id])->delete();
            return redirect()->back()->with('flash_message_success','Product deleted Successfully!');
        }

    }    

    //Delete product function End



    //Add attribute function Start

    public function add_Attribute(Request $request , $id=null){

        //Simply add with('attributes') in query to show Products Attributes as well.
        $productDetails = Product::with('arrtibute')->where(['id'=>$id])->first();
        //$productDetails = json_decode(json_encode($productDetails));
        //echo "<pre>" ; print_r($productDetails) ; die;
        if($request ->isMethod('post')){
           $data = $request->all();

          //echo "<pre>" ; print_r($data) ; die;
           foreach($data['sku'] as $key => $val){
            if(!empty($val)){

                /* Prevent duplicate SKU's :-
                We can't add duplicate SKU for any of our product.*/

                $AttributeCountSku = ProductAttributes::where('sku',$val)->count();
                    if( $AttributeCountSku >0){
                        return redirect('/admin/add-attributes/'.$id)->with('flash_message_error','SKU already exists! Please add another Sku');
                    }

                /* Prevent duplicate SIZE :-
                We can't add duplicate SIZE for any of our product.*/
                $AttributeCountSize = ProductAttributes::where(['product_id'=>$id,'size'=>$data['size'][$key]])->count();
                    if( $AttributeCountSize >0){
                        return redirect('/admin/add-attributes/'.$id)->with('flash_message_error',
                        '"'.$data['size'][$key].'" Size already exists For this product! Please add another Size');
                    }

                $attribute = new ProductAttributes;
                $attribute->product_id = $id;
                $attribute->sku = $val;
                $attribute->size = $data['size'][$key];
                $attribute->price = $data['price'][$key];
                $attribute->stock = $data['stock'][$key];
                $attribute->save();

            }

          }
           return redirect('/admin/add-attributes/'.$id)->with('flash_message_success','Product attribute has been added Successfully!');
        }
        return view('admin.products.add_atrribute')->with(compact('productDetails'));
       
    }

    //Add attribute function End



    //Edit attribute function Start
    public function editAttribute(Request $request , $id = null){
        if($request->isMethod('post')){
            $data = $request->all();
            //echo"<pre>"; print_r($data); die;
            foreach($data['IdAttr'] as $key=>$attr){
                ProductAttributes::where(['id'=>$data['IdAttr'][$key]])->update(['price'=>$data['price'][$key] ,
                'stock'=>$data['stock'][$key]]);
            }
            return redirect()->back()->with('flash_message_success','Product attribute has been Updated Successfully!');
        }
    }
    //Edit attribute function END


    // Products Alternate Images function Start
    public function addImage(Request $request , $id=null){

        $productDetails = Product::with('arrtibute')->where(['id'=>$id])->first();
        
        
        if($request ->isMethod('post')){
            $data = $request->all();
            if ($request->hasFile('image')) {
                $files = $request->file('image');
                foreach($files as $file){
                    // Upload Images after Resize
                    $image = new ProductsImage;
                    $extension = $file->getClientOriginalExtension();
                    $fileName = rand(111,99999).'.'.$extension;
                    $large_image_path = 'images/backend_images/products/large/'.$fileName;
                    $medium_image_path = 'images/backend_images/products/medium/'.$fileName;  
                    $small_image_path = 'images/backend_images/products/small/'.$fileName;  
                    Image::make($file)->save($large_image_path);
                    Image::make($file)->resize(900, 900)->save($medium_image_path);
                    Image::make($file)->resize(100, 100)->save($small_image_path);
                    $image->image = $fileName;  
                    $image->product_id = $data['product_id'];
                    $image->save();
                }
           }
         
        }

         // get all the alternate images of the product.
         $productsImage = ProductsImage::where(['product_id'=>$id])->get();
         $productsImage = json_decode(json_encode($productsImage));
         //echo "<pre>" ; print_r($productsImage) ; die;

        return view('admin.products.add_images')->with(compact('productDetails','productsImage'));

    }
    // Products Alternate Images function End


    // delete Attribute function Start

    public function deleteAttribute(Request $request , $id = null){

        if(!empty($id)){
            ProductAttributes::where(['id'=>$id])->delete();
            return redirect()->back()->with('flash_message_success','Product Attributes deleted Successfully!');
        }

    }    

    // delete Attribute function end


    // delete Images  function Start

    public function deleteImages($id = null){
        //Get Produc Image name
        $productImage = ProductsImage::where(['id'=>$id])->first();

        ////Get Produc Image path
        $large_image_path = 'images/backend_images/products/large/';
        $medium_image_path = 'images/backend_images/products/medium/';
        $small_image_path = 'images/backend_images/products/small/';

        /*We will first check if image exists in those folders with "file_exists" built-in function 
        of PHP and then delete the image from the folder with "unlink" built-in function of PHP.*/

        //Delete lareg image if it not existing in folder
        if(file_exists($large_image_path.$productImage->image)){//file_exists — Checks whether a file or directory exists
            unlink($large_image_path.$productImage->image);//unlink — Deletes a file
        }

        //Delete meduim image if it not existing in folder
        if(file_exists($medium_image_path.$productImage->image)){//file_exists — Checks whether a file or directory exists
            unlink($medium_image_path.$productImage->image);//unlink — Deletes a file
        }

        //Delete small image if it not existing in folder
        if(file_exists($small_image_path.$productImage->image)){//file_exists — Checks whether a file or directory exists
            unlink($small_image_path.$productImage->image);//unlink — Deletes a file
        }

        //Delete Image from product table

        ProductsImage::where(['id'=>$id])->delete();
        return redirect()->back()->with('flash_message_success','Product Alternative image delete Successfully!');

            
     }   

    // delete Images  function Start


    // products  function Start
    public function products($url = null){

        //show 404 not found if category url doesnt exist 

        $countCategrot = Category::where(['url'=>$url , 'status'=> 1])->count();//these methods return the appropriate scalar value 
        if($countCategrot==0){
            abort(404);
        }
        //Get all category and sub category 
        //with  allows you to pass variables to a views
        $category = Category::with('category')->where(['parent_id'=>0])->get();

        $categoryDetails = Category::where(['url'=>$url])->first();//first() returns a Category model object of just one (the first) match for the WHERE clause 
        //echo "<pre>" ; print_r($category) ; die;

        if($categoryDetails->parent_id==0){
        //if url is main category url

            $subCategory =  Category::where(['parent_id'=>$categoryDetails->id])->get();//get() returns a collection object containing Category models for all records that match the WHERE condition;
            $subCategory = json_decode(json_encode($subCategory));
            foreach($subCategory as $subcat){
                    $cat_ids[] = $subcat->id;
            }
            $ProductAll = Product::whereIn('category_id' , $cat_ids )->where('status',1)->orderBy('id','DESC');//the get method to retrieve the results:

           // $ProductAll = json_decode(json_encode($ProductAll));
           
        }else{
        //if url is sub category url
            $ProductAll = Product::where(['category_id' => $categoryDetails->id ])->orderBy('id','DESC');
        }

        if(!empty($_GET['color'])){
            $colorArry = explode('-',$_GET['color']);
            $ProductAll = $ProductAll->whereIn('product_color' , $colorArry);
        }

        if(!empty($_GET['size'])){
            $sizeArray = explode('-',$_GET['size']);
            $ProductAll = $ProductAll->join('product_attributes','product_attributes.product_id','=','products.id')
            ->select('products.*','product_attributes.product_id','product_attributes.size')
            ->groupBy('product_attributes.product_id')
            ->whereIn('product_attributes.size',$sizeArray);
        //groupBy may be used to group the query results. The having method's signature is similar to that of the where method
        }
        $ProductAll = $ProductAll->paginate(9);
        //$colorArry = array('black' , 'gray','blue' , 'brown' ,'green' , 'orange' , 'purple', 'red' ,'white' , 'yellow');
        $colorArry = Product::select('product_color')->groupBy('product_color')->get();
        $colorArry = array_flatten(json_decode(json_encode($colorArry),true));//The Arr::flatten method flattens a multi-dimensional array into a single level array: 
        
        $sizeArry = ProductAttributes::select('size')->groupBy('size')->get();
        $sizeArry = array_flatten(json_decode(json_encode($sizeArry),true));

        $meta_title = $categoryDetails->meta_title;
        $meta_description = $categoryDetails->meta_description;
        $meta_keywords = $categoryDetails->meta_keywords;
        return view('products.listing')->with(compact('category','categoryDetails','ProductAll','meta_title','meta_description','meta_keywords','url','colorArry','sizeArry'));

    }
    // products  function End
    
    public function filter(Request $request){
        $data = $request->all();
        //echo"<pre>" ; print_r($data) ; die;
        $ColorUrl = "";
        if(!empty($data['colorFilter'])){
            foreach($data['colorFilter'] as $color){
                if(empty($ColorUrl)){
                    $ColorUrl = "&color=".$color;
                }else{
                    $ColorUrl .= "-".$color;
                }
            }
        }

        $SizeUrl = "";
        if(!empty($data['sizeFilter'])){
            foreach($data['sizeFilter'] as $size){
                if(empty($SizeUrl)){
                    $SizeUrl = "&size=".$size;
                }else{
                    $SizeUrl .= "-".$size;
                }
            }
        }

        $finalUrl = "products/".$data['url']."?".$ColorUrl.$SizeUrl;
        return redirect::to($finalUrl);
    }

    //Start search Products function 
        public function searchProducts(Request $request){
            if($request->isMethod('post')){
                $data = $request->all();
                //echo "<pre>" ; print_r($data); die;
                $category = Category::with('category')->where(['parent_id'=>0])->paginate(6);
                $search_product = $data['product'];
                //% - The percent sign represents zero, one, or multiple characters
                /*$ProductAll = Product::where('product_name','like','%'.$search_product.'%')->orwhere('product_code',$search_product)
                ->where('status',1)->paginate(6);*/
                $ProductAll = Product::where(function($query) use ($search_product){
                    $query->where('product_name','like','%'.$search_product.'%')->
                    orwhere('product_code','like','%'.$search_product.'%')->
                    orwhere('product_color','like','%'.$search_product.'%');
                })->where('status',1)->paginate();
            }
            return view('products.listing')->with(compact('category','search_product','ProductAll'));
        }
    //End search Products function 



    // product Details  function Start
    public function product(Request $request, $id = null ){

        //show 404 not found if category url doesnt exist 

        $countProduct = Product::where(['id'=>$id , 'status'=> 1])->count();//these methods return the appropriate scalar value 
        if($countProduct==0){
            abort(404);
        }
        //Get Product Details
        $productDetaiels = Product::with('arrtibute')->where('id',$id)->first();
        $productDetaiels =  json_decode(json_encode($productDetaiels));
        //echo "<pre>" ; print_r($productDetaiels) ; die;
        //dump($productDetaiels);


        //$relatedProducts variable for getting all the other products of the same category
         $relatedProduct = Product::where('id','!=', $id)->where(['category_id'=>$productDetaiels->category_id])->get();
         $relatedProduct = json_decode(json_encode( $relatedProduct));
        //echo "<pre>" ; print_r($relatedProduct) ; die;
        
        //Get all category and sub category
        $category = Category::with('category')->where(['parent_id'=>0])->get();
        $category =  json_decode(json_encode($category));
       
        $catregoryDetails = Category::where('id', $productDetaiels->category_id)->first();
         //echo $catregoryDetails->name; die;
        //echo "<pre>" ; print_r($catregoryDetails) ; die;

         //Get product alternative image
        $ProductAlternativeImage = ProductsImage::where('product_id',$id)->get();
        $ProductColor = ProductColor::where('product_id',$id)->get();

        //$ProductColor =  json_decode(json_encode($ProductColor));
         //echo "<pre>" ; print_r($ProductColor) ; die;

        $total_stock = ProductAttributes::where('product_id',$id)->sum('stock');
        $meta_title = $productDetaiels->meta_title;
        $meta_description = $productDetaiels->meta_description;
        $meta_keywords = $productDetaiels->meta_keywords;
        /* foreach($category as $key =>$cat){
        $categoryDetails = Category::where('parent_id',$cat->parent_id)->first();
        $category[$key]->name= $categoryDetails->name;
        }*/
        // echo "<pre>" ; print_r($category); die;
      return view('products.details')->with(compact('productDetaiels','category','catregoryDetails','ProductAlternativeImage','total_stock','relatedProduct','ProductColor','meta_title','meta_description','meta_keywords'));

    }
    // product Details  function End


    // getProductPrice  function Start
    //where we will get the product attribute price by product id and its size and will return its price.
    public function getProductPrice(Request $request){
        $data = $request->all();
       // echo "<pre>" ; print_r($data) ; die;
        $proArr = explode("-",$data['idSize']);
        $proAttr = ProductAttributes::where(['product_id' => $proArr[0] , 'size'=>$proArr[1]])->first();
        $getCurrencyRate = Product::getCurrencyRates($proAttr->price);
        echo $proAttr->price."-".$getCurrencyRate['ILS_rate']."-".$getCurrencyRate['JOD_rate'];
        echo "#";
        echo $proAttr->stock;

    }
    // getProductPrice  function End


   // addtocart function Start
   public function addtocart(Request $request){

    Session::forget('couponAmount');
    Session::forget('couponCode');

    $data = $request->all();
    //echo "<pre>" ; print_r($data); die;

    if(!empty($data['cartWishlist']) && $data['cartWishlist']=="Wish List"){
        //echo "wishlist is selected " ;die;
        //Check user is logged in 
        if(!Auth::check()){
            return redirect()->back()->with('flash_message_error','Please login to add product in your wish list ');
        }else{
            
            if(empty($data['size'])){
                return redirect()->back()->with('flash_message_error','Under size missing ');
            }
            
            $SizeArr = explode("-",$data['size']);
            $Product_size = $SizeArr[1];

            $proPrice = ProductAttributes::where(['product_id'=>$data['product_id'] , 'size'=>$Product_size])->first();
            $product_price = $proPrice->price;
            // Get user email/username
            $user_email = Auth::user()->email;
            //Get Quentity as 1
            $quentity = 1; 
            //Get current date 
            $created_at = Carbon::now();

            $wishlistCount = DB::table('wish_list')->where(['product_id' =>$data['product_id'] , 'user_email'=>$user_email , 'size'=>$Product_size])->count();
            if($wishlistCount > 0){
                return redirect()->back()->with('flash_message_success','Product is already in wish list ');

            }

            //Insert product in wishlist table 
            DB::table('wish_list')->insert(['product_id' =>$data['product_id'] , 'product_name'=>$data['product_name'],
            'product_code'=>$data['product_code'] , 'product_color' =>$data['product_color'] ,  'price'=>$product_price , 
            'size'=>$Product_size ,'quantity' =>$quentity , 'user_email'=>$user_email ,'created_at'=>$created_at ]);
            return redirect()->back()->with('flash_message_success','Product has been added in wish list ');
            }
    }else{
        //echo"shopping cart selected" ; die;
        
        if(empty($data['size'])){
            return redirect()->back()->with('flash_message_error','Under size missing ');
        }
        $SizeArr = explode("-",$data['size']);
        $Product_size = $SizeArr[1];
        //echo "<pre>" ; print_r($Product_size); die;

            //Check product stock is avilable or not
        $getProductStock = ProductAttributes::where(['product_id'=>$data['product_id'] , 'size'=>$Product_size])->first();
        if($getProductStock->stock < $data['quantity']){
            return redirect()->back()->with('flash_message_error','Required Quentity is not available the stock available !');
        }
        
        if(empty(Auth::user()->email)){
            $data['user_email'] = '';    
        }else{
            $data['user_email'] = Auth::user()->email;
        }

        //need to creat sessionid variable to store the particular session of the user to cart table
        $session_id = Session::get('session_id');
        if(!isset($session_id)){
            // str_random method generates a random string of the specified length.
            $session_id = str_random(40);
            Session::put('session_id',$session_id);
        }


        if(!empty($data['colorFilter'])){

            $productFilter = $data['colorFilter'];
            $productFilter = json_decode(json_encode($productFilter));
        }


        if(!empty($productFilter)){

            foreach($productFilter as $color){
                $getColor = ProductColor::select('color')->where(['product_id'=>$data['product_id'] , 'color'=>$color])->first();
            }
    
            if(empty(Auth::check())){
                $countProduct =  DB::table('cart')->where(['product_id'=>$data['product_id'] ,'product_color' =>$getColor->color ,'product_name'=>$data['product_name'],
                'size'=>$Product_size,'session_id'=>$session_id])->count();
        
                if($countProduct >0){
        
                    return redirect()->back()->with('flash_message_error','Product Already exiest in Cart !');
                }
            }else{

                $countProduct =  DB::table('cart')->where(['product_id'=>$data['product_id'] , 'product_name'=>$data['product_name'],
                
                'product_color' =>$getColor->color,'size'=>$Product_size,'user_email'=> $data['user_email']])->count();
        
                if($countProduct >0){
        
                    return redirect()->back()->with('flash_message_error','Product Already exiest in Cart !');
                }
                
            }
        }else{
            if(empty(Auth::check())){
                $countProduct =  DB::table('cart')->where(['product_id'=>$data['product_id'] ,'product_color'=>$data['product_color'] ,'product_name'=>$data['product_name'],
                'size'=>$Product_size,'session_id'=>$session_id])->count();
        
                if($countProduct >0){
        
                    return redirect()->back()->with('flash_message_error','Product Already exiest in Cart !');
                }
            }else{

                $countProduct =  DB::table('cart')->where(['product_id'=>$data['product_id'] , 'product_name'=>$data['product_name'],
                
                'product_color'=>$data['product_color'],'size'=>$Product_size,'user_email'=> $data['user_email']])->count();
        
                if($countProduct >0){
        
                    return redirect()->back()->with('flash_message_error','Product Already exiest in Cart !');
                }
        }

    }


        $getSku = ProductAttributes::select('sku')->where(['product_id'=>$data['product_id'] , 'size'=>$Product_size])->first();
      
        //echo "<pre>" ; print_r($productFilter); die;
        
     
        if(!empty($productFilter)){

            foreach($productFilter as $color){
                $getColor = ProductColor::select('color')->where(['product_id'=>$data['product_id'] , 'color'=>$color])->first();
            }

            DB::table('cart')->insert(['product_id'=>$data['product_id'] , 'product_name'=>$data['product_name'],
            'product_code'=>$getSku->sku , 'product_color' =>$getColor->color , 'price'=>$data['price'],
             'size'=>$Product_size,'quantity' =>$data['quantity'] ,'user_email'=>$data['user_email'],
             'session_id'=>$session_id]);    
         
        }else{
            DB::table('cart')->insert(['product_id'=>$data['product_id'] , 'product_name'=>$data['product_name'],
            'product_code'=>$getSku->sku , 'product_color' =>$data['product_color'] , 'price'=>$data['price'],
             'size'=>$Product_size,'quantity' =>$data['quantity'] ,'user_email'=>$data['user_email'],
             'session_id'=>$session_id]);    
         
        }



     return redirect('cart')->with('flash_message_success','Product Added in Cart Successfully!');

    }
}
// addtocart function End



    // cart function Start
    //we can get all cart items and return to cart page.
    public function cart(){
        if (Auth::check()){
            $user_email = Auth::user()->email;
            $UserCart = DB::table('cart')->where( ['user_email' => $user_email])->get();
        }else{
            $session_id = Session::get('session_id');
            $UserCart = DB::table('cart')->where(['session_id' => $session_id])->get();
        }
        foreach($UserCart as $key =>$product){
            $productDetails = Product::where('id',$product->product_id)->first();
            $UserCart[$key]->image= $productDetails->image ;
        }

        //echo "<pre>" ; print_r($UserCart); die;
        $meta_title = "Shopping Cart - Karl Website";
        $meta_description = "View Shopping Cart of Karl Website";
        $meta_keywords="Shopping Cart , Karl , Karl Style , shopping ";
        return view('products.cart')->with(compact('UserCart' ,'meta_title','meta_description','meta_keywords'));
    }
    // cart function End


    public function wishList(){
        $user_email = Auth::user()->email;
        $UserWishlist = DB::table('wish_list')->where('user_email' , $user_email)->get();
        foreach($UserWishlist as $key =>$product){
            $productDetails = Product::where('id',$product->product_id)->first();
            $UserWishlist[$key]->image= $productDetails->image ;
        }
        $meta_title = "Whish List - Uzak Website";
        $meta_description = "View Whish List of Karl Website";
        $meta_keywords="Whish List , Karl , Karl Style , shopping ";
        return view('products.wish_list')->with(compact('UserWishlist' ,'meta_title','meta_description','meta_keywords'));
    }

// Just two function to return quentity and grand total from cart table to  front header  
    public static function Bag(){
        $total_bag = 0 ;
        if (Auth::check()){
            $user_email = Auth::user()->email;
            $UserCart = DB::table('cart')->where( ['user_email' => $user_email])->get();
            foreach($UserCart as $cart){
                $total_bag = $total_bag + round($cart->price * $cart->quantity) ; 
            }
        }else{
            $session_id = Session::get('session_id');
            $UserCart = DB::table('cart')->where(['session_id' => $session_id])->get();
            foreach($UserCart as $cart){
                $total_bag = $total_bag + round($cart->price * $cart->quantity) ; 
            }
        }
        return $total_bag;
        
    }

    public static function Quentity(){
        $quentity = 0;

        if (Auth::check()){
            $user_email = Auth::user()->email;
            $UserCart = DB::table('cart')->where( ['user_email' => $user_email])->get();
            foreach($UserCart as $cart){
                $quentity = $quentity + ($cart->quantity);
            }
        }else{
            $session_id = Session::get('session_id');
            $UserCart = DB::table('cart')->where(['session_id' => $session_id])->get();
            foreach($UserCart as $cart){
                $quentity = $quentity + ($cart->quantity);
            }
        }
        return $quentity;
        
    }

//End two static function

    public function DeleteProductCart($id = null){
        Session::forget('couponAmount');
        Session::forget('couponCode');

        DB::table('cart')->where('id',$id)->delete();

        return redirect('cart')->with('flash_message_success','Product Deleted Successfully!');
        
    }

    
    public function DeleteProductWishList($id = null){
        DB::table('wish_list')->where('id',$id)->delete();
        return redirect('/wish-list')->with('flash_message_success','Product Deleted Successfully!');
        
    }

    public function updateCartQuantity(Request $request ,$id = null , $quantity = null){
        
        Session::forget('couponAmount');
        Session::forget('couponCode');
  
        $getCartDetails = DB::table('cart')->where('id',$id)->first();
        $getAttributeSKU = ProductAttributes::where('sku',$getCartDetails->product_code)->first();
        echo $getAttributeSKU->stock; echo "--";
         $updatedQuentity = $getCartDetails->quantity +  $quantity;
         if($getAttributeSKU->stock >=$updatedQuentity){
            DB::table('cart')->where('id',$id)->increment('quantity',$quantity);
            return redirect('cart')->with('flash_message_success','Product quentity Updated Successfully!');
    
         }else{

            return redirect('cart')->with('flash_message_error','Requierd Product Quentity not available!');

         }
      
    }

     
    public function applyCoupon(Request $request){

        Session::forget('couponAmount');
        Session::forget('couponCode');

        $data = $request->all();
        //echo "<pre>" ; print_r($data); die;
        $couponCount = Coupon::where('coupon_code',$data['coupon_code'])->count();
        if($couponCount == 0){
            return redirect()->back()->with('flash_message_error','Coupon is not a valid !');
        }else{
            //with perform other checks like Active/InActive , Expiry date ..

            $couponDetails = Coupon::where('coupon_code',$data['coupon_code'])->first();
            //If coupon in InActive
            if($couponDetails->status==0){
                return redirect()->back()->with('flash_message_error','This Coupon is not Active !');
            }

            //If coupon expired 
            $expiry_date = $couponDetails->expiry_date ;
            $current_date = date('Y-m-d'); 

            if( $expiry_date < $current_date ){
                return redirect()->back()->with('flash_message_error','This Coupon is expired !');
            }

            //Copoun is Valid 

            //Get Card Total Amount
            if(Auth::check()){
                $user_email = Auth::user()->email;
                $UserCart = DB::table('cart')->where(['user_email'=>$user_email])->get();

            }else{
                $session_id = Session::get('session_id');
                $UserCart = DB::table('cart')->where(['session_id'=>$session_id])->get();
            }
           
            $total_amount = 0;
            foreach($UserCart as $item){
              $total_amount =  $total_amount +( $item->price * $item->quantity ); 
            }
            //Check if amount type fixed or percentage

            if($couponDetails->amount_type == "Fixed"){
                $couponAmount = $couponDetails->amount;
            }else{
                $couponAmount = $total_amount * ($couponDetails->amount/100)  ;
            }

            //Add Copoun Code & Amount in Session 
            Session::put('couponAmount',$couponAmount);
            Session::put('couponCode',$data['coupon_code']);


            return redirect()->back()->with('flash_message_success',' Coupon Code successfully applied availing discount !');

        }
    }

//Start checkout product function
    public function productCheckout(Request $request){
        $user_id = Auth::user()->id;
        $user_email = Auth::user()->email;

        $userDetailes = User::find( $user_id);
        //echo "<pre>" ; print_r($userDetailes ) ; die;
        $Countries = Countries::get();

        //check if Shiping address is existing 
        $shipingAddressCount = ShipingAddress::where('user_id',$user_id)->count();
        $shipingDetailes = array();
        if($shipingAddressCount > 0 ){
            $shipingDetailes = ShipingAddress::where('user_id',$user_id)->first();
        }
        $session_id = Session::get('session_id');
        DB::table('cart')->where(['session_id'=>$session_id])->update(['user_email'=>$user_email]);



        if($request->isMethod('post')){
            $data = $request->all();
            //echo "<pre>" ; print_r($data ); die;
            //Return to checkout page if any fields empty 
            /*  if(empty($data['shiping_name']) || empty($data['shiping_address']) || empty($data['shiping_city']) ||
                empty($data['shiping_state']) || empty($data['shiping_country']) || empty($data['shiping_pincode']) || 
                empty($data['shiping_phone']) || empty($data['billing_name']) || empty($data['billing_address']) || 
                empty($data['billing_city']) || empty($data['billing_state']) || empty($data['billing_country']) || 
                empty($data['billing_pincode']) || empty($data['billing_phone'])){

                    return redirect()->back()->with('flash_message_error' , 'Please fill all fields to Checkout');
                    
                }*/

                //Update user details
                User::where('id',$user_id)->update(['name'=>$data['billing_name'],'address'=>$data['billing_address'],
                'city'=>$data['billing_city'], 'state'=>$data['billing_state'], 'country'=>$data['billing_country'], 
                'pincode'=>$data['billing_pincode'],'mobile'=>$data['billing_phone']]);

                if($shipingAddressCount >0 ){
                    //Update Shiping Address
                    ShipingAddress::where('user_id',$user_id)->update(['name'=>$data['shiping_name'],'address'=>$data['shiping_address'],
                    'city'=>$data['shiping_city'], 'state'=>$data['shiping_state'], 'country'=>$data['shiping_country'], 
                    'pincode'=>$data['shiping_pincode'],'mobile'=>$data['shiping_phone']]);

                }else{
                    //Add new Shiping Address
                    $shiping = new ShipingAddress;
                    $shiping->user_id = $user_id;
                    $shiping->user_email = $user_email;
                    $shiping->name = $data['shiping_name'];
                    $shiping->address = $data['shiping_address'];
                    $shiping->city = $data['shiping_city'];
                    $shiping->state = $data['shiping_state'];
                    $shiping->country = $data['shiping_country'];
                    $shiping->pincode = $data['shiping_pincode'];
                    $shiping->mobile = $data['shiping_phone'];
                    $shiping->save();

                }
                return redirect()->action('ProductsController@orderReview');

        }

        return view('products.checkout')->with(compact('userDetailes','Countries','shipingDetailes'));
    }
//End productCheckout function


//Start Order review function to show the checkout product
    public function orderReview(){


        $user_id = Auth::user()->id;
        $user_email = Auth::user()->email;
        $userDetailes =  User::where('id',$user_id)->first();
        $shipingDetailes = ShipingAddress::where('user_id',$user_id)->first();
        $shipingDetailes = json_decode(json_encode($shipingDetailes )); //It is for debugging the code and converting the json to array at times..

        $session_id = Session::get('session_id');
        $UserCart = DB::table('cart')->where(['user_email'=>$user_email])->get();
        $total_wight = 0;
        foreach($UserCart as $key =>$product){
            $productDetails = Product::where('id',$product->product_id)->first();
            $UserCart[$key]->image= $productDetails->image ;
            $total_wight = $total_wight + $productDetails->weight;
        }
        //echo $wight_total ; die;
        $ShippingCharges = Product::getShippingCharges($total_wight, $shipingDetailes->country);
        Session::put('ShippingCharges',$ShippingCharges);

        return view('products.order_review')->with(compact('shipingDetailes','userDetailes' ,'UserCart','productDetails'));
    }
//End function order review

//Start Delete Product in orderReview blade 
    public function DeleteProductorderReview($id = null){
        Session::forget('couponAmount');
        Session::forget('couponCode');

        DB::table('cart')->where('id',$id)->delete();

        return redirect('order-review')->with('flash_message_success','Product Deleted Successfully!');
    }
//End Delete Product in orderReview blade 

//Stare place order function
    public function placeOrder(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            $user_id = Auth::user()->id;
            $user_email = Auth::user()->email;

            //Prevent out of stock products from ordering 
            $userCart = DB::table('cart')->where('user_email',$user_email)->get();
            $userCart = json_decode(json_encode($userCart));
            //echo "<pre>" ; print_r($userCart); die;
            foreach($userCart as $cart){

                $getAttributeCount =  Product::getAttributeCount($cart->product_id , $cart->size );
                if($getAttributeCount == 0){
                    Product::deleteCartProduct($cart->product_code , $user_email);
                    return redirect('/cart')->with('flash_message_error','One Product not available ! Please Choose another product');
                }

                //echo $cart->product_color; die;
                $product_stock = Product::getProductStock($cart->product_id , $cart->size );
                if($product_stock == 0){
                    Product::deleteCartProduct($cart->product_code , $user_email);
                    return redirect('/cart')->with('flash_message_error','Sold out product removed from cart   ! Please Choose another product');
                }
                if($cart->quantity > $product_stock){
                    return redirect('/cart')->with('flash_message_error','Product is Sold out ! Reduce Product Stock and try again .');

                }

                $product_status = Product::getProductStatus($cart->product_id);
                if($product_status == 0){
                    Product::deleteCartProductdisable($cart->product_id , $user_email);
                    return redirect('/cart')->with('flash_message_error','Disabled product removed from cart! Please Choose another product');

                }
                $getCategoryId = Product::select('category_id')->where('id',$cart->product_id)->first();
                $category_status = Product::getCategoryStatus($getCategoryId->category_id);
                if($category_status == 0){
                    Product::deleteCartProductdisable($cart->product_id , $user_email);
                    return redirect('/cart')->with('flash_message_error','Disabled category product removed from cart! Please Choose another product');

                }
            }

            //Get shipping address of user 

            $shipingDetailes = ShipingAddress::where(['user_email'=>$user_email])->first();
           // echo "<pre>" ; print_r($data); die;
           if(empty(Session::get('couponCode'))){
               $coupon_code='';
            }else{
                $coupon_code = Session::get('couponCode'); 
            }

           if(empty(Session::get('couponAmount'))){
            $coupon_amount=0.0;
            }else{
                $coupon_amount = Session::get('couponAmount'); 
             }

            //$grand_total = Product::getGrandTotal();

            $order = new Order;
            $order->user_id = $user_id;
            $order->user_email =  $user_email;
            $order->name = $shipingDetailes->name;
            $order->address = $shipingDetailes->address;
            $order->city = $shipingDetailes->city;
            $order->state = $shipingDetailes->state;
            $order->pincode = $shipingDetailes->pincode;
            $order->country = $shipingDetailes->country;
            $order->mobile = $shipingDetailes->mobile;
            $order->shipping_charges = Session::get('ShippingCharges');

            $order->coupon_code =$coupon_code;
            $order->coupon_amount =$coupon_amount;

            $order->order_status = "New";
            $order->payment_method = $data['payment_method'];
            $order->grand_total = $data['grand_total'];
            $order->save();

            $order_id = DB::getPdo()->lastInsertId(); //Return the id of the current insert
            $cartProduct = DB::table('cart')->where(['user_email'=>$user_email])->get();
            foreach($cartProduct as $product){
                $cartPro = new OrdersProduct ;
                $cartPro->order_id = $order_id;
                $cartPro->user_id = $user_id;;
                $cartPro->product_id = $product->product_id;
                $cartPro->product_code = $product->product_code;
                $cartPro->product_name = $product->product_name;
                $cartPro->product_size = $product->size;
                $cartPro->product_color = $product->product_color;
                $product_price = Product::getProductPrice($cart->product_id ,$cart->size );

                $cartPro->product_price = $product_price;
                $cartPro->product_qty = $product->quantity;

                $cartPro->save();
                // Reduce Stock Script Starts
                $getProductStock = ProductAttributes::where('sku',$product->product_code)->first();
                $newStock = $getProductStock->stock - $product->quantity;
                ProductAttributes::where('sku',$product->product_code)->update(['stock'=>$newStock]);
                // Reduce Stock Script Ends
            }
            Session::put('order_id',$order_id);
            Session::put('grand_total',$data['grand_total']);

            if($data['payment_method'] == "COD"){
                $productDetaiels = Order::with('orders')->where('id',$order_id)->first();
                $productDetaiels = json_decode(json_encode($productDetaiels),true);
               // echo "<pre>" ; print_r($productDetaiels) ; die;
                $userDetailes = User::where('id',$user_id)->first();
                $userDetailes = json_decode(json_encode($userDetailes),true);
                //echo "<pre>" ; print_r($userDetailes) ; die;
            // Code of order email Start
                 $email = $user_email;
                 $MassegeData = [
                     'email'=>$email,
                     'name'=>$shipingDetailes->name,
                     'order_id'=>$order_id,
                     'productDetaiels'=>$productDetaiels,
                     'userDetailes'=>$userDetailes                   
                    ];
                 Mail::send('emails.order',$MassegeData,function($message) use ($email){
                    $message->to($email)->subject('Orderd Placed -E-commerce ');
                  });
            // Code of order email End

            //COD Redirect user to thank page after saving order
            return redirect('/Thanks');
            }else{
            //PayPal Redirect user to thank page after saving order
            return redirect('/paypal');
            }
        

        }
    }
//End place order function

//Start Thanks function
    public function Thanks(Request $request){
        $user_email =  Auth::user()->email;
        DB::table('cart')->where('user_email',$user_email)->delete();

        return view('orders.thanks');
    }
//End Thanks function

//Start Thanks PayPal Function
    public function Thankspaypal(){
        return view('orders.thanks');
    }
//End Thanks function

//
    public function Cancelpaypal(){
        return view('orders.Cancel');
    }
//
//Start paypal function

    public function paypal(){
        $user_email =  Auth::user()->email;
        DB::table('cart')->where('user_email',$user_email)->delete();
        return view('orders.paypal');
    }

//End paypal function


//Start userOrders function
    public function userOrders(){
        $user_id = Auth::user()->id;
        $orders = Order::with('orders')->where('user_id',$user_id)->orderBy('id','desc')->get(); 
       /* $orders = json_decode(json_encode($orders));
        echo "<pre>"  ; print_r($orders); die;*/
        return view('orders.user_orders')->with(compact('orders'));
    }
//End userOrders function

//Start user Ordered Product Details function
    public function userOrdersDetails($order_id){
            $user_id = Auth::user()->id;
            $orderDetails = Order::with('orders')->where('id',$order_id)->first();
            $orderDetails = json_decode(json_encode($orderDetails));
            //echo "<pre>" ; print_r($orderDetails); die;
            return view('orders.orderd_product')->with(compact('orderDetails'));
    }
//End User Orderd Product Details Function

//Start View Orders Function
    public function viewOrders(){
        $orders = Order::with('orders')->orderBy('id','Desc')->get();
        $orders = json_decode(json_encode($orders));

        //echo "<pre>" ; print_r($orders) ; die;
        return view('admin.orders.view_orders')->with(compact('orders'));

    }
//End View Orders Function

//Start View Orders Details Function

    public function viewOrdersDetails($order_id){
        $orderDetails = Order::with('orders')->where('id',$order_id)->first();
        $orderDetails = json_decode(json_encode($orderDetails));
        //echo "<pre>" ; print_r($orderDetails); die;
        $user_id = $orderDetails->user_id; 
        $userDetails = User::where('id',$user_id)->first();
        //$userDetails = json_decode(json_encode($userDetails));
        //echo "<pre>" ; print_r($userDetails); die;
        return view('admin.orders.order_details')->with(compact('orderDetails','userDetails'));
    }
//End View Orders Details Function

//Start Update Order Status Function 
public function updateOrderStaus(Request $request){
    if($request->isMethod('post')){
        $data= $request->all();
        //echo "<pre>" ; print_r($data) ; die;
        $id = $data['order_id'];
        $status = $data['order_status'];
        Order::where('id',$id)->update(['order_status'=>$status]);

        return redirect()->back()->with('flash_message_success','Banners Updated Successfully');
    }

 }

//End Update Order Status Function 

//Start view Order Invoice Function 

public function viewOrderInvoice($order_id){
    $orderDetails = Order::with('orders')->where('id',$order_id)->first();
    $orderDetails = json_decode(json_encode($orderDetails));
    //echo "<pre>" ; print_r($orderDetails); die;
    $user_id = $orderDetails->user_id; 
    $userDetails = User::where('id',$user_id)->first();
    //$userDetails = json_decode(json_encode($userDetails));
    //echo "<pre>" ; print_r($userDetails); die;
    return view('admin.orders.order_invoice')->with(compact('orderDetails','userDetails'));
}
//End view Order Invoice Function 

}

 