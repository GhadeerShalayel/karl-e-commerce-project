<?php

namespace App\Http\Controllers;
use Image;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    public function add_Category(Request $request){
        if ($request->isMethod('post')){
            $data = $request->all();
            //echo "<pre>" ; print_r($data) ; die;
            $category = new Category;
            $category->name = $data['category_name'];
            $category->parent_id = $data['parent_id'];
            $category->description = $data['description'];
            $category->url = $data['url'];
            $category->meta_title = $data['meta_title'];
            $category->meta_description = $data['meta_description'];
            $category->meta_keywords = $data['meta_keywords'];
            
            if(empty($data['status'])){
                $status='0';
            }else{
                $status='1';
            }

            if(empty($data['top_category'])){
                $top_category='0';
            }else{
                $top_category='1';
            }

            if(empty( $data['meta_title'])){

                $data['meta_title']='';
            }
            if(empty( $data['meta_description'])){

                $data['meta_description']='';
            }
            if(empty( $data['meta_keywords'])){

                $data['meta_keywords']='';
            }


            
            if(!empty($data['title'])){
                $category->title = $data['title'];
            }else{
                $category->title ='';
            }

            $category->status = $status;
            $category->top_category = $top_category;

            if($request->hasFile('image')){
                $image_tmp = input::file('image');
                if($image_tmp->isValid()){
                    $extinsion = $image_tmp->getClientOriginalExtension();
                    $file_name = rand(111,999).'.'.$extinsion;
                    $image_path = 'images/frontend_images/top_category/'.$file_name;
                    //Resize Image
                 Image::make($image_tmp)->save($image_path);
                 //Store image name in products table
                 $category->image = $file_name;
                }
            }


            $category ->save();
            return redirect('/admin/add-category')->with('flash_message_success','Category added Successfully!');
        }
        $levels = Category::where(['parent_id'=>0])->get();
        return view('admin.categories.add_category')->with(compact('levels')); 
    }




    public function view_Category(){
        $categories = Category::get();
        return view('admin.categories.view_category')->with(compact('categories'));
    }




    public function edit_Category(Request $request , $id = null){
        if($request->isMethod('post')){

        $data = $request->all();
           // echo "<pre>"; print_r($data); die;
            if($request->hasFile('image')){
                $image_tmp = input::file('image');
                    if($image_tmp->isValid()){
                        $extinsion = $image_tmp->getClientOriginalExtension();
                        $file_name = rand(111,999).'.'.$extinsion;
                        $image_path = 'images/frontend_images/top_category/'.$file_name;
                        //Resize Image
                    Image::make($image_tmp)->save($image_path);
                    }
            }else{
                $file_name = $data['image'];
            }
           if(empty($data['status'])){
                $status='0';
            }else{
                $status='1';
            }

            if(empty($data['top_category'])){
                $top_category='0';
            }else{
                $top_category='1';
            }
            
            if(empty( $data['meta_title'])){

                $data['meta_title']='';
            }
            if(empty( $data['meta_description'])){

                $data['meta_description']='';
            }
            if(empty( $data['meta_keywords'])){

                $data['meta_keywords']='';
            }

         

            if(empty( $data['title'])){

                $data['title']='';
            }


           Category::where(['id'=>$id])->update(['name'=>$data['category_name'] ,//name in form 
                                                 'description'=>$data['description'] ,
                                                  'url'=>$data['url'],
                                                  'status'=>$status,
                                                  'image'=>$file_name,
                                                  'top_category'=>$top_category,
                                                  'title'=>$data['title'],
                                                  'meta_title'=>$data['meta_title'],
                                                  'meta_description'=>$data['meta_description'],
                                                  'meta_keywords'=>$data['meta_keywords']]);
                                                  
         return redirect('/admin/view-category')->with('flash_message_success','Category updated Successfully!');
        }
        $categoryDetaiels = Category::where(['id'=>$id])->first();
        $levels = Category::where(['parent_id'=>0])->get();
        return view('admin.categories.edit_category')->with(compact('categoryDetaiels','levels'));
    }




    
    public function delete_Category(Request $request , $id = null){
        if(!empty($id)){
            Category::where(['id'=>$id])->delete();
            return redirect()->back()->with('flash_message_success','Category deleted Successfully!');
        }
    }
}
