<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use Image;
use App\Banner;

class BannersController extends Controller
{
    public function addBanner(Request $request){
        
        if($request->isMethod('post')){
            $data = $request->all();
           // echo "<pre>" ; print_r($data);die;

            $banner = new Banner;
          
            $banner->title = $data['title'];
            $banner->link = $data['link'];

        

            if(empty($data['status'])){
                $status='0';
                }else{
                $status='1';
            }

           if(!empty($data['description'])){
                $banner->description = $data['description'];
            }else{
                $banner->description ='';
            }


            if($request->hasFile('image')){

                $image_tmp = input::file('image');
                if($image_tmp->isValid()){
                   $extinsion = $image_tmp->getClientOriginalExtension();
                   $file_name = rand(111,999).'.'.$extinsion;
                   $image_path = 'images/frontend_images/banners/'.$file_name;
                   //Resize Image

                   Image::make($image_tmp)->save($image_path);
                //Store image name in products table
                $banner->image = $file_name ;
               }
            }

            $banner->status = $status;
            $banner->save();

           // return redirect()->back()->with('flash_message_success','Banner added Successfully');

           return redirect('/admin/view-banner')->with('flash_message_success','Banners added Successfully');


        }

        return view('admin.banners.add_banner');
    }


    public function viewBanner(){
        $banners = Banner::get();
        return view('admin.banners.view_banner')->with(compact('banners'));
    }



    public function editBanner(Request $request , $id = null){

        if($request->isMethod('post')){
            $data = $request->all();

            
            if(empty($data['status'])){
                $status='0';
            }else{
                $status='1';
            }
            
            if(empty($data['title'])){
                $data['title'] = '';
            }

            if(empty($data['link'])){
                $data['link'] = '';
            }

            if(empty( $data['description'])){

                $data['description']='';
            }

           // Upload Image
           if($request->hasFile('image')){
            $image_tmp = Input::file('image');
            if ($image_tmp->isValid()) {
                // Upload Images after Resize
                $extension = $image_tmp->getClientOriginalExtension();
                $fileName = rand(111,99999).'.'.$extension;
                $banner_path = 'images/frontend_images/banners/'.$fileName;
                Image::make($image_tmp)->save($banner_path);
            }
            }else{
                $fileName = $data['image'];
            }


        Banner::where('id',$id)->update(['status'=>$status,'title'=>$data['title'],'description'=>$data['description'],'link'=>$data['link'],'image'=>$fileName]);
        return redirect('/admin/view-banner')->with('flash_message_success','Banners Updated Successfully');

        }
        
        $bannerDetaiels = Banner::where(['id'=>$id])->first();

        return view('admin.banners.edit_banner')->with(compact('bannerDetaiels'));

    }

        
    public function deleteBanner(Request $request , $id = null){
        if(!empty($id)){
            Banner::where(['id'=>$id])->delete();
            return redirect()->back()->with('flash_message_success','Banner deleted Successfully!');
        }
    }







 } 

 



