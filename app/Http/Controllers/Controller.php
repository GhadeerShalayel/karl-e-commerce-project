<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Category;
use App\Coupon;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /*
Create function this time in Controller.php so that we can include it in front header file that is common for all the frontend pages.
We will create mainCategories function where we will get all the main categories that is having parent id 0.
And this time we are going to add static function and return the variable.*/


    public static function mainCategories(){

        $mainCategories = Category::with('category')->where(['parent_id'=>0])->get();
      /*$mainCategories = json_decode(json_encode($mainCategories));
        echo "<pre>" ; print_r($mainCategories); die;*/


        return $mainCategories;
    }

    public static function category(){
        $category = Category::with('category')->where(['parent_id'=>0])->get();
        return $category;

    }

    public static function viewcopoun(){
        $coupons = Coupon::orderBy('id','DESC')->where(['status'=>1])->get();
        //echo "<pre>" ; print_r($coupons); die;

       return $coupons;
    }

    

}
