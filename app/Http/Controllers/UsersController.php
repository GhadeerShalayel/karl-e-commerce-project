<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Session;
use App\User;
use App\countries;
use DB;

class UsersController extends Controller
{

    public function userLoginRegister(){
        return view('users.login_register');

    }

  

    public function register(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
           // echo "<pre>" ; print_r($data) ; die;
           //check if user account already exist 
           $userCount = User::where('email',$data['email'])->count();
           if( $userCount >0){
               return redirect()->back()->with('flash_message_error','Email already exists!');
           }else{
               $user = new User;
               $user->name=$data['name'];
               $user->email=$data['email'];
               $user->password=bcrypt($data['pass']);
               date_default_timezone_set('Asia/Gaza');
               $user->created_at = date("Y-m-d H:i:s");
               $user->updated_at = date("Y-m-d H:i:s");
               $user->save();
               /*
               //Send Register Email
               $email = $data['email'];
               $name = $data['name'];
               $MassegeData = ['email'=>$data['email'],'name'=>$data['name']];
               Mail::send('emails.register',$MassegeData,function($message) use ($email){
                   $message->to($email)->subject('Register to Karl E-commerce Website');
               });
               */

                //Send Confirmation Email
                $email = $data['email'];
                $name = $data['name'];
                $MassegeData = ['email'=>$data['email'],'name'=>$data['name'] , 'code'=>base64_encode($data['email'])];
                Mail::send('emails.confirmation',$MassegeData,function($message) use ($email){
                    $message->to($email)->subject('Confirm your Karl E-commerce Account');
                    
                });
                return redirect()->back()->with('flash_message_success','Please confirm your email to active your account !');

            if (Auth::attempt(['email' =>$data['email'], 'password' =>$data['pass'] ] )) {
                Session::put('frontSession',$data['email']);

                if(!empty(Session::get('session_id'))){
                    $session_id = Session::get('session_id');
                    DB::table('cart')->where('session_id',$session_id)->update(['user_email' => $data['email']]);
                }

                return redirect('/')->with('flash_message_success','You are registerd Successflly!');
            }

           }

        }
        return view('users.register');

    }

//Start Confirm Account Function
    public function confirmAccount($email){
         $email =  base64_decode($email) ;
         $userCount = User::where('email',$email)->count();
         if($userCount > 0){
           /* check the status of the user if user email valid.
           If status is 1 then we will return the user back to login page with message that
          'Your Email account is already activated. You can login now! otherwise we are going
           to make status 1 and will send welcome/register email to user.*/
            $userDetailes = User::where('email',$email)->first();
            $UserStatus = $userDetailes->status;
            if($UserStatus == 1){
                return redirect('/user-login')->with('flash_message_success','Your Email account is already activated. You can login now!');
            }else{
                User::where('email',$email)->update(['status'=>'1']);

                //Send Welcome Email
               $MassegeData = ['email'=>$email,'name'=>$userDetailes->name];
               Mail::send('emails.welcome',$MassegeData,function($message) use ($email){
                   $message->to($email)->subject('Welcome to Karl E-commerce Website');
               });

                return redirect('/user-login')->with('flash_message_success','Your Email account is activated. You can login now!');
            }
         }else{
             abort(404);
         }

    }
//End Confirm Account Function
    public function login(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            /*echo "<pre>"; print_r($data); die;*/
            if(Auth::attempt(['email'=>$data['email'],'password'=>$data['your_pass']])){
                $UserStatus = User::where('email',$data['email'])->first();
                if($UserStatus->status == 0){
                    return redirect()->back()->with('flash_error','Your Account has not activated ! Please confirm your email .');
                }else{
                    Session::put('frontSession',$data['email']);
                }

                if(!empty(Session::get('session_id'))){
                    $session_id = Session::get('session_id');
                    DB::table('cart')->where('session_id',$session_id)->update(['user_email' => $data['email']]);
                }

                return redirect('/cart');
            }else{
                return redirect()->back()->with('flash_error','Invalid Username or Password!');
            }
        }
        return view('users.login');

    }

    public function forgot_password(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
           // echo "<pre>" ; print_r($data) ; die;
           $email = $data['email'];
           $userCount = User::where('email',$email)->count();
           if($userCount == 0 ){
            return redirect()->back()->with('flash_error','Email doesent Exist!');

           }else{
            //Get User Details 
            $userDetails = User::where('email',$email)->first();
            $userDetails = json_decode(json_encode($userDetails));
            //echo "<pre>" ; print_r($userDetails) ; 
            $random_password = str_random(8);
            $new_password = bcrypt($random_password);
            $name = $userDetails->name;
             //echo "<pre>" ; print_r($name) ; die;
            //Update  Password 
            User::where('email',$email)->update(['password'=>$new_password]);
            //send forget password email code

            $MassegeData = ['email'=>$email,'name'=>$name , 'password'=>$random_password];
            Mail::send('emails.forgetPassword',$MassegeData,function($message) use ($email){
                $message->to($email)->subject('New Password - KARL E-commerce');
            });
            return redirect('/user-login')->with('flash_message_success','Please check your email for new password!');

           }
        }
        return view('users.forgot_password');
    }


    public function account(Request $requset){
        $user_id = Auth::user()->id;
        $userDetailes = User::find( $user_id);
        //echo "<pre>" ; print_r($userDetailes ) ; die;
        $Countries = Countries::get();

        if($requset->isMethod('post')){
            $data = $requset->all();
            $user = User::find($user_id);

            if(empty($data['name'])){
                return redirect()->back()->with('flash_message_error','Please enter your name to update your account!');

            }

            if(empty($data['address'])){
                $data['address'] = '';    
            }

            if(empty($data['city'])){
                $data['city'] = '';    
            }

            if(empty($data['state'])){
                $data['state'] = '';    
            }

            if(empty($data['country'])){
                $data['country'] = '';    
            }

            if(empty($data['pincode'])){
                $data['pincode'] = '';    
            }

            if(empty($data['mobile'])){
                $data['mobile'] = '';    
            }


            $user->name = $data['name'];
            $user->address = $data['address'];
            $user->city = $data['city'];
            $user->state = $data['state'];
            $user->country = $data['country'];
            $user->pincode = $data['pincode'];
            $user->mobile = $data['mobile'];
            $user->email = $data['email'];

            $user->save();

            return redirect()->back()->with('flash_message_success','Account Updated Successflly!');
        }
        return view('users.account')->with(compact('Countries','userDetailes'));

    }



  

    public function logout(){
        Auth::logout();
        Session::forget('frontSession');
        Session::forget('session_id');

        return redirect('/');

    }

    public function checkEmail(Request $request){
        $data = $request->all();

        $userCount = User::where('email',$data['email'])->count();
        if( $userCount >0){
            echo "false";
        }else{
            echo "true"; die;
        }
    }

 

    public function chkPassword(Request $request){ /*Now we have created checkPassword function 
'H       where we have checked current password entered by the user is correct or not.
        We have returned true or false message to Ajax to display the message on our form.*/
        $data = $request->all();
        /*echo "<pre>"; print_r($data); die;*/
        $current_password = $data['current_pwd'];
        $user_id = Auth::User()->id;
        $check_password = User::where('id',$user_id)->first();
        if(Hash::check($current_password,$check_password->password)){
            echo "true"; die;
        }else{
            echo "false"; die;
        }
    }

    
    public function updatePassword(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            $user_id = Auth::user()->id;
            $check_password = User::where(['id' => $user_id])->first();
            $current_password = $data['current_pwd'];
            if(Hash::check($current_password,$check_password->password)){
                $password = bcrypt($data['new_pwd']);
                User::where('id', $user_id)->update(['password'=>$password]);
                return redirect('/account')->with('flash_success','Password updated Successfully!');
            }else {
                return redirect('/account')->with('flash_error','Incorrect Current Password!');
            }
        }
    }

//Start View User function in Admin
    public function viewUsers(){
        $UserDetails = User::get()->all();
        $UserDetails = json_decode(json_encode($UserDetails));
        //echo "<pre>" ; print_r($UserDetails) ;die;

        return view('admin.Users.users')->with(compact('UserDetails'));
    }
//End View User function in Admin



  
}   