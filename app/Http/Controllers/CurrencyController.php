<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Currency;

class CurrencyController extends Controller
{
    public function addCurrency(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            //echo "<pre>"; print_r($data); die;
            $currency = new Currency;
            $currency->currency_code = $data['currency_code'];
            $currency->exchange_rate = $data['exchange_rate'];
            if(empty($data['status'])){
                $status='0';
                }else{
                $status='1';
            }
            $currency->status = $status;
            $currency->save();
            return redirect()->back()->with('flash_message_success','Currency added Successfully');

        }
        return view('admin.currencies.add_currency');
    }

    public function viewCurrencies(){
        $CurrencyDetails = Currency::get()->all();
        $CurrencyDetails = json_decode(json_encode($CurrencyDetails));
       // echo "<pre>" ; print_r($CurrencyDetails) ;die;

        return view('admin.currencies.view_currencies')->with(compact('CurrencyDetails'));
    }

    public function editCurrency(Request $request , $id = null){
        if($request->isMethod('post')){
            $data = $request->all();

            if(empty($data['status'])){
                $status='0';
            }else{
                $status='1';
            }

            Currency::where(['id'=>$id])->update(['currency_code'=>$data['currency_code'] ,//name in form 
             'exchange_rate'=>$data['exchange_rate'],
             'status'=>$status]);
             return redirect('/admin/view-currencies')->with('flash_message_success','Coupon updated Successfully!');

        }
        $CurrencyDetails = Currency::where(['id'=>$id])->first();

        return view('admin.currencies.edit_currency')->with(compact('CurrencyDetails'));

    }
}
