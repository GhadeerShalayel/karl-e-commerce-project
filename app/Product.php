<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ProductAttributes;
use DB;
use App\Product;
use App\ShippingCharges;
use Auth ;
use Session;
class Product extends Model
{
    public function arrtibute(){

        return $this->hasMany('App\ProductAttributes','product_id');
    }

    protected $table = "products";
    protected $filltable = [
        
        'category_id',
        'product_name',
        'product_code',
        'product_color',
        'description',
        'care',
        'price',
        'image',
        'status',
        'created_at',
        'updated_at'

    ];

    public static function getCurrencyRates($price){
        $getCurrencyRate = Currency::where('status',1)->get();
        foreach($getCurrencyRate as $currency){
            if($currency->currency_code=="ILS"){
                $ILS_rate = round($price*$currency->exchange_rate,2);
            }else if($currency->currency_code=="JOD"){
                $JOD_rate = round($price/$currency->exchange_rate,2);
            }
        }
        $CurrencyArr = array('ILS_rate'=>$ILS_rate , 'JOD_rate'=>$JOD_rate);
        return $CurrencyArr;
    }

 
    public static function getProductStock($product_id , $product_size){
        $getProductStock = ProductAttributes::select('stock')->where(['product_id'=>$product_id , 'size'=>$product_size])->first();
        return $getProductStock->stock;
    }

    public static function getProductPrice ($product_id , $product_size){
        $getProductPrice = ProductAttributes::select('price')->where(['product_id'=>$product_id , 'size'=>$product_size])->first();
        return $getProductPrice->price;

    }

    public static function deleteCartProduct($product_code , $user_email){
        DB::table('cart')->where(['product_code'=>$product_code , 'user_email'=>$user_email])->delete();
    }

    public static function getProductStatus($product_id){
        $getProductStatus = Product::select('status')->where('id',$product_id )->first();
        return $getProductStatus->status;
    }

    public static function deleteCartProductdisable($product_id , $user_email){
        DB::table('cart')->where(['product_id'=>$product_id , 'user_email'=>$user_email])->delete();
    }

    
    public static function getCategoryStatus($category_id){
        $getCategoryStatus = Category::select('status')->where('id',$category_id)->first();
        return $getCategoryStatus->status;
    }


    public static function getAttributeCount ($product_id , $product_size){
        $getAttributeCount =  ProductAttributes::where(['product_id'=>$product_id , 'size' => $product_size])->count();
        return $getAttributeCount;

    }

    public static function getShippingCharges($total_wight,$country){
        $ShippingDetails = ShippingCharges::where('country',$country)->first();
        if($total_wight >= 0){
            if($total_wight >= 0 && $total_wight <= 500){
                $ShippingCharges = $ShippingDetails->shipping_charges0_500g;
            }else if ($total_wight >= 501 && $total_wight <= 1000){
                $ShippingCharges = $ShippingDetails->shipping_charges501_1000g;

            }else if ($total_wight >= 1001 && $total_wight <= 2000){
                $ShippingCharges = $ShippingDetails->shipping_charges1001_2000g;

            }else if ($total_wight >= 20001 && $total_wight <= 5000){
                $ShippingCharges = $ShippingDetails->shipping_charges2001g_5000g;
            }else{
                $ShippingCharges = 0 ;

            }
        }else{

            $ShippingCharges = 0 ;
        }
        return $ShippingCharges; 
    }
   // Secure Grand Total at Order placement
   /* public static function getGrandTotal (){
        $getGrandTotal = '';
        $useremail = Auth::user()->email;
        $UserCart = DB::table('cart')->where('user_email',$useremail)->get();
        $UserCart = json_decode(json_encode($UserCart),true);
        //echo "<pre>" ; print_r($UserCart); die;
        foreach($UserCart as $product){
            $productPrice = ProductAttributes::where(['product_id'=>$product['product_id'] , 'size'=>$product['size']])->first();
            $priceArray[] = $productPrice->price;
        }
        //echo print_r($priceArray); die;
        $grand_total = array_sum($priceArray) - Session::get('couponAmount') + Session::get('ShippingCharges') ;
        return $grand_total;
    }*/

  


}
