@extends('layouts.frontlayouts.front_design')
@section('content')

    <!-- /.wrapper end -->
    <div class="main">

<!-- Sign up form -->
            @if(Session::has('flash_error'))    
              
              <div class="alert alert-danger alert-block alert_message" >
                 <button type="button" class="close" data-dismiss="alert">×</button>	
                   <strong> {!! session('flash_error') !!} </strong>
                </div>
                @endif

                @if(Session::has('flash_message_success'))  
                    <div class="alert alert-success alert-block alert_message">
                        <button type="button" class="close" data-dismiss="alert">×</button>	
                        <strong> {!! session('flash_message_success') !!} </strong>
                        </div>
             @endif

             @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        <!-- Sing in  Form -->
        <section class="sign-in" id ="signin">
            <div class="container-register">
                <div class="signin-content" >
                 
                    <div class="signin-form">
                        <h2 class="form-title">Contact Us</h2>
                        <form method="post" action ="{{url('/page/contact')}}" class="login-form" id="login-form">{{csrf_field()}}
                            <div class="row">
                                <!--Grid column-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="registerlabel" for="name" ><i class="ti-user"></i></label>
                                        <input class="inputRegieter" type="text" name="name" id="name" placeholder="Your Name"/>    
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="registerlabel" for="email"><i class="ti-email"></i></label>
                                        <input class="inputRegieter" type="email" name="email" id="email" placeholder="Your Email"/>
                                    </div>
                                </div>
                                <!--Grid column-->
                                </div> <br>

                                <div class="form-group">
                                    <label class="registerlabel" for="subject"><i class="ti-wand"></i></label>
                                    <input class="inputRegieter" type="text" name="subject" id="subject" placeholder="Subject"/>
                                </div>
                                <div class="form-group">
                                    <label  class="registerlabel" class="form-control md-textarea" for="Massage"></label>
                                    <textarea class="inputRegieter" placeholder="Your Message Here."  name="Massage" id="Massage"></textarea> 
                                </div>
                                
                                <div class="form-group">
                                    <input type="checkbox" name="remember-me" id="remember-me" class="agree-term" />
                                    <label class="registerlabel" class="inputRegieter" for="remember-me" class="label-agree-term"><span><span></span></span>Remember me</label>
                                </div>
                                <div class="form-group form-button">
                                    <input type="submit" name="send" id="signin" value="Send" class="btn btn-secondary form-submit"/>
                                </div>

                        </form>

                    </div>
                    <div class="signin-image">
                        <figure><img src="{{ asset ('images/frontend_images/login/email.png')}}" alt="sing up image"></figure>
                    </div>
                </div>
            </div>
            
        </section>
</div>
    
@endsection