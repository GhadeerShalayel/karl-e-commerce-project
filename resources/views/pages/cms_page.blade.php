@extends('layouts.frontlayouts.front_design')
@section('content')


        <!-- ****** New Arrivals Area Start ****** -->
        <section class="new_arrivals_area section_padding_100_0 clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="section_heading text-center">
                            <h2>{{$cmsPageDetails->title}}</h2>
                        </div>
                    </div>
                </div>
            </div>

            <div class="karl-projects-menu mb-100">
                <div class="text-center portfolio-menu">
                    <p style="text-align: center;">{{$cmsPageDetails->description}}</p>

                </div>
            </div>
          
    <!-- /.wrapper end -->

    
@endsection