<?php
use App\Http\Controllers\Controller;
use App\Http\Controllers\ProductsController;
$mainCategories = Controller::mainCategories();
$category = Controller::category();
$coupons = Controller::viewcopoun();
$total_bag = ProductsController::Bag();
$quentity = ProductsController::Quentity();


use Illuminate\Support\Facades\Auth;

?>

<div class="catagories-side-menu">
        <!-- Close Icon -->
        <div id="sideMenuClose">
            <i class="ti-close"></i>
        </div>
        <!--  Side Nav  -->
        <div class="nav-side-menu">
            <div class="menu-list">
                <h6>Categories</h6>
                <ul id="menu-content" class="menu-content collapse out">
                    @foreach($category as $cat)
                    @if($cat->status == "1")
                    <!-- Single Item -->
                    <li data-toggle="collapse" data-target="#{{$cat->id}}" class="collapsed">
                        <a href="#">{{$cat->name}}<span class="arrow"></span></a>
                        <ul class="sub-menu collapse" id="{{$cat->id}}">
                            @foreach($cat->category as $sub)
                            @if($sub->status == "1")
                            <li><a href="{{url('products/'.$sub->url)}}">{{ $sub ->name}}</a></li>
                            @endif
                            @endforeach
                        </ul>
                    </li>
                    @endif
                    @endforeach
                
                </ul>
            </div>
        </div>
    </div>

    <div id="wrapper">

        <!-- ****** Header Area Start ****** -->
        <header class="header_area bg-img background-overlay-white backgroundImage"  style="background-image: url(images/frontend_images/bg-img/bg-1.jpg);" > 

            <!-- Top Header Area Start -->
            <div class="top_header_area">
         
                <div class="container h-100">
                    <div class="row h-100 align-items-center justify-content-end">
                        <div class="col-12 col-lg-7">
                            <div class="top_single_area d-flex align-items-center">
                                <!-- Logo Area -->
                                <div class="top_logo">
                                    <a href="{{url('./')}}"><img src="{{ asset ('images/frontend_images/core-img/logo.png')}}" alt=""></a>
                                </div>
                              
                                <!-- Cart & Menu Area -->
                                <div class="header-cart-menu d-flex align-items-center ml-auto">
                                    <!-- Cart Area -->
                                    <div class="cart">
                                        <a href="{{url('/cart')}}" id="header-cart-btn" target="_blank"><span class="cart_quantity">{{$quentity}}</span><i class="ti-bag"></i> Your Bag ${{$total_bag}}</a>
                                        <!-- Cart List Area Start -->
                                       
                                    </div>
                                    <div class="header-right-side-menu ml-15">
                                        <a href="#" id="sideMenuBtn"><i class="ti-menu" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <!-- Top Header Area End -->
            <div class="main_header_area">
          
                <div class="container h-100">
                    <div class="row h-100">
                        <div class="col-12 d-md-flex justify-content-between" >
                     
                            <!-- Header Social Area -->
                            <div class="header-social-area">

                                <div class="a2a_kit a2a_kit_size_32 ">
                                    <a style="color:black;" class="a2a_button_facebook"><span class="karl-level" style="bottom: auto;padding-bottom: 0px;height: 27px;padding-top: 0px;">Share</span><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    <a style="color:black;" class="a2a_button_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                    <a  style="color:black;"  class="a2a_button_email"><i class="fa fa-envelope"aria-hidden="true"></i></a>
                                </div>
                                <script async src="https://static.addtoany.com/menu/page.js"></script>
                                <!-- AddToAny END -->
                            </div>
                          
                            <!-- Menu Area -->
                            <div class="main-menu-area">
                                <nav class="navbar navbar-expand-lg align-items-start">

                                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#karl-navbar" aria-controls="karl-navbar" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"><i class="fa fa-bars"></i></i></span></button>

                                    <div class="collapse navbar-collapse align-items-start collapse" id="karl-navbar">
                                        <ul class="navbar-nav animated" id="nav">
                                            <li class="nav-item active nav_padding"><a style="color:black;" class="nav-link" href="{{ url ('/')}}">Home</a></li>
                                            <li class="nav-item dropdown nav_padding">
                                                <a class="nav-link dropdown-toggle" style="color:black;" href="#" id="karlDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pages</a>
                                                <div class="dropdown-menu" aria-labelledby="karlDropdown">
                                                    @if(empty(Auth::check()))
                                                    <a class="dropdown-item"  href="{{ url ('/cart')}}">Cart</a>
                                                    <a class="dropdown-item"  href="{{ url ('/orders')}}">Orders</a>
                                                    @else
                                                    <a class="dropdown-item"  href="{{ url ('/wish-list')}}">Wish List <i class="fa fa-heart"></i></a>
                                                    <a class="dropdown-item"  href="{{ url ('/cart')}}">Cart</a>
                                                    <a class="dropdown-item"  href="{{ url ('/orders')}}">Orders</a>
                                                    @endif
                                                </div>
                                            </li>

                                            <li class="nav-item dropdown nav_padding">
                                                <a style="color:black;" class="nav-link dropdown-toggle" href="#" id="karlDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">shop</a>
                                                <div class="dropdown-menu  menu-image" aria-labelledby="karlDropdown">
                                                     @foreach($mainCategories as $cat)
                                                     @if($cat->status == "1")
                                                     <a class="dropdown-item" style="color:black;" href="{{ asset('/products/'.$cat->url)}}"><img class="img-top" src="{{ asset ('images/frontend_images/top_category/'.$cat->image)}}" alt="">{{$cat->name}}</a>
                                                     @endif
                                                     @endforeach

                                                </div>
                                            </li>
                                            <!--We will use Auth::check() to check whether the user has logged in or not.
                                             If Auth::check() is empty, it means the user has not logged in.-->
                                            @if(empty(Auth::check()))
                                                <li class="nav-item nav_padding"><a style="color:black;" class="nav-link" href="{{url('/user-login')}}">Login</a></li>
                                                <li class="nav-item nav_padding"><a style="color:black;" class="nav-link" href="{{url('/user-register')}}">Creat new Account</a></li>
                                            @else
                                                <li class="nav-item nav_padding"><a style="color:black;" class="nav-link" href="{{ url ('/account')}}">Account</a></li>
                                                <li class="nav-item nav_padding"><a style="color:black;" class="nav-link" href="{{ url ('/user-logout')}}">Logout</a></li>
                                            @endif
                                        </ul>
                                    </div>
                                </nav>
                            </div>
                            <!-- Help Line -->
                            <div class="help-line" >
                                <form  class="form-inline my-2 my-lg-0" action="{{url('products/search-products')}}" method="post">{{ csrf_field() }}
                                    <div class="input-group mb-3">
                                            <input type="text" class="form-control" placeholder="Search Product" name="product" aria-label="Search Product" aria-describedby="basic-addon2">
                                            <div class="input-group-append">
                                                <button class="btn btn-outline-danger" type="submit" style=" :hover {background: yellow}"><i class="ti-search"></i></button>
                                            </div>
                                    </div>
                                    
                                </form>
                            <a href="https:/wa.me/346573556778"><span><i class="fa fa-whatsapp fa-lg" aria-hidden="true" style="margin-right: 5px;"></i><span>+34 657 3556 778</a> <br>
        
                            </div>
                           
                        </div>
                     
                    </div>
                </div>
            </div>
        </header>
        <!-- ****** Header Area End ****** -->

         <!-- ****** Top Discount Area Start ****** -->
         <section class="top-discount-area d-md-flex align-items-center">
            <!-- Single Discount Area -->
     
            @foreach($coupons as $coup)
            <!-- Single Discount Area -->
            <div class="single-discount-area">
                <h5>@if($coup->amount_type=="Percentage") % @else $ @endif {{$coup->amount}} {{$coup->description}}</h5>
                <h6>USE CODE:{{$coup->coupon_code}}</h6>
            </div>
            <!-- Single Discount Area -->
            @endforeach
        </section>
        <!-- ****** Top Discount Area End ****** -->