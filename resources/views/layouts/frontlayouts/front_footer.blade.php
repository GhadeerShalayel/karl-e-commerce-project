   <!-- ****** Footer Area Start ****** -->
   <footer class="footer_area">
            <div class="container">
                <div class="row">
               
                    <!-- Single Footer Area Start -->
                    <div class="col-12 col-sm-6 col-md-3 col-lg-2">
                        <div class="single_footer_area">
                            <ul class="footer_widget_menu">
                                <li><a href="{{url('/page/about_us')}}">About</a></li>
                                <li><a href="{{url('/page/faq')}}">Faq</a></li>
                                <li><a href="{{url('/page/returns')}}">Returns</a></li>
                                <li><a href="{{url('/page/contact')}}">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- Single Footer Area Start -->
                    <div class="col-12 col-sm-6 col-md-3 col-lg-2">
                        <div class="single_footer_area">
                            <ul class="footer_widget_menu">
                                <li><a href="{{url('/account')}}">My Account</a></li>
                                <li><a href="{{url('/page/shipping')}}">Shipping</a></li>	
                                <li><a href="{{url('/page/our-policies')}}">Our Policies</a></li>
                                <li><a href="{{url('/page/afiliates')}}">Afiliates</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- Single Footer Area Start -->
                    <div class="col-12 col-lg-5">
                        <div class="single_footer_area">
                            <div class="footer_heading mb-30">
                                <h6>Subscribe to our newsletter</h6>
                            </div>
                            <div class="subscribtion_form">
                                <form action="javascript:void(0);" type="post" >{{csrf_field()}}
                                    <input onfocus="enableSubscribe();" onfocusout ="CheckSubscriber();" type="email" id="email" name="email" class="mail" placeholder="Your email here" required=""/>
                                    <button onclick ="CheckSubscriber(); AddSubscriber();" id="btnSubmit" type="submit" class="submit">Subscribe</button>
                                    <span id = "StatusSub" name = "StatusSub"> </span>
                                </form>
                            </div>
                        </div>
                    </div>

                         <!-- Single Footer Area Start -->
                     <div class="col-12 col-md-6 col-lg-3">
                        <div class="single_footer_area">
                            <div class="footer-logo" style="    margin-top: 50px;">
                                <img src="{{ asset ('images/frontend_images/core-img/logo.png')}}" alt="">
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="line"></div>

                <!-- Footer Bottom Area Start -->
                <div class="footer_bottom_area">
                    <div class="row">
                        <div class="col-12">
                            <div class="footer_social_area text-center">
                                <a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- ****** Footer Area End ****** -->
    </div>

<script>
        function CheckSubscriber(){
		var email = $('#email').val();
		$.ajax({
			type:'post',
			url:'/check-subscriber-email',
			data:{email:email},
			success:function(resp){
                //alert(resp);
                if(resp == true){
                    $("#StatusSub").show();
                    $("#btnSubmit").hide();
					$("#StatusSub").html("<font color='#ff084e'>This email is already exist </font>");
                }
			},error:function(){
				alert("Error");
			}
		});
    }

    function enableSubscribe(){
        $("#btnSubmit").show();
        $("#StatusSub").hide();

    }

    function AddSubscriber(){
        var email = $('#email').val();
		$.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
			type:'post',
			url:'/add-subscriber-email',
			data:{email:email},
			success:function(resp){
                //alert(resp);
                if(resp == true){
                    $("#StatusSub").show();
                    $("#btnSubmit").hide();
					$("#StatusSub").html("<font color='#red'>This email is already exist </font>");
                }else if(resp==false){
                    $("#StatusSub").show();
					$("#StatusSub").html("<font color='#ff084e'>Thanks! for subsecribing </font>");
                }
			},error:function(){
				alert("Error");
			}
		});
    }
</script>