<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title> @if(!empty($meta_title)) {{$meta_title}} @else Karl - Fashion @endif </title>
    @if(!empty($meta_description))<meta name="description" content="{{$meta_description}}">@endif
    @if(!empty($meta_keywords))<meta name="keywords" content="{{$meta_keywords}}">@endif
    <!-- Favicon  -->
    <link rel="icon" href="{{ asset ('images/frontend_images/core-img/favicon.ico')}}">
    
    <!-- Core Style CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset ('css/frontend_css/style404.css')}}">
    <link rel="stylesheet" href="{{ asset ('css/frontend_css/animate.css')}}">
    <link rel="stylesheet" href="{{ asset ('css/frontend_css/easyzoom.css')}}" />
    <link rel="stylesheet" href="{{ asset ('css/frontend_css/core-style.css')}}">
    <link rel="stylesheet" href="{{ asset ('css/frontend_css/jquery-ui.min.css')}}">
    <link rel="stylesheet" href="{{ asset ('css/frontend_css/nouislider.css')}}">
    <link rel="stylesheet" href="{{ asset ('css/frontend_css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{ asset ('css/frontend_css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{ asset ('css/frontend_css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset ('css/frontend_css/style-register.css')}}">
    <link rel="stylesheet" href="{{ asset ('css/frontend_css/style.css')}}">
    <link rel="stylesheet" href="{{ asset ('css/frontend_css/passtrength.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">

    <!-- Responsive CSS -->
    <link href="{{ asset ('css/frontend_css/responsive.css')}}" rel="stylesheet">

</head>

<body>
@include('layouts.frontlayouts.front_header')

@yield('content')

@include('layouts.frontlayouts.front_footer')


    <!-- jQuery (Necessary for All JavaScript Plugins) -->
   

    <!--<script src="{{ asset ('js/frontend_js/jquery/jquery_register.min.js')}}"></script>
    <script src="{{ asset ('js/frontend_js/main_register.js')}}"></script>-->


    <script src="{{ asset ('js/frontend_js/jquery/jquery-2.2.4.min.js')}}"></script> <!--مشكلة الزر الي بطلع فوق لاول الصفحةانو مستخدمة 2 جيكويري زبطيها وشيلي ام 2|.2 -->
    <!-- Popper js -->
    <!-- Bootstrap js -->
    <!-- Plugins js -->
    <script src="{{ asset ('js/frontend_js/plugins.js')}}"></script>
    <!-- Active js -->
    <script src="{{ asset ('js/frontend_js/active.js')}}"></script>

    <!-- Easy zoom js -->
    <script src="{{ asset ('js/frontend_js/easyzoom.js')}}"></script>
    <script src="{{ asset ('js/frontend_js/easyzoom1.js')}}"></script>
    <!-- main scripy in my e-commerce project -->
    <script src="{{ asset ('js/frontend_js/main.js')}}"></script>
    <!-- to validate input js -->
    <script src="{{ asset ('js/frontend_js/jquery.validate.js') }}"></script> 
    <!-- Password Strength Indicator Plugin For jQuery - Passtrength.js -->
    <script src="{{ asset ('js/frontend_js/passtrength.js')}}"></script>

    <script src=" https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
           <script>
                $(function () {
            $('[data-toggle="tooltip"]').tooltip()
            })
        </script>
        

</body>

</html>
