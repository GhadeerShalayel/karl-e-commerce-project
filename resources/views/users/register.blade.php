@extends('layouts.frontlayouts.front_design')
@section('content')

    <div class="main">

        <!-- Sign up form -->
        <section class="signup" id ="signup">
            @if(Session::has('flash_message_error'))    
              
              <div class="alert alert-danger alert-block alert_message ">
                 <button type="button" class="close" data-dismiss="alert">×</button>	
                   <strong> {!! session('flash_message_error') !!} </strong>
                </div>
                @endif

                @if(Session::has('flash_message_success'))  
                    <div class="alert alert-success alert-block alert_message">
                        <button type="button" class="close" data-dismiss="alert">×</button>	
                        <strong> {!! session('flash_message_success') !!} </strong>
                        </div>
            @endif
            <div class="container-register">
                <div class="signup-content">
                    <div class="signup-form" >
                        <h2 class="form-title">Sign up</h2>
                        <form method="post" id="registerForm" name="registerForm"  action ="{{url('/user-register')}}"> {{csrf_field()}}
                            <div class="form-group">
                                <label class="registerlabel" for="name" ><i class="ti-user"></i></label>
                                <input class="inputRegieter" type="text" name="name" id="name" placeholder="Your Name"/>
                            </div>
                            <div class="form-group">
                                <label class="registerlabel"  for="email"><i class="ti-email"></i></label>
                                <input class="inputRegieter" type="email" name="email" id="email" placeholder="Your Email"/>
                            </div>
                            <div class="form-group">
                                <label class="registerlabel" for="pass"><i class="ti-lock"></i></label>
                                <input class="inputRegieter " type="password" name="pass" id="myPassword" placeholder="Password"/>
                            </div>
                            <div class="form-group">
                                <label  class="registerlabel" for="re-pass"><i class="ti-pin-alt"></i></label>
                                <input class="inputRegieter" type="password" name="re_pass" id="re_pass" placeholder="Repeat your password"/>
                            </div>

                            <div class="form-group form-button">
                            <input type="submit" name="signup" id="signup" value="Register" class="btn btn-secondary form-submit"/>
                            </div>
                        </form>
                    </div>
                    <div class="signup-image">
                        <figure><img src="{{ asset ('images/frontend_images/login/log1.png')}}" alt="sing up image"></figure>
                        <a href="{{url('/user-login')}}"  style="text-decoration: none;" class="signup-image-link">I am already member ?</a>
                    </div>
                </div>
            </div>
        </section>
      

    </div>

	@endsection