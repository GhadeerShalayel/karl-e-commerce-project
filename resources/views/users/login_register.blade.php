@extends('layouts.frontlayouts.front_design')
@section('content')


<div class="main">

<!-- Sign up form -->
            @if(Session::has('flash_error'))    
              
              <div class="alert alert-danger alert-block alert_message" >
                 <button type="button" class="close" data-dismiss="alert">×</button>	
                   <strong> {!! session('flash_error') !!} </strong>
                </div>
                @endif

                @if(Session::has('flash_message_success'))  
                    <div class="alert alert-success alert-block alert_message">
                        <button type="button" class="close" data-dismiss="alert">×</button>	
                        <strong> {!! session('flash_message_success') !!} </strong>
                        </div>
             @endif
        <!-- Sing in  Form -->
        <section class="sign-in" id ="signin">
            <div class="container-register">
                <div class="signin-content" >
                    <div class="signin-image">
                        <figure><img src="{{ asset ('images/frontend_images/login/log2.png')}}" alt="sing up image"></figure>
                        <a href="{{url('/user-register')}}"  style="text-decoration: none;" class="signup-image-link">Create an account ?</a>
                    </div>
                    <div class="signin-form">
                        <h2 class="form-title">Sign In</h2>
                        <form method="post" action ="{{url('/user-login')}}" class="login-form" id="login-form">{{csrf_field()}}
                        <div class="form-group">
                                <label class="registerlabel" for="email"><i class="ti-email"></i></label>
                                <input class="inputRegieter" type="email" name="email" id="email" placeholder="Your Email"/>
                            </div>
                            <div class="form-group">
                                <label  class="registerlabel" for="your_pass"><i class="ti-lock"></i></label>
                                <input  class="inputRegieter" type="password" name="your_pass" id="your_pass" placeholder="Password"/>
                            </div>
  
                            <div class="form-group form-button">
                                <input type="submit" name="signin" id="signin" value="Log in" class="btn btn-secondary form-submit"/>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
            
        </section>
</div>

@endsection