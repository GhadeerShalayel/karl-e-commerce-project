@extends('layouts.frontlayouts.front_design')
@section('content')

    <div class="main">

        <!-- Sign up form -->
        <section class="signup" id ="signup">
        @if(Session::has('flash_message_error'))    
              
              <div class="alert alert-danger alert-block alert_message ">
                 <button type="button" class="close" data-dismiss="alert">×</button>	
                   <strong> {!! session('flash_message_error') !!} </strong>
                </div>
                @endif

                @if(Session::has('flash_message_success'))  
                    <div class="alert alert-success alert-block alert_message">
                        <button type="button" class="close" data-dismiss="alert">×</button>	
                        <strong> {!! session('flash_message_success') !!} </strong>
                        </div>
                @endif
            <div class="container-register">
                <div class="signup-content">
                    <div class="signup-form" >
                        <h2 class="form-title">Update Account</h2>
                        <form method="post" id="UpdateForm" name="UpdateForm"  action ="{{url('/account')}}"> {{csrf_field()}}
                            <div class="form-group">
                                <label class="registerlabel" for="name" ><i class="ti-user"></i></label>
                                <input class="inputRegieter" type="text" name="name" id="name" value = "{{$userDetailes->name}}" placeholder="Your Name"/>
                            </div>
                            <div class="form-group">
                                <label  class="registerlabel"  for="name" ><i class="ti-map-alt"></i></label>
                                <input class="inputRegieter" type="text" name="address" id="address" value = "{{$userDetailes->address}}" placeholder="Your Address"/>
                            </div>
                            <div class="form-group">
                                <label  class="registerlabel"    class="registerlabel"  for="name" ><i class="ti-location-pin"></i></label>
                                <input class="inputRegieter " type="text" name="city" id="city" value = "{{$userDetailes->city}}" placeholder="Your City"/>
                            </div>
                            <div class="form-group">
                                <label  class="registerlabel"  for="name" ><i class="ti-home"></i></label>
                                <input class="inputRegieter" type="text" name="state" id="state" value = "{{$userDetailes->state}}"  placeholder="Your State"/>
                            </div>
                            <div class="form-group ">
                                <label  class="registerlabel"  for="inputState" style="height: 19px;width: 10px;margin-left: 125px;"><i class="ti-world"></i></label>
                                <select id="country"  name ="country"  class="form-control">
                                    <option  value="" >Select Country  </option>
                                    @foreach($Countries as $country)
                                    <option value = "{{ $country->country_name }}" @if($country->country_name == $userDetailes->country ) selected @endif > {{$country->country_name}} </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label   class="registerlabel" for="name" ><i class="ti-pin2"></i></label>
                                <input class="inputRegieter" type="text" name="pincode" id="pincode" value = "{{$userDetailes->pincode}}" placeholder="Your Pincode"/>
                            </div>
                            <div class="form-group">
                                <label  class="registerlabel" for="name" ><i class="ti-mobile"></i></label>
                                <input class="inputRegieter" type="text" name="mobile" id="mobile" value = "{{$userDetailes->mobile}}" placeholder="Your Mobile"/>
                            </div>
                            <div class="form-group">
                                <label class="registerlabel"  for="name" ><i class="ti-email"></i></label>
                                <input class="inputRegieter" type="email" name="email" id="email" value = "{{$userDetailes->email}}" placeholder="Your Email"/>
                            </div>
                          

                            <div class="form-group form-button">
                            <input type="submit" name="signup" id="signup" value="Update" class="btn btn-secondary form-submit"/>
                            </div>
                        </form>
                    </div>
                    <div class="signup-image">
                        <figure><img src="{{ asset ('images/frontend_images/login/log1.png')}}" alt="sing up image"></figure>
                    </div>
                </div>
            </div>
        </section>

        @if(Session::has('flash_error'))    
              
              <div class="alert alert-danger alert-block alert_message" >
                 <button type="button" class="close" data-dismiss="alert">×</button>	
                   <strong> {!! session('flash_error') !!} </strong>
                </div>
                @endif

                @if(Session::has('flash_success'))  
                    <div class="alert alert-success alert-block alert_message">
                        <button type="button" class="close" data-dismiss="alert">×</button>	
                        <strong> {!! session('flash_success') !!} </strong>
                        </div>
                @endif
        <!-- Sing in  Form -->
        <section class="sign-in" id ="signin">
            <div class="container-register">
                <div class="signin-content" >
                    <div class="signin-image">
                        <figure><img src="{{ asset ('images/frontend_images/login/log2.png')}}" alt="sing up image"></figure>
                    </div>
                    <div class="signin-form">
                        <h2 class="form-title">Update Password</h2>
                        <form method="post" action ="{{ url('/update-pwd')}}" class="login-form" name="password_validate" id="password_validate">{{csrf_field()}}
                        <div class="form-group">
                                <label  class="registerlabel" for="your_pass"><i class="ti-lock"></i></label>
                                <input class="inputRegieter" type="password" name="current_pwd" id="current_pwd" placeholder="Current Password"/>
                                <span id="chkPwd"></span>

                            </div>
                            <div class="form-group">
                                <label  class="registerlabel" for="your_pass"><i class="ti-star"></i></label>
                                <input  class="inputRegieter" type="password" name="new_pwd" id="new_pwd" placeholder="New Password"/>
                            </div>
                            <div class="form-group">
                                <label  class="registerlabel" for="your_pass"><i class="ti-reload"></i></label>
                                <input  class="inputRegieter" type="password" name="confirm_pwd" id="confirm_pwd" placeholder="Confirm Password"/>
                            </div>
                
                            <div class="form-group form-button">
                                <input type="submit" name="Update" id="Update" value="Update" class="btn btn-secondary form-submit"/>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
            
        </section>

    </div>
     
	@endsection