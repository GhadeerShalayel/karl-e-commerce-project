@extends('layouts.frontlayouts.front_design')
@section('content')

 <div class="form-gap"></div>

<div class="container">
@if(Session::has('flash_error'))    
              
              <div class="alert alert-danger alert-block alert_message " style="margin-top: 30px;" >
                 <button type="button" class="close" data-dismiss="alert">×</button>	
                   <strong> {!! session('flash_error') !!} </strong>
                </div>
                @endif

                @if(Session::has('flash_message_success'))  
                    <div class="alert alert-success alert-block alert_message" style="margin-top: 30px;">
                        <button type="button" class="close" data-dismiss="alert">×</button>	
                        <strong> {!! session('flash_message_success') !!} </strong>
                        </div>
             @endif
	<div class="row">
		<div class="col-md-4 col-md-offset-4"  style="margin: 0 auto;">
            <div class="panel panel-default" style ="margin-top: 80px; margin-bottom: 80px;">
              <div class="panel-body">
                <div class="text-center">
                  <h3><i class="fa fa-lock fa-4x" style ="color:#f10e4f;"></i></h3>
                  <h2 class="text-center">Forgot Password?</h2>
                  <p>You can reset your password here.</p>
                  <div class="panel-body">
    
                    <form id="register-form" role="form" action="{{url('/forgot-password')}}" autocomplete="off" class="form" method="post">{{csrf_field()}}
    
                      <div class="form-group">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>
                          <input id="email" name="email" placeholder="email address" class="form-control"  type="email" required="">
                        </div>
                      </div>
                      <div class="form-group">
                        <input name="recover-submit" class="btn btn-lg btn-primary btn-block" value="Reset Password" type="submit" style="background-color: #f10e4f;"  >
                      </div>
                      
                      <input type="hidden" class="hide" name="token" id="token" value=""> 
                    </form>
    
                  </div>
                </div>
              </div>
            </div>
          </div>
	</div>
</div>

@endsection('content')
