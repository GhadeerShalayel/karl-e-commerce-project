@extends('layouts.frontlayouts.front_design')
@section('content')

<div class="main">

        <!-- Thanks form -->
        <section class="signup" id ="signup">
            @if(Session::has('flash_message_error'))    
              
              <div class="alert alert-danger alert-block alert_message ">
                 <button type="button" class="close" data-dismiss="alert">×</button>	
                   <strong> {!! session('flash_message_error') !!} </strong>
                </div>
                @endif

                @if(Session::has('flash_message_success'))  
                    <div class="alert alert-success alert-block alert_message">
                        <button type="button" class="close" data-dismiss="alert">×</button>	
                        <strong> {!! session('flash_message_success') !!} </strong>
                        </div>
            @endif
            <div class="container-register">
                <div class="signup-content">
                    <div class="signup-form" >
                        <h2 class="form-title" style="color:#ff084e; text-align: center;">Thank You</h2>
                        <h3 style="text-align: center; "> Your PayPal Order Has Been Placed </h3>
                        <p  style="text-align: center; ">Thanks for the payment. We will process your order very Soon. </p>
                        <p style="text-align: center;text-align: center;margin-bottom: 0px;padding-top: 10px; "> Your order nubmer "{{Session::get('order_id')}}"</p>
                        <p style="text-align: center; ">total amount paid is $ {{Session::get('grand_total')}}</p>
                    </div>
                    <div class="signup-image">
                        <figure><img src="{{ asset ('images/frontend_images/login/log1.png')}}" alt="sing up image"></figure>
                    </div>
                </div>
            </div>
        </section>
</div>

@endsection

<?php 
Session::forget('grand_total');
Session::forget('order_id'); ?>
