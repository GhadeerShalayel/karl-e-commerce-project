@extends('layouts.frontlayouts.front_design')
@section('content')

<div class="main">

        <!-- Thanks form -->
        <section class="signup" id ="signup">
            
            <div class="container-register">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb" style="background-color :white;">
                    <li class="breadcrumb-item "><a style="text-decoration: none;" href="{{url('/')}}">Home</a></li>
                    <li class="breadcrumb-item "><a style="text-decoration: none;" href="{{url('/orders')}}">Orders</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$orderDetails->id}}</li>
                    </ol>
                </nav>
                <h4 style=" padding-top: 26px; padding-left: 20px;   text-align: center;" > User Order Details </h4>

                <div class="signup-content">
                    <table id="example" class="table table-striped table-bordered" style="width:100% " >
                        <thead>
                            <tr>
                                <th>Product Code</th>
                                <th>Product Name</th>
                                <th>Product Size</th>
                                <th>Product Color</th>
                                <th>Product Price</th>
                                <th>Product Qty</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($orderDetails->orders as $pro)
                                <tr>
                                    <td>{{$pro->product_code}}</td>
                                    <td>{{$pro->product_name}}</td>
                                    <td>{{$pro->product_size}}</td>
                                    <td>{{$pro->product_color}}</td>
                                    <td>{{$pro->product_price}}</td>
                                    <td>{{$pro->product_qty}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
</div>

@endsection