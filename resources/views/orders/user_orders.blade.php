@extends('layouts.frontlayouts.front_design')
@section('content')

<div class="main">

        <!-- Thanks form -->
        <section class="signup" id ="signup">
    
            <div class="container-register">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb" style="background-color :white;">
                        <li class="breadcrumb-item "><a style="text-decoration: none;" href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Orders</li>
                    </ol>
                </nav>
                <h4 style=" padding-top: 26px; padding-left: 20px;   text-align: center;" > User Order Details </h4>
                <div class="signup-content">
                    <div class="table-responsive"> 
                    <table id="example" class="table table-striped table-bordered" style="width:100% " >
                        <thead>
                            <tr>
                                <th>Order ID</th>
                                <th>Order Products</th>
                                <th>Payment Method</th>
                                <th>Grand Total</th>
                                <th>Created On</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($orders as $order)
                                <tr>
                                    <td>{{ $order -> id }}</td>

                                    <td>
                                        @foreach($order->orders as $pro)
                                          <a href="{{url('/orders/'.$order->id)}}" style = "color : #ff084e;  text-decoration: none;" >
                                           {{$pro->product_code}}
                                           ({{$pro->product_qty}})

                                           </a> <br>
                                        @endforeach
                                    </td>
                                    <td>{{ $order -> payment_method }}</td>
                                    <td>${{ $order -> grand_total }}</td>
                                    <td>{{ $order -> created_at }}</td>
                                    <td> <a href="{{url('/orders/'.$order->id)}}" style = "color : #ff084e;  text-decoration: none;" >View Details</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </section>
</div>

@endsection