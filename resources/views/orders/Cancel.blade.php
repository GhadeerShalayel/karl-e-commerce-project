@extends('layouts.frontlayouts.front_design')
@section('content')

<div class="main">

        <!-- Thanks form -->
        <section class="signup" id ="signup">
            @if(Session::has('flash_message_error'))    
              
              <div class="alert alert-danger alert-block alert_message ">
                 <button type="button" class="close" data-dismiss="alert">×</button>	
                   <strong> {!! session('flash_message_error') !!} </strong>
                </div>
                @endif

                @if(Session::has('flash_message_success'))  
                    <div class="alert alert-success alert-block alert_message">
                        <button type="button" class="close" data-dismiss="alert">×</button>	
                        <strong> {!! session('flash_message_success') !!} </strong>
                        </div>
            @endif
            <div class="container-register">
                <div class="signup-content">
                    <div class="signup-form" >
                        <h3 style="text-align: center; "> Your PayPal Order Has Been Canceld </h3>
                        <p style="text-align: center; ">Please contact us if there is any enquiry. </p>
                     
                    </div>
                    <div class="signup-image">
                        <figure><img src="{{ asset ('images/frontend_images/login/log1.png')}}" alt="sing up image"></figure>
                    </div>
                </div>
            </div>
        </section>
</div>

@endsection

<?php 
Session::forget('grand_total');
Session::forget('order_id'); ?>
