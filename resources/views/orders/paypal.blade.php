<?php
 use App\Order;
  ?>

@extends('layouts.frontlayouts.front_design')
@section('content')

<div class="main">

        <!-- Thanks form -->
        <section class="signup" id ="signup">
            @if(Session::has('flash_message_error'))    
              
              <div class="alert alert-danger alert-block alert_message ">
                 <button type="button" class="close" data-dismiss="alert">×</button>	
                   <strong> {!! session('flash_message_error') !!} </strong>
                </div>
                @endif

                @if(Session::has('flash_message_success'))  
                    <div class="alert alert-success alert-block alert_message">
                        <button type="button" class="close" data-dismiss="alert">×</button>	
                        <strong> {!! session('flash_message_success') !!} </strong>
                        </div>
            @endif
            <div class="container-register">
                <div class="signup-content">
                    <div class="signup-form" >
                        <h2 class="form-title" style="color:#ff084e; text-align: center;">Thank You</h2>
                        <h3 style="text-align: center; "> Your Order Has Been Placed </h3>
                        <p style="text-align: center;text-align: center;margin-bottom: 0px;padding-top: 10px; "> Your order nubmer "{{Session::get('order_id')}}"</p>
                        <p style="text-align: center; ">total payable about is $ {{Session::get('grand_total')}}</p>
                        <p style="text-align: center; ">Please make payment by clicking on below Payment Button </p>
                       <?php 
                        $ordersDetails = Order::gerOrderDetails(Session::get('order_id')); 
                        $ordersDetails = json_decode(json_encode($ordersDetails ));
                        //echo"<pre>" ; print_r($ordersDetails) ; die;
                        $nameArr = explode(' ',$ordersDetails->name);
                        //echo "<pre>" ; print_r($nameArr) ; die;
                        $getCountryCode = Order::getCountryCode($ordersDetails->country);
                        ?>
                        <!--
                            طريقة الدفع في بي بال عملناها عن طريق sandbox بالبيرسونال اكونت وبزنس اكونت الموجودين فيه 
                                الايميل الرئيسي 
                                paypalghad@gmail.com
                                Ghadeerfares12@@@
                                وع الموقع الايميل 
                                sb-yqwk6457637@personal.example.com
                                والسر ghadeer1234

                                ولكن لازم نخلي الurl يكون live  https://www.paypal.com/cgi-bin/webscr/
                                ف لما بدك تجربيه تاني رجعيه لساندوكس  https://www.sandbox.paypal.com/cgi-bin/webscr
                           

                            -->
                            <form style="text-align: center; " action="https://www.paypal.com/cgi-bin/webscr/" method="post">
                                <input type="hidden" name="cmd" value="_xclick">
                                <input type="hidden" name="business" value="sb-hiva3459522@business.example.com">
                                <input type="hidden" name="item_name" value="{{ Session::get('order_id') }}">
                                <input type="hidden" name="currency_code" value="USD">
                                <input type="hidden" name="amount" value="{{ Session::get('grand_total') }}">
                                <input type="hidden" name="first_name" value="{{ $nameArr[0] }}">
                                <input type="hidden" name="last_name" value="{{ $nameArr[1] }}">
                                <input type="hidden" name="address1" value="{{ $ordersDetails->address }}">
                                <input type="hidden" name="address2" value="">
                                <input type="hidden" name="city" value="{{ $ordersDetails->city }}">
                                <input type="hidden" name="state" value="{{ $ordersDetails->state }}">
                                <input type="hidden" name="zip" value="{{ $ordersDetails->pincode }}">
                                <input type="hidden" name="email" value="{{ $ordersDetails->user_email }}">
                                <input type="hidden" name="country" value="{{ $getCountryCode->country_code }}">
                                <input type="hidden" name="return" value="{{url('/paypal/thanks')}}">
                                <input type="hidden" name="cancel_return" value="{{url('/paypal/cancel')}}">

                                <input type="image"
                                    src="https://www.paypalobjects.com/webstatic/en_US/i/btn/png/btn_paynow_107x26.png" alt="Pay Now">
                                <img alt="" src="https://paypalobjects.com/en_US/i/scr/pixel.gif"
                                    width="1" height="1">
                            </form>
                        
                            
                    </div>
                    <div class="signup-image">
                        <figure><img src="{{ asset ('images/frontend_images/login/log1.png')}}" alt="sing up image"></figure>
                    </div>
                </div>
            </div>
        </section>
</div>

@endsection

<?php 
/*Session::forget('grand_total');
Session::forget('order_id');*/
 ?>
