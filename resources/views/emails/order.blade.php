<html>
    <head>
    <title>Orderd Placed</title>
    </head>
    <body>
        <table width='700px' >
            <tr>
                <td>  </td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td> <img src="{{ asset ('images/frontend_images/core-img/logo.png')}}" alt=""></td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td> Hello {{$name}} </td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td>Thank you for shopping with us . Your Order details are as below:</td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td> Order No : {{$order_id}} </td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td>
                    <table width='95%' cellpadding="5" cellspacing="5" bgcolor="#e0d9d9">
                        <tr bgcolor="#cccccc"> 
                            <td>Product Name  </td>
                            <td>Product Size </td>
                            <td>Product Code </td>
                            <td>Product Color </td>
                            <td>Product Quentity </td>
                            <td>Unit Price </td>
                        </tr>
                        @foreach($productDetaiels['orders'] as $product)
                        <tr> 
                            <td>{{$product['product_name'] }} </td>
                            <td>{{$product['product_size'] }} </td>
                            <td>{{$product['product_code'] }} </td>
                            <td>{{$product['product_color'] }} </td>
                            <td>{{$product['product_qty'] }} </td>
                            <td>{{$product['product_price'] }} </td>
                        </tr>
                        @endforeach
                        <tr>
                            <td colspan="5" align="right"> Shipping Sharges </td>
                            <td>${{$productDetaiels['shipping_charges']}} </td>
                        </tr>
                        <tr>
                            <td colspan="5" align="right"> Coupon Discount </td>
                            <td>${{$productDetaiels['coupon_amount']}} </td>
                        </tr>
                          <tr>
                            <td colspan="5" align="right">Grand Total</td>
                            <td>${{$productDetaiels['grand_total']}} </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td>
                    <table width='100%' >
                        <tr>
                            <td width="50%" >
                                <table >
                                    <tr>
                                        <td><strong>Bill To:</strong></td>
                                    </tr>
                                    <tr>
                                        <td>{{$userDetailes['name']}}</td>
                                    </tr>
                                    <tr>
                                        <td>{{$userDetailes['address']}}</td>
                                    </tr>
                                    <tr>
                                        <td>{{$userDetailes['city']}}</td>
                                    </tr>
                                    <tr>
                                        <td>{{$userDetailes['state']}}</td>
                                    </tr>
                                    <tr>
                                        <td>{{$userDetailes['country']}}</td>
                                    </tr>
                                    <tr>
                                        <td>{{$userDetailes['pincode']}}</td>
                                    </tr>
                                    <tr>
                                        <td>{{$userDetailes['mobile']}}</td>
                                    </tr>

                                </table>
                            </td>
                            <td width="50%" >
                                <table >
                                        <tr>
                                            <td><strong>Ship To:</strong></td>
                                        </tr>
                                        <tr>
                                            <td>{{$productDetaiels['name']}}</td>
                                        </tr>
                                        <tr>
                                            <td>{{$productDetaiels['address']}}</td>
                                        </tr>
                                        <tr>
                                            <td>{{$productDetaiels['city']}}</td>
                                        </tr>
                                        <tr>
                                            <td>{{$productDetaiels['state']}}</td>
                                        </tr>
                                        <tr>
                                            <td>{{$productDetaiels['country']}}</td>
                                        </tr>
                                        <tr>
                                            <td>{{$productDetaiels['pincode']}}</td>
                                        </tr>
                                        <tr>
                                            <td>{{$productDetaiels['mobile']}}</td>
                                        </tr>

                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                For any enquiries , you can contact us at <a href="mailto:info@ecom-website.com">info@ecom-website.com </a> 
                </td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td>Regards</td></tr>
            <tr><td>&nbsp;</td></tr>



        </table>
    </body>
</html>