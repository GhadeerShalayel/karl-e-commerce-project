<?php /* href="{{ url ('/admin/delete-product/'.$product->id)}}" */?> 

@extends('layouts.adminLayouts.admin_design')
@section('content')


<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Tables</a> </div>
    <h1>Products</h1>

    @if(Session::has('flash_message_error'))    
              
              <div class="alert alert-warning alert-block alert_message1">
                 <button type="button" class="close" data-dismiss="alert">×</button>	
                   <strong> {!! session('flash_message_error') !!} </strong>
                </div>
        @endif

        @if(Session::has('flash_message_success'))   
              <div class="alert alert-success alert-block alert_message1">
                 <button type="button" class="close" data-dismiss="alert">×</button>	
                   <strong> {!! session('flash_message_success') !!} </strong>
                </div>
        @endif

  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
       
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>View Banners</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Banner ID</th>
                  <th>Image</th>
                  <th>Banner Title</th>
                  <th>Banner description</th>
                  <th>Banner Link</th>
                  <th>Status</th>
                  <th>Action</th>

                </tr>
              </thead>

              <tbody>
                  @foreach($banners as $banner)
                <tr class="gradeX" >
                  <td style="text-align: center;" >{{$banner->id}}</td>
                  <td style="text-align: center;"> 
                  @if(!empty($banner->image))
                  <img style="width:100px;" src ="{{ asset('images/frontend_images/banners/'.$banner->image) }}" >
                  @endif
                  </td>
                  <td style="text-align: center;">{{$banner->title}}</td>
                  <td style="text-align: center;">{{$banner->description}}</td>
                  <td style="text-align: center;">{{$banner->link}}</td>
                  <td style="text-align: center;">
                  @if($banner->status==1) Active @else InActive @endif
                  </td>
                  

                  <td class="center" >
                  <a style="text-align: center;border-left-width: 0px;margin-right: 1px;" href="{{ url ('/admin/edit-banner/'.$banner->id)}}" class="btn btn-primary btn-mini" title="Edit Product">Edit</a>
               
               
                  <a rel="{{ $banner->id }}" rel1="delete-banner" style="text-align: center;margin-right: 20px;"
               href="javascript:" class="btn btn-danger btn-mini deleteRecord" title="Delete Product">Delete</a> </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
            
        
@endsection