@extends('layouts.adminLayouts.admin_design')
@section('content')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
     <a href="#">Currencies</a> <a href="#" class="current">Edit Shipping</a> </div>
    <h1>Currencies</h1>
                
    @if(Session::has('flash_message_error'))    
              
              <div class="alert alert-warning alert-block alert_message1">
                 <button type="button" class="close" data-dismiss="alert">×</button>	
                   <strong> {!! session('flash_message_error') !!} </strong>
                </div>
        @endif

        @if(Session::has('flash_message_success'))  
              <div class="alert alert-success alert-block alert_message1">
                 <button type="button" class="close" data-dismiss="alert">×</button>	
                   <strong> {!! session('flash_message_success') !!} </strong>
                </div>
        @endif

  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5>Edit Shipping</h5>

          </div>
          <div class="widget-content nopadding"> <!--when your form includes any <input type="file"> elements-->
            <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{ url('/admin/edit-shipping/'.$ShippingDetails->id) }}" name="edit_shipping" id="edit_shipping" novalidate="novalidate">{{ csrf_field()}}
          
            <input type ="hidden" name="id" value="{{$ShippingDetails->id}}">
            <div class="control-group">
                <label class="control-label">Country</label>
                <div class="controls">
                  <input type="text" readonly="" name="country" id="country" value="{{$ShippingDetails->country}}">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">shipping_charges0_500g</label>
                <div class="controls">
                  <input type="text" name="shipping_charges0_500g" id="shipping_charges0_500g" value="{{$ShippingDetails->shipping_charges0_500g}}">
                </div>
              </div>
              
              <div class="control-group">
                <label class="control-label">shipping_charges501_1000g</label>
                <div class="controls">
                  <input type="text" name="shipping_charges501_1000g" id="shipping_charges501_1000g" value="{{$ShippingDetails->shipping_charges501_1000g}}">
                </div>
              </div>
              
              <div class="control-group">
                <label class="control-label">shipping_charges1001_2000g</label>
                <div class="controls">
                  <input type="text" name="shipping_charges1001_2000g" id="shipping_charges1001_2000g" value="{{$ShippingDetails->shipping_charges1001_2000g}}">
                </div>
              </div>
              
              <div class="control-group">
                <label class="control-label">shipping_charges2001g_5000g</label>
                <div class="controls">
                  <input type="text" name="shipping_charges2001g_5000g" id="shipping_charges2001g_5000g" value="{{$ShippingDetails->shipping_charges2001g_5000g}}">
                </div>
              </div>

              <div class="form-actions">
                <input type="submit" value="Edit Shipping" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
 
  </div>
</div>


@endsection