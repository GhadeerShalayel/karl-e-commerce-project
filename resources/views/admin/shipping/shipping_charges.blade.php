
@extends('layouts.adminLayouts.admin_design')
@section('content')


<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Shipping sharges</a> <a href="#" class="current">View Orders</a> </div>
    <h1>Shipping sharges</h1>

    @if(Session::has('flash_message_error'))    
              
              <div class="alert alert-warning alert-block alert_message1">
                 <button type="button" class="close" data-dismiss="alert">×</button>	
                   <strong> {!! session('flash_message_error') !!} </strong>
                </div>
        @endif

        @if(Session::has('flash_message_success'))  
              <div class="alert alert-success alert-block alert_message1">
                 <button type="button" class="close" data-dismiss="alert">×</button>	
                   <strong> {!! session('flash_message_success') !!} </strong>
                </div>
        @endif

  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
       
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>View Shipping sharges</h5>
          </div>
         <div class="widget-content nopadding">
         <div class="table-responsive">
            <table class="table table-striped table-bordered data-table"  >
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Country</th>
                  <th>shipping_charges0_500g</th>
                  <th>shipping_charges501_1000g</th>
                  <th>shipping_charges1001_2000g</th>
                  <th>shipping_charges2001g_5000g </th>
                  <th>Updated at </th>
                  <th>Action</th>

                </tr>
              </thead>

              <tbody>
                  @foreach($ShippingSharges as $Shipping)
                <tr class="gradeX" >
                  <td style="text-align: center;" >{{$Shipping->id}}</td>
                  <td style="text-align: center;">{{$Shipping->country}}</td>
                  <td style="text-align: center;">{{$Shipping->shipping_charges0_500g}}</td>
                  <td style="text-align: center;">{{$Shipping->shipping_charges501_1000g}}</td>
                  <td style="text-align: center;">{{$Shipping->shipping_charges1001_2000g}}</td>
                  <td style="text-align: center;">{{$Shipping->shipping_charges2001g_5000g}}</td>
                  <td style="text-align: center;">{{$Shipping->updated_at}}</td>
                  <td class="center" >
                  <a style="text-align: center;border-left-width: 0px;margin-right: 1px;" href="{{ url ('/admin/edit-shipping/'.$Shipping->id)}}" class="btn btn-primary btn-mini" title="Edit Product">Edit</a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
            
@endsection