@extends('layouts.adminLayouts.admin_design')
@section('content')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Tables</a> </div>
    <h1>Categories</h1>

    @if(Session::has('flash_message_error'))    
              
              <div class="alert alert-warning alert-block alert_message1">
                 <button type="button" class="close" data-dismiss="alert">×</button>	
                   <strong> {!! session('flash_message_error') !!} </strong>
                </div>
        @endif

        @if(Session::has('flash_message_success'))  
              <div class="alert alert-success alert-block alert_message1">
                 <button type="button" class="close" data-dismiss="alert">×</button>	
                   <strong> {!! session('flash_message_success') !!} </strong>
                </div>
        @endif

  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
       
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>View Category</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Category ID</th>
                  <th>Category Name</th>
                  <th>Category Level</th>
                  <th>URL</th>
                  <th>Top Category</th>
                  <th> Meta Title</th>
                  <th> Meta Description</th>
                  <th> Meta Keywords</th>
                  <th>Image</th>
                  <th>Title</th>
                  <th>Status</th>
                  <th>Action</th>
                  
                </tr>
              </thead>

              <tbody>
                  @foreach($categories as $category)
                <tr class="gradeX" >
                  <td style="text-align: center;" >{{$category->id}}</td>
                  <td style="text-align: center;">{{$category->name}}</td>
                  <td style="text-align: center;">{{$category->parent_id}}</td>
                  <td style="text-align: center;">{{$category->url}}</td>
                  <td style="text-align: center;">@if($category->top_category == 1 ) Yes @else No @endif</td> 
                  <td style="text-align: center;" >{{$category->meta_title}}</td>
                  <td style="text-align: center;" >{{$category->meta_description}}</td>
                  <td style="text-align: center;" >{{$category->meta_keywords}}</td>
                  <td style="text-align: center;"> 
                  @if(!empty($category->image))
                  <img style="width:100px;" src ="{{ asset('images/frontend_images/top_category/'.$category->image) }}" >
                  @endif
                  </td>
                  <td style="text-align: center;">{{$category->title}}</td>
                  <td style="text-align: center;">@if($category->status == 1 ) Active @else Not Active @endif</td> 

                  <td ><div  class="fr"><a style="text-align: center;border-left-width: 0px;margin-right: 1px;" href="{{ url ('/admin/edit-category/'.$category->id)}}" class="btn btn-primary btn-mini">Edit</a>
                  <a rel="{{ $category->id }}" rel1="delete-category" style="text-align: center;margin-right: 20px;"
                   href="javascript:" class="btn btn-danger btn-mini deleteCategoryRecord">Delete</a></div> </td>

                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


@endsection