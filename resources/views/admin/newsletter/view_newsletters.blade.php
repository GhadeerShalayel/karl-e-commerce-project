
@extends('layouts.adminLayouts.admin_design')
@section('content')


<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Newsletter Subscribers</a> </div>
    <h1>Users</h1>

    @if(Session::has('flash_message_error'))    
              
              <div class="alert alert-warning alert-block alert_message1">
                 <button type="button" class="close" data-dismiss="alert">×</button>	
                   <strong> {!! session('flash_message_error') !!} </strong>
                </div>
        @endif

        @if(Session::has('flash_message_success'))   
              <div class="alert alert-success alert-block alert_message1">
                 <button type="button" class="close" data-dismiss="alert">×</button>	
                   <strong> {!! session('flash_message_success') !!} </strong>
                </div>
        @endif

  </div>
  <div class="container-fluid">
    <hr>
    <div>
        <a href="{{url('/admin/export-newsletter-emails')}}" class="btn btn-primary btn-mini">Export</a>
    </div>
    <div class="row-fluid">
      <div class="span12">
       
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>View Newsletter Subscribers</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Users ID</th>
                  <th> email</th>
                  <th> status</th>
                  <th>Subscribers on</th>
                  <th>Actions</th>

                </tr>
              </thead>

              <tbody>
                  @foreach($newsletterDetails as $user)
                <tr class="gradeX" >
                  <td style="text-align: center;" >{{$user->id}}</td>
                  <td style="text-align: center;" >{{$user->email}}</td>
                  <td style="text-align: center;">
                  @if($user->status==1)
                   <a href="{{url('/admin/update-newsletter-status/'.$user->id.'/0')}}"><span style ="color:green">Active</span></a>
                  @else
                   <a href="{{url('/admin/update-newsletter-status/'.$user->id.'/1')}}"> <span style ="color:red">InActive </span></a>
                  
                  @endif
                  </td> 
                  <td style="text-align: center;" >{{$user->created_at}}</td>
                  <td style="text-align: center;" ><a href="{{url('/admin/delete-newsletter-email/'.$user->id)}}">Delete</a></td>

                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
            
@endsection