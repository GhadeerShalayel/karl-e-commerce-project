
@extends('layouts.adminLayouts.admin_design')
@section('content')


<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Tables</a> </div>
    <h1>Users</h1>

    @if(Session::has('flash_message_error'))    
              
              <div class="alert alert-warning alert-block alert_message1">
                 <button type="button" class="close" data-dismiss="alert">×</button>	
                   <strong> {!! session('flash_message_error') !!} </strong>
                </div>
        @endif

        @if(Session::has('flash_message_success'))   
              <div class="alert alert-success alert-block alert_message1">
                 <button type="button" class="close" data-dismiss="alert">×</button>	
                   <strong> {!! session('flash_message_success') !!} </strong>
                </div>
        @endif

  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
       
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>View Currencies</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Currency ID</th>
                  <th> Currency Code</th>
                  <th> Exchange Rate </th>
                  <th> Status</th>
                  <th> Actions </th>

                </tr>
              </thead>

              <tbody>
                  @foreach($CurrencyDetails as $currency)
                <tr class="gradeX" >
                  <td style="text-align: center;" >{{$currency->id}}</td>
                  <td style="text-align: center;" >{{$currency->currency_code}}</td>
                  <td style="text-align: center;" >{{$currency->exchange_rate}} <smal> for 1 $ </small></td> 
                  <td style="text-align: center;">
                  @if($currency->status==1)
                   <span style ="color:green">Active</span>
                  @else
                   <span style ="color:red">InActive </span>
                  @endif
                  </td>

                  <td class="center" >
                  <a style="text-align: center;border-left-width: 0px;margin-right: 1px;" href="{{ url ('/admin/edit-currency/'.$currency->id)}}" class="btn btn-primary btn-mini" title="Edit currency">Edit</a>
               
                </div> </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
            
@endsection