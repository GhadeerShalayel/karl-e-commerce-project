
@extends('layouts.adminLayouts.admin_design')
@section('content')


<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Coupons</a> <a href="#" class="current">View Coupons</a> </div>
    <h1>Coupons</h1>

    @if(Session::has('flash_message_error'))    
              
              <div class="alert alert-warning alert-block alert_message1">
                 <button type="button" class="close" data-dismiss="alert">×</button>	
                   <strong> {!! session('flash_message_error') !!} </strong>
                </div>
        @endif

        @if(Session::has('flash_message_success'))  
              <div class="alert alert-success alert-block alert_message1">
                 <button type="button" class="close" data-dismiss="alert">×</button>	
                   <strong> {!! session('flash_message_success') !!} </strong>
                </div>
        @endif

  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
       
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>View Coupon</h5>
          </div>
         <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Coupon ID</th>
                  <th>Coupon code</th>
                  <th>Coupon amount</th>
                  <th>Amount type</th>
                  <th>Expiry Date</th>
                  <th>Created Date</th>
                  <th>Description</th>
                  <th>Status</th>
                  <th>Action</th>

                </tr>
              </thead>

              <tbody>
                  @foreach($coupons as $coupon)
                <tr class="gradeX" >
                  <td style="text-align: center;" >{{$coupon->id}}</td>
                  <td style="text-align: center;">{{$coupon->coupon_code}}</td>

                  <td style="text-align: center;">
                  {{$coupon->amount}}
                  @if($coupon->amount_type=="Percentage") % @else $ @endif
                  </td>
                  <td style="text-align: center;">{{$coupon->amount_type}}</td>
                  <td style="text-align: center;">{{$coupon->expiry_date}}</td>
                  <td style="text-align: center;">{{$coupon->created_at}}</td>
                  <td style="text-align: center;">{{$coupon->description}}</td>
                  <td style="text-align: center;">
                  @if($coupon->status==1) Active @else InActive @endif
                  </td>

            

                  <td class="center" >
                  <a style="text-align: center;border-left-width: 0px;margin-right: 1px;" href="{{ url ('/admin/edit-coupons/'.$coupon->id)}}" class="btn btn-primary btn-mini" title="Edit Product">Edit</a>
               
                  <a rel="{{ $coupon->id }}" rel1="delete-coupons" style="text-align: center;margin-right: 20px;"
                   href="javascript:" class="btn btn-danger btn-mini deletecouponRecord">Delete</a></div> </td>
                </tr>

                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
            
@endsection