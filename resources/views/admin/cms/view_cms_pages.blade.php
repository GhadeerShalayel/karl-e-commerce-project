
@extends('layouts.adminLayouts.admin_design')
@section('content')


<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Tables</a> </div>
    <h1>Users</h1>

    @if(Session::has('flash_message_error'))    
              
              <div class="alert alert-warning alert-block alert_message1">
                 <button type="button" class="close" data-dismiss="alert">×</button>	
                   <strong> {!! session('flash_message_error') !!} </strong>
                </div>
        @endif

        @if(Session::has('flash_message_success'))   
              <div class="alert alert-success alert-block alert_message1">
                 <button type="button" class="close" data-dismiss="alert">×</button>	
                   <strong> {!! session('flash_message_success') !!} </strong>
                </div>
        @endif

  </div>
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
       
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>View CMS Pages</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Page ID</th>
                  <th> Title</th>
                  <th> Description</th>
                  <th> URL</th>
                  <th> Meta Title</th>
                  <th> Meta Description</th>
                  <th> Meta Keywords</th>
                  <th> Status</th>
                  <th> Action</th>

                </tr>
              </thead>

              <tbody>
                  @foreach($cms_pages as $page)
                <tr class="gradeX" >
                  <td style="text-align: center;" >{{$page->id}}</td>
                  <td style="text-align: center;" >{{$page->title}}</td>
                  <td style="text-align: center;" >{{$page->description}}</td>
                  <td style="text-align: center;" >{{$page->url}}</td>
                  <td style="text-align: center;" >{{$page->meta_title}}</td>
                  <td style="text-align: center;" >{{$page->meta_description}}</td>
                  <td style="text-align: center;" >{{$page->meta_keywords}}</td>
                  <td style="text-align: center;">
                  @if($page->status==1)
                   <span style ="color:green">Active</span>
                  @else
                   <span style ="color:red">InActive </span>
                  @endif
                  </td>
                  <td style="text-align: center;" >
                  <a href="#myModal{{$page->id}}" data-toggle="modal" class="btn btn-success btn-mini" title="View Page">View </a>
                  <a style="text-align: center;border-left-width: 0px;margin-right: 1px;" href="{{ url ('/admin/edit-cms-page/'.$page->id)}}" class="btn btn-primary btn-mini" title="Edit Page">Edit</a>
                  <a rel="{{ $page->id }}" rel1="delete-cms-page" style="text-align: center;margin-right: 20px;"
               href="javascript:" class="btn btn-danger btn-mini deleteRecord" title="Delete Page">Delete</a>
             </td>
                </tr>
                <div id="myModal{{$page->id}}" class="modal hide">
                               <div class="modal-header">
                                 <button data-dismiss="modal" class="close" type="button">×</button>
                                 <h3>{{$page->title}} Full Details</h3>
                               </div>
                             <div class="modal-body">
                             <p><strong>page Title</strong> : {{$page->title}} </p>
                              <p><strong>page ID</strong> : {{$page->id}} </p>
                              <p><strong>page Description</strong> : {{$page->description}} </p>
                              <p><strong>page Status</strong> : {{$page->status}} </p>
                              <p><strong>page URL</strong> : {{$page->url}} </p>
                              <p><strong>Created on  </strong>: {{$page->created_at}} </p>

                              </div>
                  </div>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
            
@endsection