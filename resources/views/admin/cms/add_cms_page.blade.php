@extends('layouts.adminLayouts.admin_design')
@section('content')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
     <a href="#">CMS Page</a> <a href="#" class="current">Add Product</a> </div>
    <h1>Products</h1>
                
    @if(Session::has('flash_message_error'))    
              
              <div class="alert alert-warning alert-block alert_message1">
                 <button type="button" class="close" data-dismiss="alert">×</button>	
                   <strong> {!! session('flash_message_error') !!} </strong>
                </div>
        @endif

        @if(Session::has('flash_message_success'))  
              <div class="alert alert-success alert-block alert_message1">
                 <button type="button" class="close" data-dismiss="alert">×</button>	
                   <strong> {!! session('flash_message_success') !!} </strong>
                </div>
        @endif

  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5>Add CMS Page</h5>

          </div>
          <div class="widget-content nopadding"> <!--when your form includes any <input type="file"> elements-->
            <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{ url('/admin/add-cms-pages')}}" name="add_cms_pages" id="add_cms_pages" novalidate="novalidate">{{ csrf_field()}}
          
              <div class="control-group">
                <label class="control-label">CMS Title</label>
                <div class="controls">
                  <input type="text" name="cms_title" id="cms_title">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">CMS Description</label>
                <div class="controls">
                  <input type="text" name="cms_description" id="cms_title">
                </div>
              </div>

              <div class="control-group">
                <label class="control-label">CMS URL</label>
                <div class="controls">
                  <input type="text" name="cms_url" id="cms_title">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Meta Title</label>
                <div class="controls">
                  <input type="text" name="meta_title" id="meta_title">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Meta Description</label>
                <div class="controls">
                  <input type="text" name="meta_description" id="meta_description">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Meta Keywords</label>
                <div class="controls">
                  <input type="text" name="meta_keywords" id="meta_keywords">
                </div>
              </div>

             <div class="control-group">
                <label class="control-label">Enable</label>
                <div class="controls">
                  <input type="checkbox" name="status" id="status" value="1">
                </div>
              </div>
          
       
              
              <div class="form-actions">
                <input type="submit" value="Add CMS Page" class="btn btn-success">
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
 
  </div>
</div>


@endsection