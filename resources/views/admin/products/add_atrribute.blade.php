@extends('layouts.adminLayouts.admin_design')
@section('content')

<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
     <a href="#">Products</a> <a href="#" class="current">Add Attribute</a> </div>
    <h1>Product Attributes</h1>
                
    @if(Session::has('flash_message_error'))    
              
              <div class="alert alert-warning alert-block alert_message1">
                 <button type="button" class="close" data-dismiss="alert">×</button>	
                   <strong> {!! session('flash_message_error') !!} </strong>
                </div>
        @endif

        @if(Session::has('flash_message_success'))  
              <div class="alert alert-success alert-block alert_message1">
                 <button type="button" class="close" data-dismiss="alert">×</button>	
                   <strong> {!! session('flash_message_success') !!} </strong>
                </div>
        @endif

  </div>
  <div class="container-fluid"><hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
            <h5>Add Product Attributes</h5>

          </div>
          <div class="widget-content nopadding"> <!--when your form includes any <input type="file"> elements-->
            <form enctype="multipart/form-data" class="form-horizontal" method="post" action="{{ url('/admin/add-attributes/'.$productDetails->id)}}"
             name="add_attribute" id="add_attribute">{{ csrf_field()}}
             <input type="hidden" name="product_id" value = "{{$productDetails->id}}" />


              <div class="control-group">
                <label class="control-label">Product Name</label>
                <label class="control-label" style="text-align: center;"><strong >{{$productDetails->product_name}}</strong></label>

              </div>

              <div class="control-group">
                <label class="control-label">Product Code</label>
                <label class="control-label" style="text-align: center;"><strong>{{$productDetails->product_code}}</strong></label>

              </div>

              <div class="control-group">
                <label class="control-label">Product Color</label>
                <label class="control-label" style="text-align: center;"><strong>{{$productDetails->product_color}}</strong></label>

              </div>

              <div class="control-group">
                <label class="control-label"> </label>
                <div class="field_wrapper">
                <div>
                <!--one input field will display with Add button. When the add button is clicked, the duplicate input field will appear with the remove button.-->
                    <input required="" type="text" name="sku[]" id="sku" placeholder="SKU" style="width:120px;"/>
                    <input required="" type="text" name="size[]" id="size" placeholder="SIZE" style="width:120px;"/>
                    <input required="" type="text" name="price[]" id="price" placeholder="PRICE" style="width:120px;"/>
                    <input required="" type="text" name="stock[]" id="stock" placeholder="STOCK" style="width:120px;"/>

                    <a href="javascript:void(0);" class="add_button" title="Add field">Add</a>
                </div>
            </div>
              </div>

         


        
       
              
              <div class="form-actions">
                <input type="submit" value="Add Attribute" class="btn btn-success">
              </div>
              
        </form>
          </div>
        </div>
      </div>
    </div>

    <div class="row-fluid">
      <div class="span12">
       
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
            <h5>View Attributes</h5>
          </div>
          <div class="widget-content nopadding">
            <form method="post" action="{{ url('/admin/edit-attributes/'.$productDetails->id)}}"
             name="edit_attribute" id="edit_attribute">{{ csrf_field()}}
              <table class="table table-bordered data-table">
                <thead>
                  <tr>
                    <th>Attribute ID</th>
                    <th>SKU</th>
                    <th>Size</th>
                    <th>Price</th>
                    <th>Stock</th>
                  
                    <th>Action</th>

                  </tr>
                </thead>

                <tbody>
                    @foreach($productDetails['arrtibute'] as $attributes)
                  <tr class="gradeX" >
                    <td style="text-align: center;" ><input type="hidden" name="IdAttr[]" value ="{{$attributes->id}}">{{$attributes->id}}</td>
                    <td style="text-align: center;">{{$attributes->sku}}</td>
                    <td style="text-align: center;">{{$attributes->size}}</td>
                    <td style="text-align: center;"><input type="text" name="price[]" value="{{$attributes->price}}"></td>
                    <td style="text-align: center;"><input type="text" name="stock[]" value="{{$attributes->stock}}"></td>
                  
                    <td class="center" >   
                      <input type="submit" value="Update" class="btn btn-primary btn-mini" >        
                    <a rel="{{ $attributes->id }}" rel1="delete-attribute" style="text-align: center;margin-right: 20px;"
                href="javascript:" class="btn btn-danger btn-mini deleteRecord">Delete</a> </td>
                  </tr>                               
                  </div>
                  @endforeach
                </tbody>
              </table>
            </form>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>


@endsection