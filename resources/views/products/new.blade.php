@extends('layouts.frontlayouts.front_design')
@section('content')
<?php use App\Product ; ?>

        
        @if(Session::has('flash_message_error'))    
              
              <div class="alert alert-warning alert-block alert_message1">
                 <button type="button" class="close" data-dismiss="alert">×</button>	
                   <strong> {!! session('flash_message_error') !!} </strong>
                </div>
                @endif

                @if(Session::has('flash_message_success'))  
                    <div class="alert alert-success alert-block alert_message1">
                        <button type="button" class="close" data-dismiss="alert">×</button>	
                        <strong> {!! session('flash_message_success') !!} </strong>
                        </div>
                @endif
        <!-- ****** New Arrivals Area Start ****** -->
        <section class="new_arrivals_area section_padding_100_0 clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="section_heading text-center">
                            <h2>New Arrivals</h2>
                        </div>
                    </div>
                </div>
            </div>

            <div class="karl-projects-menu mb-100">
                <div class="text-center portfolio-menu">
                    <button class="btn active" data-filter="*">Featured items</button>
                  <!--  @foreach($category as $cat)
                        @if($cat->status == "1" )
                        
                            <button class="btn" data-filter=".{{$cat->id}}">{{$cat->name}}</button>

                        @endif
                    @endforeach-->


                </div>
            </div>
          
            <div class="container">
                <div class="row karl-new-arrivals">

                    <!-- Single gallery Item Start -->
            @foreach($ProductAll as $product)
              <div class="col-gh col-sm-6 col-md-4 single_gallery_item {{$product->id}} wow fadeInUpBig" data-wow-delay="0.2s">
                    <!-- ****** Quick View Modal Area Start ****** -->
                <div class="modal fade" id="quickview{{$product->id}}" tabindex="-1" role="dialog" aria-labelledby="quickview" aria-hidden="true">
                    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <button type="button" class="close btn" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>

                            <div class="modal-body">
                                <div class="quickview_body">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-12 col-lg-5">
                                                <div class="quickview_pro_img">
                                                    <img src="{{ asset ('images/backend_images/products/home/'.$product->image)}}" alt="">
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-7">
                                                <div class="quickview_pro_des">
                                                    <h4 class="title">{{$product->product_name}}</h4>
                                                    <div class="top_seller_product_rating mb-15">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                    </div>
                                                             <!-- Add to Cart Form -->
                                                <form class="cart" method="post" action="{{url('/add-cart')}}">{{csrf_field()}}

                                                    <input type = "hidden" name ="product_id"value ="{{$product->id}}">
                                                    <input type = "hidden" name ="product_name"value ="{{$product->product_name}}">
                                                    <input type = "hidden" name ="product_code"value ="{{$product->product_code	}}">
                                                    <input type = "hidden" name ="product_color" value ="{{$product->product_color}}">
                                                    <input type = "hidden" name ="price" id="price" value ="{{$product->price}}">
                                                    <select  class="form-control" id ="selectSize" name="size" style="width:100px;margin-right: 10px ">
                                                        <option value="" >Size </option>
                                                        @foreach($product->arrtibute as $Size)
                                                        <option value="{{$product->id}}-{{$Size->size}}" class="widget-title">{{$Size->size}} </option>
                                                        @endforeach
                                                    </select>
                                                    <div class="quantity">
                                                        <span class="qty-minus" onclick="var effect = document.getElementById('{{$product->id}}'); var qty = effect.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 1 ) effect.value--;return false;"><i class="fa fa-minus" aria-hidden="true"></i></span>
                                                        <input type="number" class="qty-text" id="{{$product->id}}" step="1" min="1" max="12" name="quantity" value="1">
                                                        <span class="qty-plus" onclick="var effect = document.getElementById('{{$product->id}}'); var qty = effect.value; if( !isNaN( qty )) effect.value++;return false;"><i class="fa fa-plus" aria-hidden="true"></i></span>
                                                    </div>
                                                    <button type="submit" name="addtocart" value="5" class="cart-submit" style=" width: 40px;"><i class="ti-bag"></i></button>
                                                    <!-- Wishlist -->
                                                    <div class="modal_pro_wishlist">
                                                        <button type="submit" style="padding: 0;border: none;"  id="cartWishlist"  value="Wish List" name="cartWishlist"><i class="ti-heart"></i></button>
                                                    </div>

                                                </form></br>
                                                <?php $getCurrencyRate = Product::getCurrencyRates( $product->price); ?>
                                                <span>
                                                    <h4 class="price" id="getPrice">$ {{$product->price}}
                                                    <img data-html="true"  data-toggle="tooltip"
                                                    title="<small>ILS</small> {{$getCurrencyRate['ILS_rate']}}<br>
                                                        <small>JOD</small> {{$getCurrencyRate['JOD_rate']}}<br><br<br>" style=" margin-bottom: 4px;" src="https://img.icons8.com/windows/32/000000/currency-exchange.png">  
                                                </h4></span>
                                                    <p>{{$product->product_name}}</p>
                                                    <a href="{{url('product/'.$product->id)}}">View Full Product Details</a>
                                                </div>
                                       

                                                <div class="share_wf mt-30">
                                                    <p>Share With Friend</p>
                                                    <div class="_icon">
                                                        <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                                        <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                                        <a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                                                        <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

        <!-- ****** Quick View Modal Area End ****** -->
                        <!-- Product Image -->
                        <div class="product-img">
                            <img src="{{ asset ('images/backend_images/products/home/'.$product->image)}}" alt="">
                            <div class="product-quicview">
                                <a href="#" data-toggle="modal" data-target="#quickview{{$product->id}}"><i class="fa fa-plus"></i></a>
                            </div>
                        </div>
                        <!-- Product Description -->
                        <div class="product-description">
                            <h4 class="product-price">${{$product->price}}</h4>
                            <p>{{$product->product_name}}</p>
                            <!-- Add to Cart -->
                            <a href="#" class="add-to-cart-btn">ADD TO CART</a>
                        </div>
                    </div>
            @endforeach
                </div>
                {{ $ProductAll->links( ) }}

            </div>
        </section>
        <!-- ****** New Arrivals Area End ****** -->
     
    <!-- /.wrapper end -->

    
@endsection