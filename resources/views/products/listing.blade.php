@extends('layouts.frontlayouts.front_design')
@section('content')
   
<?php use App\Product ; ?>

   <section class="shop_grid_area section_padding_100">
            <div class="container">
                <div class="row">
                <div class="col-12 col-md-4 col-lg-3">
                            <form name="form" action ="{{url('/products/filter')}}" method="post">{{csrf_field()}}
                                @if(!empty($url))
                                <input type="hidden" value="{{$url}}" name="url">
                                @endif
                                    <div class="shop_sidebar_area">
                                    
                                        <div class="widget catagory mb-50">
                                            <!--  Side Nav  -->
                                            <div class="nav-side-menu">
                                                <h6 class="mb-0" style="padding-bottom: 50px;">Catagories</h6>
                                                @foreach($category as $cat)
                                                @if($cat->status == "1")

                                                <div class="menu-list" style="margin-top: 0px; ">

                                                    <ul id="menu-content2" class="menu-content collapse out" >
                                                        <!-- Single Item -->
                                                        <li data-toggle="collapse" data-target="#{{$cat->url}}" class="collapsed">
                                                            <a href="#">{{$cat->name}}</a>
                                                            <ul class="sub-menu collapse" id="{{$cat->url}}">
                                                                @foreach($cat->category as $sub)
                                                                @if($sub->status == "1")
                                                                <li><a style="margin-bottom: 10px;  margin-top: 6px;" href="{{$sub->url}}"><img class="img-listing" src="{{ asset ('images/frontend_images/top_category/'.$sub->image)}}" alt="">{{$sub->name}}</a><li>
                                                                @endif
                                                                @endforeach

                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                                @endif
                                                @endforeach
                                            </div>
                                        </div>
                                        @if(!empty($url))
                                        <div class="widget color mb-70">
                                            <h6 class="widget-title mb-30">Filter by Color</h6>
                                                <div class="widget-desc">
                                                    <ul class="d-flex justify-content-between">
                                                        @foreach($colorArry as $color)
                                                            @if(!empty($_GET['color']))
                                                            <?php $colorArry = explode('-',$_GET['color']);?>
                                                                @if(in_array($color,$colorArry))
                                                                <?php $colorcheck="checked"; ?>
                                                                @else
                                                                <?php $colorcheck=""; ?>
                                                                @endif

                                                            @else
                                                                <?php $colorcheck=""; ?>
                                                            @endif
                                                            <li class="">
                                                            <input class="inputcolor" type="checkbox" name = "colorFilter[]" {{$colorcheck }} value="{{$color}}" id="{{$color}}" onchange="javascript:this.form.submit();">
                                                                <label class="labelcolor {{$color}}" for="{{$color}}" style=" margin-top: 0px;"></label>
                                                            </li>
                                                        @endforeach
                                                        </ul>
                                                </div>
                                        </div>

                                        <div class="widget size mb-50">
                                            <h6 class="widget-title mb-30">Filter by Size</h6>
                                            <div class="widget-desc">
                                                <ul class=" justify-content-between">
                                                        @foreach($sizeArry as $size)
                                                            @if(!empty($_GET['size']))
                                                            <?php $sizeArry = explode('-',$_GET['size']);?>
                                                                @if(in_array($size,$sizeArry))
                                                                <?php $sizecheck="checked"; ?>
                                                                @else
                                                                <?php $sizecheck=""; ?>
                                                                @endif

                                                            @else
                                                                <?php $sizecheck=""; ?>
                                                            @endif
                                                            <li class="" >
                                                            <input class="inputcolor" type="checkbox" name = "sizeFilter[]" {{$sizecheck }} value="{{$size}}" id="{{$size}}" onchange="javascript:this.form.submit();">
                                                                <label class="size" for="{{$size}}" style=" margin-top: 0px;text-align: center;" > {{$size}}</label>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                            </div>
                                        </div>
                                        @endif

                                    </div>
                            </form>
                        </div>   

      

            <div class="col-12 col-md-8 col-lg-9">
            @if(Session::has('flash_message_error'))    
              
              <div class="alert alert-warning alert-block alert_message1">
                  <button type="button" class="close" data-dismiss="alert">×</button>	
                  <strong> {!! session('flash_message_error') !!} </strong>
                  </div>
                  @endif

                  @if(Session::has('flash_message_success'))  
                      <div class="alert alert-success alert-block alert_message1">
                          <button type="button" class="close" data-dismiss="alert">×</button>	
                          <strong> {!! session('flash_message_success') !!} </strong>
                          </div>
                  @endif
                <div class="shop_grid_product_area">
                     
                            <div class="row">
                            @foreach($ProductAll as $product)
                             @if($cat->status == "1")

                 <!-- ****** Quick View Modal Area Start ****** -->
        
                <div class="modal fade" id="quickview{{$product->id}}" tabindex="-1" role="dialog" aria-labelledby="quickview" aria-hidden="true">
                    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <button type="button" class="close btn" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>

                              <div class="modal-body">
                                <div class="quickview_body">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-12 col-lg-5">
                                                <div class="quickview_pro_img">
                                                    <img src="{{ asset ('images/backend_images/products/home/'.$product->image)}}" alt="">
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-7">
                                                <div class="quickview_pro_des">
                                                    <h4 class="title">{{$product->product_name}}</h4>
                                                    <div class="top_seller_product_rating mb-15">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                    </div>
                                                             <!-- Add to Cart Form -->
                                                <form class="cart" method="post" action="{{url('/add-cart')}}">{{csrf_field()}}

                                                    <input type = "hidden" name ="product_id"value ="{{$product->id}}">
                                                    <input type = "hidden" name ="product_name"value ="{{$product->product_name}}">
                                                    <input type = "hidden" name ="product_code"value ="{{$product->product_code	}}">
                                                    <input type = "hidden" name ="product_color" value ="{{$product->product_color}}">
                                                    <input type = "hidden" name ="price" id="price" value ="{{$product->price}}">
                                                    <select  class="form-control" id ="selectSize" name="size" style="width:100px;margin-right: 10px ">
                                                        <option value="" >Size </option>
                                                        @foreach($product->arrtibute as $Size)
                                                        <option value="{{$product->id}}-{{$Size->size}}" class="widget-title">{{$Size->size}} </option>
                                                        @endforeach
                                                    </select>
                                                    <div class="quantity">
                                                        <span class="qty-minus" onclick="var effect = document.getElementById('{{$product->id}}'); var qty = effect.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 1 ) effect.value--;return false;"><i class="fa fa-minus" aria-hidden="true"></i></span>
                                                        <input type="number" class="qty-text" id="{{$product->id}}" step="1" min="1" max="12" name="quantity" value="1">
                                                        <span class="qty-plus" onclick="var effect = document.getElementById('{{$product->id}}'); var qty = effect.value; if( !isNaN( qty )) effect.value++;return false;"><i class="fa fa-plus" aria-hidden="true"></i></span>
                                                    </div>
                                                    <button type="submit" name="addtocart" value="5" class="cart-submit" style=" width: 40px;"><i class="ti-bag"></i></button>
                                                    <!-- Wishlist -->
                                                    <div class="modal_pro_wishlist">
                                                        <button type="submit" style="padding: 0;border: none;"  id="cartWishlist"  value="Wish List" name="cartWishlist"><i class="ti-heart"></i></button>
                                                    </div>
                                                 
                                                </form></br>

                                                <?php $getCurrencyRate = Product::getCurrencyRates( $product->price); ?>
                                                <span>
                                                    <h4 class="price">$ {{$product->price}}
                                                        <img data-html="true"  data-toggle="tooltip"
                                                        title="<small>ILS</small> {{$getCurrencyRate['ILS_rate']}}<br>
                                                            <small>JOD</small> {{$getCurrencyRate['JOD_rate']}}<br><br<br>" style=" margin-bottom: 4px;" src="https://img.icons8.com/windows/32/000000/currency-exchange.png">  
                                                    </h4>
                                                </span>
                                                <p>{{$product->product_name}}</p>
                                                    <a href="{{url('product/'.$product->id)}}">View Full Product Details</a>
                                                </div>
                                       

                                                <div class="share_wf mt-30">
                                                    <p>Share With Friend</p>
                                                    <div class="_icon">
                                                        <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                                        <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                                        <a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                                                        <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        <!-- ****** Quick View Modal Area End ****** -->

                                <!-- Single gallery Item -->
                                <div class="col-gh col-sm-6 col-lg-4 single_gallery_item wow fadeInUpBig" data-wow-delay="0.2s">
                                    <!-- Product Image -->
                                    <div class="product-img">
                                        <img src="{{ asset ('images/backend_images/products/shop/'.$product->image)}}" alt="">
                                        <div class="product-quicview">
                                            <a href="#" data-toggle="modal" data-target="#quickview{{$product->id}}"><i class="ti-plus"></i></a>
                                        </div>
                                    </div>
                                    <!-- Product Description -->
                                    <div class="product-description">
                                        <h4 class="product-price">${{$product->price}}</h4>
                                        <p>{{$product->product_name}}</p>
                                        <!-- Add to Cart -->
                                        <a href="#" class="add-to-cart-btn">ADD TO CART</a>
                                    </div>

                                </div>
                                @endif
                                @endforeach
                            </div>
                            @if(!empty($search_product))
                                {{ $ProductAll->links() }}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
 @endsection