@extends('layouts.frontlayouts.front_design')
@section('content')
<?php use App\Product ; ?>

        <!-- <<<<<<<<<<<<<<<<<<<< Breadcumb Area Start <<<<<<<<<<<<<<<<<<<< -->
        <div class="breadcumb_area">
        @if(Session::has('flash_message_error'))    
              
              <div class="alert alert-warning alert-block alert_message1">
                 <button type="button" class="close" data-dismiss="alert">×</button>	
                   <strong> {!! session('flash_message_error') !!} </strong>
                </div>
                @endif

                @if(Session::has('flash_message_success'))  
                    <div class="alert alert-success alert-block alert_message1">
                        <button type="button" class="close" data-dismiss="alert">×</button>	
                        <strong> {!! session('flash_message_success') !!} </strong>
                        </div>
                @endif
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <ol class="breadcrumb d-flex align-items-center ">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{url('products/'.$catregoryDetails->url)}}">{{$catregoryDetails->name}}</a></li>
                            <li class="breadcrumb-item active">{{$productDetaiels->product_name}}</li>
                        </ol>
                        <!-- btn -->
                        <a href="{{ url()->previous() }}" class="backToHome d-block"><i class="fa fa-angle-double-left"></i> Back to Category</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- <<<<<<<<<<<<<<<<<<<< Breadcumb Area End <<<<<<<<<<<<<<<<<<<< -->

        <!-- <<<<<<<<<<<<<<<<<<<< Single Product Details Area Start >>>>>>>>>>>>>>>>>>>>>>>>> -->
        <section class="single_product_details_area section_padding_0_100">
            <div class="container">
                <div class="row">

                    <div class="col-12 col-md-6">
                        <div class="single_product_thumb">
                            <div id="product_details_slider" class="carousel slide" data-ride="carousel">

                                <ol class="carousel-indicators thumbnails " >

                                  <li class="active" data-target="#product_details_slider" data-slide-to="0" >
                                    <a href ="{{ asset ('images/backend_images/products/medium/'.$productDetaiels->image)}}" data-standard="{{ asset ('images/backend_images/products/medium/'.$productDetaiels->image)}}">
                                        <img style="cursor:pointer; " class="d-block w-100" src="{{ asset ('images/backend_images/products/medium/'.$productDetaiels->image)}}" alt="First slide">
                                    </a>
                                  </li>
                                  @foreach($ProductAlternativeImage as $altImage)

                                    <li data-target="#product_details_slider" data-slide-to="1">
                                    <a href ="{{ asset ('images/backend_images/products/medium/'.$altImage->image)}}" data-standard="{{ asset ('images/backend_images/products/medium/'.$altImage->image)}}">
                                        <img  style="cursor:pointer; " class="d-block w-100 changeImage" src="{{ asset ('images/backend_images/products/medium/'.$altImage->image)}}" alt="Second slide">
                                    </a>

                                    </li>
                                    @endforeach

                                    </ol>
                                    <div class="carousel-inner easyzoom easyzoom--overlay easyzoom--with-thumbnails"  style="height:710px ;">
                                    <div class="carousel-item active" > 
                                    <a href="{{ asset ('images/backend_images/products/large/'.$productDetaiels->image)}}">
                                        <img style="height:710px ;"  class="d-block w-100" src="{{ asset ('images/backend_images/products/large/'.$productDetaiels->image)}}" alt="First slide">
                                    </a>
                                    </div>
                                    @foreach($ProductAlternativeImage as $altImage)
                                    <div  class="carousel-item easyzoom easyzoom--overlay easyzoom--with-thumbnails" >
                                    <a href="{{ asset ('images/backend_images/products/large/'.$altImage->image)}}">
                                        <img style="height:710px ;"   class="d-block w-100 mainImage" src="{{ asset ('images/backend_images/products/large/'.$altImage->image)}}" alt="Second slide">
                                    </a>
                                    </div>
                                    @endforeach
                                  </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <form name="addtocartForm" id="addtocartForm" action ="{{url('add-cart')}}" method="post">{{csrf_field()}}
                                <div class="single_product_desc">

                                    <input type = "hidden" name ="product_id"value ="{{$productDetaiels->id}}">
                                    <input type = "hidden" name ="product_name"value ="{{$productDetaiels->product_name}}">
                                    <input type = "hidden" name ="product_code"value ="{{$productDetaiels->product_code	}}">
                                    <input type = "hidden" name ="product_color" value ="{{$productDetaiels->product_color}}">
                                    <input type = "hidden" name ="price" id="price" value ="{{$productDetaiels->price}}">

                                    <h4 class="title"><a href="#">{{$productDetaiels->product_name}}</a><span><small> {{$productDetaiels->product_code}}</small></span></h4>

                                    <?php $getCurrencyRate = Product::getCurrencyRates($productDetaiels->price); ?>
                                    <span id="getPrice"><h4 class="price">$ {{$productDetaiels->price}}</h4>

                                    <h7><small>ILS</small> {{$getCurrencyRate['ILS_rate']}}<br>
                                    <small>JOD</small> {{$getCurrencyRate['JOD_rate']}}<br><br></h7>
                                    </span>
                                    <p class="available">Available: <span class="text-muted" id="available" > @if($total_stock >0) In Stock @else Out of Stock @endif</span> </p>

                                    <div class="single_product_ratings mb-15">
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                    </div>
                                        <p>
                                        <h4 class="widget-title"> Size </h4>
                                            <select class="form-control"  id ="selectSize" name="size" style="width:130px; ">
                                                <option value="" >Select Size </option>
                                                @foreach($productDetaiels->arrtibute as $Size)
                                                <option value="{{$productDetaiels->id}}-{{$Size->size}}" class="widget-title">{{$Size->size}} </option>
                                                @endforeach
                                            </select>
                                        </p>

                                        
                                        <div class="widget color mb-70">
                                            <h6 class="widget-title mb-30">Color</h6>
                                                <div class="widget-desc">
                                                    <ul class="d-flex ">
                                                        @foreach($ProductColor as $color)
                                                            <li class="">
                                                            <input class="inputcolor" type="checkbox" name = "colorFilter[]"  value="{{$color->color}}" id="{{$color->color}}">
                                                                <label class="labelcolor {{$color->color}}" for="{{$color->color}}" style=" margin-top: 0px; margin-right: 8px;"></label>
                                                            </li>
                                                        @endforeach
                                                        </ul>
                                                </div>
                                        </div>
                                    <!-- Add to Cart Form -->
                                    <div class="cart clearfix mb-50 d-flex" >

                                        <div class="quantity">
                                            <span class="qty-minus" onclick="var effect = document.getElementById('qty'); var qty = effect.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 1 ) effect.value--;return false;"><i class="fa fa-minus" aria-hidden="true"></i></span>
                                            <input type="number" class="qty-text" id="qty" step="1" min="1" max="12" name="quantity" value="1">
                                            <span class="qty-plus" onclick="var effect = document.getElementById('qty'); var qty = effect.value; if( !isNaN( qty )) effect.value++;return false;"><i class="fa fa-plus" aria-hidden="true"></i></span>
                                        </div>
                                        @if($total_stock>0)
                                        <button type="submit" name="addtocart"  class="btn cart-submit " id="cartButton">Add to cart</button>
                                        @endif
                                        <div class="modal_pro_wishlist">
                                            <button type="submit" style="padding: 0;border: none; margin-top: 3px;"  id="cartWishlist"  value="Wish List" name="cartWishlist"><i class="ti-heart"></i></button>
                                        </div>
                                    </div>

                                    <div id="accordion" role="tablist">
                                        <div class="card">
                                            <div class="card-header" role="tab" id="headingOne">
                                                <h6 class="mb-0">
                                                    <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Information</a>
                                                </h6>
                                            </div>

                                            <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                                                <div class="card-body">
                                                    <p>{{$productDetaiels->description}}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" role="tab" id="headingTwo">
                                                <h6 class="mb-0">
                                                    <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Material & Care</a>
                                                </h6>
                                            </div>
                                            <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                                                <div class="card-body">
                                                    <p>{{$productDetaiels->care}}.</p>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" role="tab" id="headingThree">
                                                <h6 class="mb-0">
                                                    <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">delivery option</a>
                                                </h6>
                                            </div>
                                            <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
                                                <div class="card-body">
                                                    <p>Tax: Applicable tax on the basis of exact location & discount will be charged at the time of checkout
                                                        100% Original Products
                                                        Free Delivery on order above Rs. 1199
                                                        Cash on delivery might be available
                                                        Easy 30 days returns and exchanges
                                                        Try & Buy might be available.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                              </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- <<<<<<<<<<<<<<<<<<<< Single Product Details Area End >>>>>>>>>>>>>>>>>>>>>>>>> -->

        <!-- ****** Quick View Modal Area Start ****** -->
        @foreach($relatedProduct as $product)
        <div class="modal fade" id="quickview{{$product->id}}" tabindex="-1" role="dialog" aria-labelledby="quickview" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                    <button type="button" class="close btn" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                    <div class="modal-body">
                        <div class="quickview_body">
                            <div class="container">
                                <div class="row">
                                    <div class="col-12 col-lg-5">
                                        <div class="quickview_pro_img">
                                            <img src="{{ asset ('images/backend_images/products/medium/'.$product->image)}}" alt="">
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-7">
                                        <div class="quickview_pro_des">
                                            <h4 class="title">{{$product->product_name}}</h4>
                                            <div class="top_seller_product_rating mb-15">
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                            </div>
                                            <h5 class="price">${{$product->price}}<span>$130</span></h5>
                                            <p>{{$product->description}}</p>
                                            <a href="{{url('product/'.$product->id)}}">View Full Product Details</a>
                                        </div>
                                        <!-- Add to Cart Form -->
                                        <form class="cart" method="post">
                                            <div class="quantity">
                                                <span class="qty-minus" onclick="var effect = document.getElementById('qty'); var qty = effect.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 1 ) effect.value--;return false;"><i class="fa fa-minus" aria-hidden="true"></i></span>

                                                <input type="number" class="qty-text" id="qty2" step="1" min="1" max="12" name="quantity" value="1">

                                                <span class="qty-plus" onclick="var effect = document.getElementById('qty'); var qty = effect.value; if( !isNaN( qty )) effect.value++;return false;"><i class="fa fa-plus" aria-hidden="true"></i></span>
                                            </div>
                                            <button type="submit" name="addtocart" value="5" class="cart-submit">Add to cart</button>
                                            <!-- Wishlist -->
                                            <div class="modal_pro_wishlist">
                                                <a href="wishlist.html" target="_blank"><i class="ti-heart"></i></a>
                                            </div>
                                            <!-- Compare -->
                                            <div class="modal_pro_compare">
                                                <a href="compare.html" target="_blank"><i class="ti-stats-up"></i></a>
                                            </div>
                                        </form>

                                        <div class="share_wf mt-30">
                                            <p>Share With Friend</p>
                                            <div class="_icon">
                                                <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                                <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                                <a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                                                <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        <!-- ****** Quick View Modal Area End ****** -->

        <section class="you_may_like_area clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="section_heading text-center">
                            <h2>related Products</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="you_make_like_slider owl-carousel">

                            <!-- Single gallery Item -->
                            @foreach($relatedProduct as $product)
                            <div class="single_gallery_item">
                                <!-- Product Image -->
                                <div class="product-img">
                                    <img src="{{ asset ('images/backend_images/products/shop/'.$product->image)}}" alt="">
                                    <div class="product-quicview">
                                        <a href="#" data-toggle="modal" data-target="#quickview{{$product->id}}"><i class="ti-plus"></i></a>
                                    </div>
                                </div>
                                <!-- Product Description -->
                                <div class="product-description">
                                    <h4 class="product-price"> $ {{$product->price}}</h4>
                                    <p>{{$product->description}}</p>
                                    <!-- Add to Cart -->
                                    <a href="#" class="add-to-cart-btn">ADD TO CART</a>
                                </div>
                            </div>
                            @endforeach

                        
                        </div>
                    </div>
                </div>
            </div>
        </section>
 @endsection