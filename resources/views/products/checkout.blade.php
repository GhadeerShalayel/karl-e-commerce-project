@extends('layouts.frontlayouts.front_design')
@section('content')


    <!-- ****** Checkout Area Start ****** -->
    <div class="checkout_area section_padding_100">
            <div class="container">
            <form action="{{url('/checkout')}}" method="post"> {{csrf_field()}}
                    @if(Session::has('flash_message_error'))    
                        
                        <div class="alert alert-warning alert-block alert_message1">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong> {!! session('flash_message_error') !!} </strong>
                            </div>
                    @endif

                    @if(Session::has('flash_message_success'))  
                        <div class="alert alert-success alert-block alert_message1">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong> {!! session('flash_message_success') !!} </strong>
                            </div>
                    @endif
                <div class="row">

                <div class="col-12 col-md-6" style="padding-left: 0px;">
                        <div class="checkout_details_area mt-50 clearfix">

                            <div class="cart-page-heading">
                                <h5>Shiping Address</h5>
                                <p>Enter your cupone code</p>
                            </div>

                           
                                <div class="row">
                                    <div class="col-md-6 mb-3">
                                        <label for="first_name"> Name <span>*</span></label>
                                        <!-- Add condition with all user and shipping fields so that if the variables are empty then value will not come. -->
                                        <input type="text" class=" form-control " id="shiping_name" name="shiping_name" @if(!empty($shipingDetailes->name)) value="{{$shipingDetailes->name}}" @endif style="width: 585px;height: 52px;"required>
                                    </div>
                                    <div class="col-12 mb-3">
                                        <label for="street_address">Address <span>*</span></label>
                                        <input type="text" class="  form-control mb-3 " name="shiping_address" id="shiping_address" @if(!empty($shipingDetailes->address)) value=" {{$shipingDetailes->address}} @endif  "required>
                                    </div>
                                    <div class="col-12 mb-3">
                                        <label for="city">Town/City <span>*</span></label>
                                        <input type="text" class="  form-control " id="shiping_city" name="shiping_city"  @if(!empty($shipingDetailes->city)) value="{{$shipingDetailes->city}}" @endif required>
                                    </div>
                                    <div class="col-12 mb-3">
                                        <label for="state">State <span>*</span></label>
                                        <input type="text" class="form-control " id="shiping_state" name="shiping_state" @if(!empty($shipingDetailes->state)) value="{{$shipingDetailes->state}}" @endif required>
                                    </div>
                                    <div  class="col-12 mb-3">
                                        <label for="country">Country <span>*</span></label>
                                        <select id="shiping_country"  name ="shiping_country"  class="form-control"required>
                                            <option  value="" >Select Country  </option>
                                            @foreach($Countries as $country)
									    	<option @if(!empty($shipingDetailes->country))  value="{{ $country->country_name }}" @endif   @if (!empty($shipingDetailes->country) && $country->country_name == $shipingDetailes->country) selected @endif>{{ $country->country_name }}</option>
									        @endforeach
                                        </select>
                                    </div>
                                  
                                    <div class="col-12 mb-3">
                                        <label for="postcode">Pincode <span>*</span></label>
                                        <input type="text" class="form-control " name="shiping_pincode" id="shiping_pincode" @if(!empty($shipingDetailes->pincode)) value="{{$shipingDetailes->pincode}}" @endif required>
                                    </div>
                                   
                                    <div class="col-12 mb-3">
                                        <label for="phone_number">Phone No <span>*</span></label>
                                        <input type="number" class="form-control " name="shiping_phone" id="shiping_phone" min="0" @if(!empty($shipingDetailes->mobile)) value="{{$shipingDetailes->mobile}}" @endif required>
                                    </div>
                            
                                </div>
                                <button type="submit"  class="btn karl-checkout-btn" >CheckOut</a>


                        </div>
                    </div>


                <div class="col-12 col-md-6 col-lg-5 ml-lg-auto">
                        <div class="order-details-confirmation">
                        <div class="cart-page-heading">
                                <h5>Billing Address</h5>
                            </div>

                                <div class="row">
                                <div class="col-md-6 mb-3">
                                        <label for="first_name"> Name <span>*</span></label>
                                        <input type="text" class=" form-control " id="billing_name" name="billing_name" @if(!empty($userDetailes->name)) value="{{$userDetailes->name}}" @endif style="width: 360px;"required>
                                    </div>
                                    <div class="col-12 mb-3">
                                        <label for="street_address">Address <span>*</span></label>
                                        <input type="text" class="  form-control mb-3 " name="billing_address" id="billing_address" @if(!empty($userDetailes->address)) value="{{$userDetailes->address}}" @endif required>
                                    </div>
                                    <div class="col-12 mb-3">
                                        <label for="city">Town/City <span>*</span></label>
                                        <input type="text" class="  form-control " id="billing_city" name="billing_city" @if(!empty($userDetailes->city)) value="{{$userDetailes->city}}" @endif frequired>
                                    </div>
                                    <div class="col-12 mb-3">
                                        <label for="state">State <span>*</span></label>
                                        <input type="text" class="form-control " id="billing_state"  name="billing_state" @if(!empty($userDetailes->state)) value="{{$userDetailes->state}}" @endif required>
                                    </div>
                                    <div  class="col-12 mb-3">
                                        <label for="state">Country <span>*</span></label>
                                            <select id="billing_country"  name ="billing_country"  class="form-control"required>
                                            <option  value="" >Select Country  </option>
                                            @foreach($Countries as $country)
                                            <option @if(!empty($userDetailes->country))  value = "{{ $country->country_name }}" @endif  @if ($country->country_name == $userDetailes->country ) selected @endif > {{$country->country_name}} </option>
                                            @endforeach
                                        </select>
                                    </div>
                                  
                                    <div class="col-12 mb-3">
                                        <label for="postcode">Pincode <span>*</span></label>
                                        <input type="text" class="form-control " id="billing_pincode" name ="billing_pincode" @if(!empty($userDetailes->pincode)) value="{{$userDetailes->pincode}}" @endif required>
                                    </div>
                                   
                                    <div class="col-12 mb-3">
                                        <label for="phone_number">Phone No <span>*</span></label>
                                        <input type="number" class="form-control " id="billing_phone" name ="billing_phone" min="0" @if(!empty($userDetailes->mobile)) value="{{$userDetailes->mobile}}" @endif required>
                                    </div>

                                    <div class="col-12 mb-3">
                                         <div class="custom-control custom-checkbox d-block mb-2">
                                            <input type="checkbox" class="custom-control-input" id="Bill2Ship">
                                            <label class="custom-control-label" for="Bill2Ship" style="margin-top: 0px;">Shiping address same as Billing Address</label>
                                        </div>
                                    </div>

                                </div>
                            
                         

                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <!-- ****** Checkout Area End ****** -->


@endsection