@extends('layouts.frontlayouts.front_design')
@section('content')
<?php use App\Product ; ?>

  <!-- ****** Cart Area Start ****** -->
  <div class="cart_area section_padding_100 clearfix">
            <div class="container">
            @if(Session::has('flash_message_error'))    
              
              <div class="alert alert-danger alert-block alert_message1">
                 <button type="button" class="close" data-dismiss="alert">×</button>	
                   <strong> {!! session('flash_message_error') !!} </strong>
                </div>
                @endif

                @if(Session::has('flash_message_success'))  
                    <div class="alert alert-success alert-block alert_message1">
                        <button type="button" class="close" data-dismiss="alert">×</button>	
                        <strong> {!! session('flash_message_success') !!} </strong>
                        </div>
                @endif
                <div class="row">
      
                    <div class="col-12">
                        <div class="cart-table clearfix">
                            <table class="table table-responsive">
                                <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Total</th>
                                        <th>Action</th>


                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $total_amount = 0; ?>
                                    @foreach($UserCart as $cart)
                                    <tr>
                                        <td class="cart_product_img d-flex align-items-center">
                                            <a href="#"><img src="{{ asset ('images/backend_images/products/medium/'.$cart->image)}}" alt="Product"></a>
                                            <h6><a href="#">{{$cart->product_name}}</a></h6>
                                            <p style="color:#D8D8D8	" class="ml-1">{{$cart->size}} </p>
                                            <p style="color:#D8D8D8	" class="ml-1">{{$cart->product_color}} </p>

                                             <!-- <small> <br> <b class="ml-1">Code:{{$cart->product_code}}<b></small>-->


                                        </td>
                                        <?php $product_price = Product::getProductPrice($cart->product_id ,$cart->size ); ?>

                                        <td class="price"><span>${{$product_price}}</span></td>
                                        <td class="qty">
                                            <div class="quantity">
                                               <a href="{{url('/cart/update-quantity/'.$cart->id.'/-1')}}" ><span class="qty-minus" onclick="var effect = document.getElementById('{{$cart->id}}'); var qty = effect.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 1 ) effect.value--;return false;"><i class="fa fa-minus" aria-hidden="true"></i></span></a>
                                                <input type="number" class="qty-text" id="{{$cart->id}}" step="1" min="1" max="99" name="quantity" value="{{$cart->quantity}}">
                                               <a href="{{url('/cart/update-quantity/'.$cart->id.'/1')}}" ><span class="qty-plus" onclick="var effect = document.getElementById('{{$cart->id}}'); var qty = effect.value; if( !isNaN( qty )) effect.value++;return false;"><i class="fa fa-plus" aria-hidden="true"></i></span></a>
                                            </div>
                                        </td>

                                        
                                        <td class="total_price"><span>${{$product_price * $cart->quantity }} </span></td>
                                        <td> <a href ="{{url('/cart/delete-product-cart/'.$cart->id)}}" style="text-decoration: none;"><span class="ti-trash trash"></a> </td>
                                    </tr>
                                    <?php $total_amount = $total_amount + ($product_price * $cart->quantity ); ?>
                                    @endforeach


                                </tbody>
                            </table>
                        </div>
                        <div class="total"><span class ="total_amount">$<?php echo $total_amount ; ?></span></div>

                        <div class="cart-footer d-flex mt-30">
                            <div class="back-to-shop w-50">
                            </div>
                            <div class="update-checkout w-50 text-right">
                                <a href="#">Update cart</a>
                            </div>
                            
                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="coupon-code-area mt-70">
                            <div class="cart-page-heading">
                                <h5>Cupon code</h5>
                                <p>Enter your cupone code</p>
                            </div>
                            <form action="{{url('cart/apply-coupon')}} " method="post">{{csrf_field()}}
                                <input type="text" name="coupon_code">
                                <button type="submit">Apply</button>
                            </form>
                        </div>
                    </div>

                   
                    <div class="col-12 col-lg-4">
                        <div class="cart-total-area mt-70">
                            <div class="cart-page-heading">
                                <h5>Cart total</h5>
                                <p></p>
                            </div>

                            <ul class="cart-total-chart">
                                @if(!empty(Session::get('couponAmount')))
                                <li><span>Subtotal</span> <span>$<?php echo $total_amount ; ?></span></li>
                                <li><span>Cupon discount</span> <span>$<?php echo Session::get('couponAmount') ; ?></span></li>
                                <li><span>Grand Total</span> <span>$<?php echo $total_amount -  Session::get('couponAmount') ; ?></span></li>

                                @else
                                <?php $getCurrencyRate = Product::getCurrencyRates( $total_amount); ?>
                                <li><span><strong>Grand Total</strong></span> <span >$ <strong ><?php echo $total_amount ; ?></strong>
                                    <img data-html="true"  data-toggle="tooltip"
                                 title="<small>ILS</small> {{$getCurrencyRate['ILS_rate']}}<br>
                                    <small>JOD</small> {{$getCurrencyRate['JOD_rate']}}<br><br<br>" src="https://img.icons8.com/windows/32/000000/currency-exchange.png">  
                           
                                 </span>
                                </li>
                                @endif
                            </ul>
                            <a href="{{url('/checkout')}}" class="btn karl-checkout-btn">Proceed to checkout</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ****** Cart Area End ****** -->

@endsection