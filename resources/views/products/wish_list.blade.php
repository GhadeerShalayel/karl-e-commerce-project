@extends('layouts.frontlayouts.front_design')
@section('content')
<?php use App\Product ; ?>
  <!-- ****** Cart Area Start ****** -->
  <div class="cart_area section_padding_100 clearfix">
            <div class="container">
            @if(Session::has('flash_message_error'))    
              
              <div class="alert alert-danger alert-block alert_message1">
                 <button type="button" class="close" data-dismiss="alert">×</button>	
                   <strong> {!! session('flash_message_error') !!} </strong>
                </div>
                @endif

                @if(Session::has('flash_message_success'))  
                    <div class="alert alert-success alert-block alert_message1">
                        <button type="button" class="close" data-dismiss="alert">×</button>	
                        <strong> {!! session('flash_message_success') !!} </strong>
                        </div>
                @endif
                <div class="row">
      
                    <div class="col-12">
                        <div class="cart-table clearfix">
                            <table class="table table-responsive">
                                <thead>
                                    <tr>
                                        <th>Wish List Product</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Action</th>


                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $total_amount = 0; ?>
                                    @foreach($UserWishlist as $cart)
                                    <tr>
                                        <td class="cart_product_img d-flex align-items-center">
                                            <a href="{{url('product/'.$cart->product_id)}}"><img src="{{ asset ('images/backend_images/products/medium/'.$cart->image)}}" alt="Product"></a>
                                            <h6><a href="{{url('product/'.$cart->product_id)}}">{{$cart->product_name}}</a></h6>
                                            <p style="color:#D8D8D8	" class="ml-1">{{$cart->size}} </p>

                                             <!-- <small> <br> <b class="ml-1">Code:{{$cart->product_code}}<b></small>-->


                                        </td>
                                        <?php $product_price = Product::getProductPrice($cart->product_id ,$cart->size ); ?>
                                        <td class="price"><span><small>KWD</small> {{$product_price}}</span></td>
                                        <td class="qty">
                                            <div class="quantity">
                                               <a href="{{url('/cart/update-quantity/'.$cart->id.'/-1')}}" ><span class="qty-minus" onclick="var effect = document.getElementById('{{$cart->id}}'); var qty = effect.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 1 ) effect.value--;return false;"><i class="fa fa-minus" aria-hidden="true"></i></span></a>
                                                <input type="number" class="qty-text" id="{{$cart->id}}" step="1" min="1" max="99" name="quantity" value="{{$cart->quantity}}">
                                               <a href="{{url('/cart/update-quantity/'.$cart->id.'/1')}}" ><span class="qty-plus" onclick="var effect = document.getElementById('{{$cart->id}}'); var qty = effect.value; if( !isNaN( qty )) effect.value++;return false;"><i class="fa fa-plus" aria-hidden="true"></i></span></a>
                                            </div>
                                        </td>
                                       <!-- <form class="cart" method="post" action="{{url('/add-cart')}}">{{csrf_field()}}
                                            <input type = "hidden" name ="product_id"value ="{{$cart->id}}">
                                            <input type = "hidden" name ="product_name"value ="{{$cart->product_name}}">
                                            <input type = "hidden" name ="product_code"value ="{{$cart->product_code	}}">
                                            <input type = "hidden" name ="product_color" value ="{{$cart->product_color}}">
                                            <input type = "hidden" name ="price" id="price" value ="{{$cart->price}}">

                                        <td>
                                            <button class="button-add" type="submit" name="cartButton" value="add to cart"><span class="ti-bag trash"></button>-->
                                            <a href ="{{url('/wish-list/delete-product-wish/'.$cart->id)}}" style="text-decoration: none;"><span class="ti-trash trash"></a> 
                                        </td>
                                        </form>
                                    </tr>
                                    @endforeach


                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- ****** Cart Area End ****** -->

@endsection