@extends('layouts.frontlayouts.front_design')
@section('content')
<?php use App\Product ; ?>

  <!-- ****** Cart Area Start ****** -->

  <div class="checkout_area section_padding_100">
            <div class="container">
                <div class="row">

                <div class="col-12 col-md-6" style="padding-left: 0px; padding-right: 80px;">
                    <div class="order-details-confirmation">
                        <div class="cart-page-heading">


                            <div class="cart-page-heading">
                                <h5>Shiping Details</h5>
                            </div>
                                <div class="row">
                                    <div class="col-md-6 mb-3">
                                    {{$shipingDetailes->name}}
                                    </div>
                                    <div class="col-12 mb-3">
                                    {{$shipingDetailes->address}}
                                    </div>
                                    <div class="col-12 mb-3">
                                    {{$shipingDetailes->city}}
                                    </div>
                                    <div class="col-12 mb-3">
                                    {{$shipingDetailes->state}}
                                    </div>
                                    <div  class="col-12 mb-3">
                                    {{$shipingDetailes->country}}
                                    </div>
                                  
                                    <div class="col-12 mb-3">
                                    {{$shipingDetailes->pincode}}
                                    </div>
                                   
                                    <div class="col-12 mb-3">
                                    {{$shipingDetailes->mobile}}
                                    </div>
                            
                                </div>

                        </div>
                    </div>
                </div>


                <div class="col-12 col-md-6 col-lg-5 ml-lg-auto">
                        <div class="order-details-confirmation">
                        <div class="cart-page-heading">
                                <h5>Billing Address</h5>
                            </div>

                                <div class="row">
                                <div class="col-md-6 mb-3">
                                {{$userDetailes->name}}
                                    </div>
                                    <div class="col-12 mb-3">
                                    {{$userDetailes->address}}
                                    </div>
                                    <div class="col-12 mb-3">
                                    {{$userDetailes->city}}
                                    </div>
                                    <div class="col-12 mb-3">
                                    {{$userDetailes->state}}
                                    </div>
                                    <div  class="col-12 mb-3">
                                    {{$userDetailes->country }}
                                    </div>
                                  
                                    <div class="col-12 mb-3">
                                    {{$userDetailes->pincode}}
                                    </div>
                                   
                                    <div class="col-12 mb-3">
                                    {{$userDetailes->mobile}}
                                    </div>

                                </div>
                            
                         

                        </div>
                    </div>
                </div>
            </div>
    </div>
  <div class="cart_area section_padding_100 clearfix">
            <div class="container">

            @if(Session::has('flash_message_error'))    
                        
                        <div class="alert alert-warning alert-block alert_message1">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong> {!! session('flash_message_error') !!} </strong>
                            </div>
                    @endif

                    @if(Session::has('flash_message_success'))  
                        <div class="alert alert-success alert-block alert_message1">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong> {!! session('flash_message_success') !!} </strong>
                            </div>
                    @endif
                
                <div class="row">
                    <div class="col-12">
                        <div class="cart-table clearfix">
                        <h6> Review & Payment</h6>

                            <table class="table table-responsive">
                                <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th>Price</th>
                                        <th>Quantity</th>
                                        <th>Total</th>
                                        <th>Action</th>


                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $total_amount = 0;?>
                                    @foreach($UserCart as $cart)
                                    <tr>
                                        <td class="cart_product_img d-flex align-items-center">
                                            <a href="#"><img src="{{ asset ('images/backend_images/products/medium/'.$cart->image)}}" alt="Product"></a>
                                            <h6><a href="#">{{$cart->product_name}}</a></h6>
                                            <p style="color:#D8D8D8	" class="ml-1">{{$cart->size}} </p>
                                             <!-- <small> <br> <b class="ml-1">Code:{{$cart->product_code}}<b></small>-->


                                        </td>
                                        <?php $product_price = Product::getProductPrice($cart->product_id ,$cart->size ); ?>

                                        <td class="price"><span>${{$product_price}}</span></td>
                                        <td class="qty">
                                            <div class="quantity">
                                               <a href="{{url('/cart/update-quantity/'.$cart->id.'/-1')}}" ><span class="qty-minus" onclick="var effect = document.getElementById('{{$cart->id}}'); var qty = effect.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 1 ) effect.value--;return false;"><i class="fa fa-minus" aria-hidden="true"></i></span></a>
                                                <input type="number" class="qty-text" id="{{$cart->id}}" step="1" min="1" max="99" name="quantity" value="{{$cart->quantity}}">
                                               <a href="{{url('/cart/update-quantity/'.$cart->id.'/1')}}" ><span class="qty-plus" onclick="var effect = document.getElementById('{{$cart->id}}'); var qty = effect.value; if( !isNaN( qty )) effect.value++;return false;"><i class="fa fa-plus" aria-hidden="true"></i></span></a>
                                            </div>
                                        </td>

                                        
                                        <td class="total_price"><span style = "color:#ff084e;" >${{$product_price * $cart->quantity }} </span></td>
                                        <td> <a href ="{{url('/order-review/delete-product-cart/'.$cart->id)}}" style="text-decoration: none;"><span class="ti-trash trash"></a> </td>
                                    </tr>
                                    <?php $total_amount = $total_amount + ($product_price * $cart->quantity );?>

                                    @endforeach


                                </tbody>
                            </table>
                        </div>
                        <div class="total"><span class ="total_amount" style = "color:#ff084e; padding-left: 942px;">$<?php echo $total_amount ; ?></span></div>

                        <div class="cart-footer d-flex mt-30">
                            <div class="back-to-shop w-50">
                            </div>
                            <div class="update-checkout w-50 text-right">
                                <a href="#">Update cart</a>
                            </div>
                            
                        </div>

                    </div>
                </div>


                <div class="row">

                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="coupon-code-area mt-70">
                            <div class="cart-page-heading">
                                <h5>Cupon code</h5>
                                <p>Enter your cupone code</p>
                            </div>
                            <form action="{{url('cart/apply-coupon')}} " method="post">{{csrf_field()}}
                                <input type="text" name="coupon_code">
                                <button type="submit">Apply</button>
                            </form>
                        </div>
                    </div>
          
                   
                    <div class="col-12 col-lg-4">
                        <div class="cart-total-area mt-70">
                            <div class="cart-page-heading">
                                <h5>Cart total</h5>
                                <p></p>
                            </div>
                            <?php $shippingCharges =  Session::get('ShippingCharges'); ?>
                            <ul class="cart-total-chart">
                                <li><span>Sub Total</span> <span>${{$total_amount}} </span></li>
                                <li><span>Shipping Cost</span> <span>$ {{$shippingCharges}}</span></li>
                                <li><span>Cupon discount</span> <span>$
                                     @if(!empty(Session::get('couponAmount')))
                                         {{Session::get('couponAmount')}}  
                                       @else 0 
                                       @endif </span></li>
                                <li><span>Grand Total</span> <span style = "color:#ff084e;">${{$grand_total = $shippingCharges + $total_amount -  Session::get('couponAmount')}} </span></li>
            
                            </ul>
                        </div>
                    </div>

                    
                    <div class="col-12 col-md-6 col-lg-4">
                        <form id="paymentMethod" name="paymentMethod" method="post" action = "{{url('/place-order')}}" >{{csrf_field()}}
                        <input type="hidden" name="grand_total" value = "{{$grand_total}}">
                        <div class="shipping-method-area mt-70">
                            <div class="cart-page-heading">
                                <h5>Payment method</h5>
                                <p>Select the one you want</p>
                            </div>

                            <div class="custom-control custom-radio mb-30">
                                <input type="radio" id="COD" name="payment_method" class="custom-control-input" value="COD">
                                <label class="custom-control-label d-flex align-items-center justify-content-between" for="COD"><span>Cash on delivery</span></label>
                            </div>

                            <div class="custom-control custom-radio mb-30">
                                <input type="radio" id="PayPal" name="payment_method" class="custom-control-input" value="PayPal">
                                <label class="custom-control-label d-flex align-items-center justify-content-between" for="PayPal"><span>PayPal</span></label>
                            </div>

                        </div>
                        <button type="submit"  class="btn karl-checkout-btn" style = "margin-top: 39px;"value="" onclick="return selectPaymentMethod();">Place Order</button>

                      </form>
                    </div>

                    
                </div>
            </div>
        </div>
        <!-- ****** Cart Area End ****** -->

@endsection