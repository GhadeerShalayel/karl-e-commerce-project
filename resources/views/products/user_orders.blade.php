@extends('layouts.frontlayouts.front_design')
@section('content')

    

<div class="main">
        


        <!-- Thanks form -->
        <section class="signup" id ="signup">
            @if(Session::has('flash_message_error'))    
              
              <div class="alert alert-danger alert-block alert_message ">
                 <button type="button" class="close" data-dismiss="alert">×</button>	
                   <strong> {!! session('flash_message_error') !!} </strong>
                </div>
                @endif

                @if(Session::has('flash_message_success'))  
                    <div class="alert alert-success alert-block alert_message">
                        <button type="button" class="close" data-dismiss="alert">×</button>	
                        <strong> {!! session('flash_message_success') !!} </strong>
                        </div>
            @endif
            <div class="container-register">
                <div class="signup-content">
                    <table id="example" class="table table-striped table-bordered" style="width:100% " >
                        <thead>
                            <tr>
                                <th>Order ID</th>
                                <th>Order Products</th>
                                <th>Payment Method</th>
                                <th>Grand Total</th>
                                <th>Created On</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($orders as $order)
                                <tr>
                                    <td>{{ $order -> id }}</td>

                                    <td>
                                        @foreach($order->orders as $pro)
                                          <a href="{{url('/orders/'.$order->id)}}" style = "color : #ff084e;  text-decoration: none;" > {{$pro->product_code}}</a> <br>
                                        @endforeach
                                    </td>
                                    <td>{{ $order -> payment_method }}</td>
                                    <td>{{ $order -> grand_total }}</td>
                                    <td>{{ $order -> created_at }}</td>
                                    <td>View Details</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
</div>

@endsection