<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*Route::resource is a way to specify several routes to your controller methods with a single declaration. 
For example, with Route::resource('user', 'UserController');
, you can access index, update, create, show, store, edit and destroy methods in your UserController*/

