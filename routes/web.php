
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
/*Route::get('/', function () {
    return view('coming-soon');
});*/

//Home Page

Route::get('/','IndexController@index');

Route::match(['get','post'] , '/admin' , 'AdminController@login'); /*Sometimes you may need to register a route that responds to multiple HTTP verbs. You may do so using the match method. Or, you may even register a route that responds to all HTTP verbs using the any method:*/
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Category listing Page 
Route::get('/products/{url}','ProductsController@products');

// Products Filters Route
Route::match(['get', 'post'],'/products/filter', 'ProductsController@filter');

//Product Details Page
Route::get('/product/{id}','ProductsController@product');

//Get Product Attribute Price
Route::get('/get-product-price','ProductsController@getProductPrice');

//Add to cart route 
Route::match(['get','post'] , '/add-cart' , 'ProductsController@addtocart');

Route::match(['get','post'] , '/cart' , 'ProductsController@cart');
//Delete product in cart route 
Route::get('/cart/delete-product-cart/{id}' , 'ProductsController@DeleteProductCart'); 
//Delete product in order-review route 
Route::get('/order-review/delete-product-cart/{id}' , 'ProductsController@DeleteProductorderReview'); 
//Update quantity product in cart route 
Route::get('/cart/update-quantity/{id}/{quantity}','ProductsController@updateCartQuantity');
//Route to Applay Coupon
Route::post('/cart/apply-coupon','ProductsController@applyCoupon');

// Wish List Page
Route::match(['get', 'post'],'/wish-list','ProductsController@wishList');

// Users Register Form Submit
Route::match(['get','post'],'/user-register','UsersController@register');

// Users Login Form Submit
Route::match(['get','post'],'/user-login','UsersController@Login');

// Users Login/Register Page
Route::match(['get','post'],'/login-register','UsersController@userLoginRegister');
Route::match(['get' , 'post'] ,'/forgot-password' ,'UsersController@forgot_password');

Route::get('/logout','AdminController@logout');

//Check if the user already exist
Route::match(['get','post'],'/check-email','UsersController@checkEmail');

//Route of Account Page
Route::match(['get','post'],'/account','UsersController@account');

// Users Logout Page
Route::get('/user-logout','UsersController@logout'); 

// Confirm Account
Route::get('confirm/{code}','UsersController@confirmAccount');

// Search Products
Route::post('products/search-products','ProductsController@searchProducts');


// Contact Page 
Route::match(['GET','POST'],'/page/contact','CmsController@contact');

Route::match(['GET','POST'],'/test','Controller@viewcopoun');

// Display CMS Page
Route::match(['get','post'],'/page/{url}','CmsController@cmsPage');

// checking subscriber email
Route::match(['get','post'],'/check-subscriber-email','NewsletterController@checkSubscriber');

// Add Subscriber Email
Route::post('/add-subscriber-email','NewsletterController@addSubscriber');

//All Routes after login
Route::group(['middleware' => ['frontlogin']] , function(){
    //Route of User Account Page
    Route::match(['get','post'],'/account','UsersController@account');
    //Check Current User Password
    Route::post('/check-user-pwd','UsersController@chkPassword');
    //Update User Password
    Route::match(['get','post'] , '/update-pwd' , 'UsersController@updatePassword');
    //checkout page 
    Route::match(['get','post'] , '/checkout' , 'ProductsController@productCheckout');
    //oreder review page
    Route::match(['get','post'],'/order-review','ProductsController@orderReview');
    //Place Order route
    Route::match(['get','post'],'/place-order','ProductsController@placeOrder');
    //Thanks page
    Route::get('/Thanks','ProductsController@Thanks');
    // Users Orders Page
    Route::get('/orders','ProductsController@userOrders');
    // User Ordered Poduct Page
    Route::get('/orders/{id}','ProductsController@userOrdersDetails');
    //Paypal Page
    Route::get('/paypal','ProductsController@paypal');
    //PayPal Thanks page
    Route::get('/paypal/thanks','ProductsController@Thankspaypal');
    //PayPal Cancel Page
    Route::get('/paypal/cancel','ProductsController@Cancelpaypal');

    // Wish List Page
    Route::match(['get', 'post'],'/wish-list','ProductsController@wishList');
    //Delete product in Wishlist route 
    Route::get('/wish-list/delete-product-wish/{id}' , 'ProductsController@DeleteProductWishList'); 
});


Route::group(['middleware' => ['adminLogin'] ] , function(){

    //admin routes
    Route::get('/admin/dashboard','AdminController@dashboard'); 
    Route::get('/admin/settings','AdminController@settings'); 
    Route::get('/admin/check-pwd','AdminController@chkPassword'); //Now we have created route for checking current password that we have passed via Ajax URL 
    Route::match(['get','post'] , '/admin/update-pwd' , 'AdminController@updatePassword');//added both GET/POST method in route 


    //Category Routes (ADMIN)

    Route::match(['get','post'] , '/admin/add-category' , 'CategoryController@add_Category');
    Route::match(['get','post'] , '/admin/edit-category/{id}' , 'CategoryController@edit_Category');
    Route::match(['get','post'] , '/admin/delete-category/{id}' , 'CategoryController@delete_Category');
    Route::get('/admin/view-category','CategoryController@view_Category'); 

    //Product Routes (ADMIN)
    Route::match(['get','post'] , '/admin/add-product' , 'ProductsController@add_Product');
    Route::match(['get','post'] , '/admin/edit-product/{id}' , 'ProductsController@edit_Product');
    Route::get('/admin/view-product','ProductsController@view_Product'); 
    Route::get('/admin/delete-product/{id}','ProductsController@deleteProduct'); 
    Route::get('/admin/delete-product-image/{id}','ProductsController@deleteProductImage'); 
    Route::get('/admin/delete-image/{id}','ProductsController@deleteImages'); 


    //Product Attribute(ADMIN)
    Route::match(['get','post'] , '/admin/add-attributes/{id}' , 'ProductsController@add_Attribute');
    Route::match(['get','post'] , '/admin/edit-attributes/{id}' , 'ProductsController@editAttribute');
    Route::match(['get','post'] , '/admin/add-color/{id}' , 'ProductsController@addColor');
    Route::get('/admin/delete-color/{id}','ProductsController@deleteColor'); 
    Route::match(['get','post'] , '/admin/add-images/{id}' , 'ProductsController@addImage');
    Route::get('/admin/delete-attribute/{id}','ProductsController@deleteAttribute'); 

    // Coupon Routes (ADMIN) 
    Route::match(['get','post'],'/admin/add-coupon','CouponsController@addCoupon');
    Route::match(['get','post'] , '/admin/edit-coupons/{id}' , 'CouponsController@editCoupon');
    Route::match(['get','post'] , '/admin/delete-coupons/{id}' , 'CouponsController@deleteCoupon');
    Route::get('/admin/view-coupons','CouponsController@viewCoupons');

    // Banner Routes (ADMIN) 
    Route::match(['get','post'],'/admin/add-banner','BannersController@addBanner');
    Route::match(['get','post'],'/admin/edit-banner/{id}','BannersController@editBanner');
    Route::match(['get','post'] , '/admin/delete-banner/{id}' , 'BannersController@deleteBanner');
    Route::get('/admin/view-banner','BannersController@viewBanner');

    // Orders Routs (ADMIN)
    Route::get('/admin/view-orders','ProductsController@viewOrders');
    // User Ordered Products Details
    Route::get('/admin/view-orders/{id}','ProductsController@viewOrdersDetails');
    // Order Invoice
    Route::get('/admin/view-order-invoice/{id}','ProductsController@viewOrderInvoice');
    //Update order status Route (ADMIN)
     Route::post('admin/order-status-update','ProductsController@updateOrderStaus');
     

    //User Routes (ADMIN)
    Route::get('/admin/view-users','UsersController@viewUsers');

    //CMS Pages Route 
    Route::match(['get','post'],'/admin/add-cms-pages','CmsController@addCmsPage');
    Route::get('/admin/view-cms-pages','CmsController@ViewCmsPage');
    Route::match(['get','post'],'/admin/edit-cms-page/{id}','CmsController@editCmsPage');
    Route::get('/admin/delete-cms-page/{id}','CmsController@deleteCmsPage'); 


    //Currency Route 
    Route::match(['get','post'],'/admin/add-currency','CurrencyController@addCurrency');
    Route::get('/admin/view-currencies','CurrencyController@viewCurrencies');
    Route::match(['get','post'],'/admin/edit-currency/{id}','CurrencyController@editCurrency');

    
    // View Shipping Charges
    Route::get('/admin/view-shipping','ShippingController@viewShipping');
    // Update Shipping Charges
    Route::match(['get','post'],'/admin/edit-shipping/{id}','ShippingController@editShipping');  


    // View Newsletter Subscribers
    Route::get('/admin/view-newsletter-subscribers','NewsletterController@viewNewsletterSubscribers');
    // Update Newsletter Status
    Route::get('/admin/update-newsletter-status/{id}/{status}','NewsletterController@updateNewsletterStatus');
    // Delete Newsletter Email
    Route::get('/admin/delete-newsletter-email/{id}','NewsletterController@deleteNewsletterEmail');
    // Export Newsletter Emails
    Route::get('/admin/export-newsletter-emails','NewsletterController@exportNewsletterEmails');


} );


/*hat contain all our admin routes that needs protection.Also,
we have made changes in RedirectifAuthenticated.php file of Laravel from where we have redirected to our login page 
if we directly tried accessing any admin page like dashboard without logging in.*/




